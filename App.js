import React, {Fragment} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SplashScreen from './src/components/splashScreen';
import Login from './src/components/login';
import SignUp from './src/components/signup';
import MobileVerification from './src/components/mobileVerification';
import OtpSignUpVerification from './src/components/otpSignUpVerification';
import ForgotPassword from './src/components/forgotPassword';
import LanguageSelect from './src/components/languageSelect';
import Home from './src/components/home';
import SocialWall from './src/components/socialWall';
import Profile from './src/components/profile';
import EditProfile from './src/components/editProfile';
import ReportsLanding from './src/components/reportsLanding';
import ReportDetails from './src/components/reportDetails';
import AddReport from './src/components/addReport';
import LearningLanding from './src/components/learningLanding';
import LearningList from './src/components/learningList';
import LearningPage from './src/components/learningPage';
import LeadershipLanding from './src/components/leadershipLanding';
import LeadershipChallenges from './src/components/leadershipChallenges';
import Notifications from './src/components/notifications';
import Settings from './src/components/settings';
import Rulesgems from './src/components/rulesGems';
import Rewardsgems from './src/components/rewardsGems';
import Faqs from './src/components/faqs';
import Help from './src/components/help';
import { Colors } from './src/constants';

const RootStack = createStackNavigator({
  SplashScreen: {
    screen: SplashScreen,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' }
    }
  },
  Login: {
    screen: Login,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' }
    }
  },
  SignUp: {
    screen: SignUp,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  MobileVerification: {
    screen: MobileVerification,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  OtpSignUpVerification: {
    screen: OtpSignUpVerification,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  ForgotPassword: {
    screen: ForgotPassword,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  LanguageSelect: {
    screen: LanguageSelect,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  Home: {
    screen: Home,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  SocialWall: {
    screen: SocialWall,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  Profile: {
    screen: Profile,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  EditProfile: {
    screen: EditProfile,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  ReportsLanding: {
    screen: ReportsLanding,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  ReportDetails: {
    screen: ReportDetails,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  AddReport: {
    screen: AddReport,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  LearningLanding: {
    screen: LearningLanding,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  LearningList: {
    screen: LearningList,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  LearningPage: {
    screen: LearningPage,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  LeadershipLanding: {
    screen: LeadershipLanding,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  LeadershipChallenges: {
    screen: LeadershipChallenges,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  Notifications: {
    screen: Notifications,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  Settings: {
    screen: Settings,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  Rulesgems: {
    screen: Rulesgems,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  Faqs: {
    screen: Faqs,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  Help: {
    screen: Help,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  },
  Rewardsgems: {
    screen: Rewardsgems,
    navigationOptions: {
      title: '',
      headerTitleStyle: { color: '#fff' },
      gesturesEnabled: false,
    }
  }

}, {
  headerMode: 'none',
  initialRouteName: 'SplashScreen',
});

const App = createAppContainer(RootStack);

// export default App


export default class Oleo extends React.Component {
  render() {
    return(
      <Fragment>
        {/* <SafeAreaView style={{ flex: 0, backgroundColor: Colors.primary }} /> */}
        {/* <SafeAreaView style={{flex:1, backgroundColor: Colors.secondary}}> */}
          {/* <View style={{flex:1,position:'relative'}}> */}
              <StatusBar barStyle = "light-content" hidden = {false} backgroundColor = 'transparent' translucent = {true}/>
              <App 
                
              />
          {/* </View> */}
        {/* </SafeAreaView> */}
      </Fragment>
    )
  }
}