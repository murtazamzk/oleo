import { StyleSheet, Dimensions } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from '@react-native-firebase/app';

let {width,height} = Dimensions.get('screen');

export const Colors = {
    primary: '#0ed96d',
    secondary: '#081d29',
    textGrey: '#5c6f7a',
    textBlue:'#638599',
    bgBlue :'#0e2b3b'
}

export const Fonts = {
    PoppinsRegular : 'Poppins-Regular',
    PoppinsMedium : 'Poppins-Medium',
    PoppinsBold : 'Poppins-Bold',
    PoppinsLight : 'Poppins-Light',
}

export const Commonstyles = StyleSheet.create({
    textInput: {
        color: Colors.textGrey,
        fontSize: 20,
        width: '80%',
        paddingBottom: 5,
        borderBottomColor: Colors.textGrey,
        borderBottomWidth: 1,
        marginBottom: 50,
        fontFamily: Fonts.PoppinsRegular
    },
    selectInput: {
        width: '80%',
        paddingBottom: 0,
        borderBottomColor: Colors.textGrey,
        borderBottomWidth: 1,
        marginBottom: 50
    },
    placeholder: {
        color: Colors.textGrey,
    },
    btnPrimary: {
        width: '80%',
        borderRadius: 20,
        backgroundColor: Colors.primary,
        height: 50,
        justifyContent: 'center'
    },
    btnPrimaryText: {
        fontSize: 20,
        textAlign: 'center',
        color: Colors.secondary,
        textTransform: 'uppercase',
        fontFamily: Fonts.PoppinsBold
    },
    radioBtnWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 30
    },
    radioBtn: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    radioBtnCircle: {
        width: 20,
        height: 20,
        borderRadius: 20,
        borderColor: Colors.textGrey,
        borderWidth: 2,
        marginRight: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    radioBtnCircleInner: {
        width: 10,
        height: 10,
        borderRadius: 10,
        backgroundColor: Colors.primary,
    },
    radioBtnCircleActive: {
        borderColor: Colors.primary,
    },
    radioBtnText: {
        fontSize: 20,
        color: Colors.textGrey,
        fontFamily: Fonts.PoppinsRegular,
    },
    socialWall: {

    },
    socialRow: {
        flexDirection: 'row',
        paddingTop: 10
    },
    userAvatarGrey: {
        width: 40,
        height: 40,
        borderRadius: 40,
        borderWidth: 1,
        borderColor: Colors.textGrey,
        overflow:'hidden',
    },
    userImageGrey: {
        width: 40,
        height: 40,
        borderRadius: 40
    },
    socialRowContent: {
        paddingLeft: 12,
        flex: 1
    },
    socialPost: {
        fontSize: 14,
        color: '#c3c7c9',
        paddingRight: 30,
        fontFamily: Fonts.PoppinsRegular,
    },
    socialPostBold:{
        fontWeight:'bold',
        color:'#ffffff',
    },
    socialRowBottom: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderColor: '#27414f'
    },
    cheersWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    cheerIcon: {
        width: 15,
        height: 13
    },
    cheerCount: {
        fontSize: 12,
        color: '#ffffff',
        paddingLeft: 5,
        fontFamily: Fonts.PoppinsRegular,
    },
    socialTime: {
        fontSize: 12,
        color: Colors.textGrey,
        fontFamily: Fonts.PoppinsRegular,
    },
    contentBox:{
        backgroundColor: '#0e2b3b',
        paddingHorizontal: 15,
        paddingVertical: 20,
        borderRadius: 10,
        marginBottom: 20
    },
    contentBoxHeader:{
        paddingBottom: 15,
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems: 'center',
        borderBottomWidth:1,
        borderColor: '#27414f'
    },
    userProfile:{
        flexDirection:'row',
        alignItems:'center',
        width:'100%'
    },
    userAvatar:{
        position:'relative',
        width:50,
        height:50,
        flex:0.15
    },
    userImage:{
        width:50,
        height:50,
        borderWidth: 1,
        borderColor: Colors.primary,
        borderRadius: 50
    },
    userLevelWrapper:{
        position:'absolute',
        right:-5,
        width: 25,
        height:20,
        backgroundColor: Colors.primary,
        borderRadius: 10,
        borderColor: Colors.secondary,
        borderWidth:1,
        justifyContent:'center'
    },
    userLevel:{
        fontSize: 12,
        textAlign:'center',
        color:Colors.secondary,
        fontFamily:Fonts.PoppinsRegular,
    },
    userDetails:{
        justifyContent:'space-between',
        paddingLeft:15,
        flex:0.65
    },
    nameWrapper:{
        flexDirection:'row',
        alignItems:'center'
    },
    nameMain:{
        fontSize: 16,
        color:'#ffffff',
        fontFamily: Fonts.PoppinsBold,
    },
    nameTitle:{
        fontSize: 14,
        color:Colors.textGrey,
        fontFamily: Fonts.PoppinsMedium,
    },
    progressWrapper:{
        marginTop:10,
        backgroundColor: '#423a3f',
        borderRadius: 15,
        marginRight: 35,
    },
    progressBar:{
        height:18,
        backgroundColor: '#dc624a',
        borderRadius: 18,
        justifyContent:'center'
    },
    progressBarValue:{
        textAlign:'center',
        fontSize: 10,
        fontFamily: Fonts.PoppinsMedium,
        position:'absolute',
        left:'50%',
        color:'#fff',
        top:2
    },
    progressNumberWrapper:{
        position:'absolute',
        top:-3,
        right:-35,
        width: 55,
        height:25,
        backgroundColor: Colors.secondary,
        borderRadius: 25,
        borderColor: '#638599',
        borderWidth:1,
        justifyContent:'center'
    },
    progressNumber:{
        color:'#638599',
        textAlign:'center',
        fontSize: 11,
        fontFamily: Fonts.PoppinsMedium,
    },
    userPrizes:{
        flex:0.2,
        alignItems:'flex-end',
        justifyContent:'space-between',
    },
    prizeBox:{
        backgroundColor:Colors.secondary,
        paddingVertical:5,
        paddingHorizontal:10,
        borderRadius:20,
        flexDirection:'row',
        alignItems:'center'
    },
    prizeIcon:{
        width:15,
        height:15,
    },
    prizeText:{
        color:'#fff',
        fontSize:10,
        marginRight:5,
        fontFamily:Fonts.PoppinsRegular,
    },
    loaderWrapper:{
        position:'absolute',
        top:0,
        left:0,
        width:'100%',
        height:height,
        backgroundColor:'rgba(0,0,0,0.3)',
        alignItems:'center',
        justifyContent:'center',
        zIndex:10,
    },
    loaderWrapperInline:{
        padding: 20,
        width: '100%',
        alignItems:'center',
    },
    loader:{
        width:50,
        height:50
    },
    dialog: {
        backgroundColor: 'transparent',
        marginTop: 70,
    },
    dialogContent: {
        backgroundColor: '#ffffff',
        justifyContent: 'space-between',
        borderRadius: 12,
        paddingVertical: 15,
        paddingHorizontal: 20,
        height: 450,
    },
    dialogFooter: {
        borderTopWidth: 1,
        borderTopColor: "#e6e9eb",
        paddingTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    dialogCloseBtn: {
        alignSelf: 'center',
        marginTop: 50,
    },
    dialogCloseIcon: {
        width: 50,
        height: 55,
    },
});

export async function logout(){
    if(firebase.auth().currentUser){
        await firebase.auth().signOut();
    }
    await AsyncStorage.removeItem('@user');
}
