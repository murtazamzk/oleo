let baseUrl = "https://us-central1-oleo-6b2af.cloudfunctions.net/";

export default class Api{
    constructor(){

    }
    postData(data) {
        return fetch(baseUrl + "app/setuserdetails", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: data
        });
    }

    learningCats(userid){
        return fetch(baseUrl + "app/learning/", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "user": userid
            })
        });
    }

    addReport(data){
        return fetch(baseUrl + "app/reportActivity", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: data
        });
    }

    updateLearning(data){
        return fetch(baseUrl + "app/updateLearning", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: data
        });
    }

    likeSocial(data){
        return fetch(baseUrl + "app/likesocial", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: data
        });
    }

    isLearningCompleted(data){
        return fetch(baseUrl + "app/isLearningCompleted", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: data
        });
    }

    updateChallenge(data) {
        return fetch(baseUrl + "app/updatechallengestatus", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: data
        });
    }

    getReports(userid){
        return fetch(baseUrl + "app/listActivities", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "user": userid
            })
        });
    }

    verifyUser(phone, password) {
        return fetch(baseUrl + "verifyUser", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "phone": phone,
                "password": password,
                "apikey": "72DF48D8-F1E2-4CC4-9EF3-283D3AC04421"
            })
        });
    }

    leaderBoard(){
        return fetch(baseUrl + "admin/leaderboard", {
            
        });
    }

    myLeaderBoard(managerid){
        return fetch(baseUrl + "admin/myleaderboard", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "managerid":managerid
            })
        });
    }

    

    changePwd(phone,password) {
        return fetch(baseUrl + "setpassword", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "phone": phone,
                "password": password,
                "apikey": "72DF48D8-F1E2-4CC4-9EF3-283D3AC04421"
            })
        });
    }

    createUser(phone, email) {
        return fetch(baseUrl + "createUser", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "mobile": phone,
                "emailId": email,
            })
        });
    }
}