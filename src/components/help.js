import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, Dimensions, ActivityIndicator, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import moment from 'moment';
import APILIB from '../api';
import AsyncStorage from '@react-native-community/async-storage';
import { Collapse, CollapseHeader, CollapseBody, AccordionList } from "accordion-collapse-react-native";

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    helpWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10,
    },
    helpHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        paddingVertical: 20,
        paddingHorizontal:20,
        position: 'relative',
        paddingTop: 40,
    },
    headerLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'left',
        flex: 1,
        fontFamily: Fonts.PoppinsRegular,
    },
    goBackIconWrapper: {
        marginRight: 30,
        paddingHorizontal: 15,
        paddingVertical: 10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    helpContent:{
        width:'90%',
        alignSelf:'center',
        flex:1,
        marginBottom:30,
    },
    helpIcon:{
        width:45,
        height:45,
        marginRight:15,
    },
    helpTitleWrapper:{
        paddingVertical:25,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        borderBottomColor:Colors.bgBlue,
        borderBottomWidth:1,
    },
    helpTitle:{
        color: '#ffffff',
        fontSize: 16,
        textAlign: 'left',
        flex: 12,
        fontFamily: Fonts.PoppinsRegular,
        marginRight:20,
    },
    helpDescWrapper:{
        paddingTop:15,
        paddingBottom:25,
        borderBottomColor:Colors.bgBlue,
        borderBottomWidth:1,
    },
    helpDesc:{
        color: '#c3c7c9',
        fontSize: 14,
        textAlign: 'left',
        flex: 1,
        fontFamily: Fonts.PoppinsRegular,
        paddingLeft:60,
        marginTop:-40,
        backgroundColor:Colors.secondary,
    },
    helpArrow:{
        width:20,
        height:20,
        marginTop:10,
    },
    helpArrowDown:{
        transform:[{rotate:'180deg'}]
    }
});

export default class help extends Component {

    constructor(props) {
        super(props);
        this.state = {
            helplist : [
                {
                    title: 'Functionality',
                    icon:require('../../assets/images/help-functionality.png'),
                    body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et',
                    open:false,
                },
                {
                    title: 'Operations',
                    icon:require('../../assets/images/help-operations.png'),
                    body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et',
                    open:false,
                },
                {
                    title: 'Prizes',
                    icon:require('../../assets/images/help-prizes.png'),
                    body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et',
                    open:false,
                },
                {
                    title: 'Coins',
                    icon:require('../../assets/images/help-functionality.png'),
                    body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et',
                    open:false,
                }
            ]
        }
        this._isMounted = false;
    }

    toggleHelp(index){
        let helplist = this.state.helplist;
        helplist[index].open = !helplist[index].open;
        this.setState({
            helplist
        });
    }

    _head(item){
        return(
            <View style={{...styles.helpTitleWrapper,borderBottomWidth:item.open ? 0 : 1 }}>
                <Image source={item.icon} style={styles.helpIcon} />
                <Text style={styles.helpTitle}>{item.title}</Text>
                <Image style={ item.open ? {...styles.helpArrow,...styles.helpArrowDown} : styles.helpArrow } source={require('../../assets/images/faqs-arrow.png')} />
            </View>
        );
    }
    
    _body(item){
        return (
            <View style={styles.helpDescWrapper}>
              <Text style={styles.helpDesc}>{item.body}</Text>
            </View>
        );
    }

    render() {
        return (
            <SafeAreaView style={styles.wrapper}>
                <View style={styles.helpWrapper}>
                    <View style={styles.helpHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                        </TouchableOpacity>
                        <Text style={styles.headerLabel}>Help</Text>
                    </View>
                    <View style={styles.helpContent}>

                    <AccordionList
                        extraData={this.state.helplist}
                        list={this.state.helplist}
                        header={this._head}
                        body={this._body}
                        onToggle={this.toggleHelp.bind(this)}
                    />
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}
