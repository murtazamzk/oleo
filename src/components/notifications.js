import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, Dimensions, ActivityIndicator, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import moment from 'moment';
import APILIB from '../api';
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    notifWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10,
    },
    notifHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf:'center',
        paddingVertical: 20,
        position: 'relative',
        width: '90%',
        paddingTop:40,
    },
    headerLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsRegular,
    },
    goBackIconWrapper: {
        marginRight: 30,
        paddingHorizontal:15,
        paddingVertical:10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    ddMenu:{

    },
    ddMenuIcon: {
        width: 5,
        height: 22,
    },
    notifications:{
        flex:1,
    },
    notifItem:{
        width:'90%',
        alignSelf:'center',
        paddingVertical:20,
        borderBottomColor:'#243641',
        borderBottomWidth:1,
    },
    notifLabel:{
        color: '#c3c7c9',
        fontSize: 16,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsMedium,
        marginBottom:10,
    },
    notifTime:{
        color: '#638599',
        fontSize: 14,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsRegular,
    }
});

export default class notifications extends Component {

    constructor(props){
        super(props);
    }

    render(){
        return(
            <SafeAreaView style={styles.wrapper}>
                <View style={styles.notifWrapper}>
                    <View style={styles.notifHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                        </TouchableOpacity>
                        <Text style={styles.headerLabel}>Notifications</Text>
                        <TouchableOpacity style={styles.ddMenu}>
                            <Image style={styles.ddMenuIcon} source={require('../../assets/images/icon-dd.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.notifications}>
                    <ScrollView style={{ flex: 1 }} contentContainerStyle={{width:'90%',alignSelf:'center'}}>
                        <TouchableOpacity style={styles.notifItem}>
                            <Text style={styles.notifLabel}>Welcome to Oleo, help us get to know you better and earn your first gem</Text>
                            <Text style={styles.notifTime}>Now</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.notifItem}>
                            <Text style={styles.notifLabel}>Welcome to Oleo, help us get to know you better and earn your first gem</Text>
                            <Text style={styles.notifTime}>Now</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.notifItem}>
                            <Text style={styles.notifLabel}>Welcome to Oleo, help us get to know you better and earn your first gem</Text>
                            <Text style={styles.notifTime}>Now</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.notifItem}>
                            <Text style={styles.notifLabel}>Welcome to Oleo, help us get to know you better and earn your first gem</Text>
                            <Text style={styles.notifTime}>Now</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.notifItem}>
                            <Text style={styles.notifLabel}>Welcome to Oleo, help us get to know you better and earn your first gem</Text>
                            <Text style={styles.notifTime}>Now</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.notifItem}>
                            <Text style={styles.notifLabel}>Welcome to Oleo, help us get to know you better and earn your first gem</Text>
                            <Text style={styles.notifTime}>Now</Text>
                        </TouchableOpacity>
                    </ScrollView>
                    </View>
                </View>
            </SafeAreaView>
        )
    }

}