import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, Dimensions, ActivityIndicator, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import moment from 'moment';
import APILIB from '../api';
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    settingsWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10,
    },
    settingsHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf:'center',
        paddingVertical: 20,
        position: 'relative',
        width: '90%',
        paddingTop:40,
    },
    headerLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsRegular,
    },
    goBackIconWrapper: {
        marginRight: 30,
        paddingHorizontal:15,
        paddingVertical:10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    contentWrapper: {
        flex: 1,
        width: '90%',
        alignSelf: 'center',
        marginTop: 20,
    },
    settingsLabel:{
        color: '#ffffff',
        fontSize: 16,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsRegular,
        marginBottom:5,
    },
    settingsDesc:{
        color: '#5389a6',
        fontSize: 14,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsRegular,
        maxWidth:'80%',
    },
    popupBtn:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    language:{
        position:'relative',
        paddingRight:30,
    },
    languageLabel:{
        color: '#5389a6',
        fontSize: 14,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsRegular,
    },
    arrowIcon:{
        position:'absolute',
        right:0,
        top:3,
        width: 8,
        height: 14
    }
});

export default class settings extends Component {

    constructor(props){
        super(props);
    }

    render(){
        return(
            <SafeAreaView style={styles.wrapper}>
                <View style={styles.settingsWrapper}>
                    <View style={styles.settingsHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                        </TouchableOpacity>
                        <Text style={styles.headerLabel}>Settings</Text>
                    </View>
                    <ScrollView style={styles.contentWrapper} contentContainerStyle={{justifyContent:'flex-start'}}>
                    <View style={Commonstyles.contentBox}>
                        <Text style={styles.settingsLabel}>Notification</Text>
                        <Text style={styles.settingsDesc}>Push notifications on your locked screen will be turned off</Text>
                    </View>
                    <View style={Commonstyles.contentBox}>
                        <TouchableOpacity style={styles.popupBtn}>
                            <Text style={styles.settingsLabel}>Language</Text>
                            <View style={styles.language}>
                                <Text style={styles.languageLabel}>English</Text>
                                <Image style={styles.arrowIcon} source={require('../../assets/images/arrowRightGreen.png')} />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={Commonstyles.contentBox}>
                        <TouchableOpacity style={{...styles.popupBtn,position:'relative'}}>
                            <Text style={styles.settingsLabel}>App Tour</Text>
                            <Image style={styles.arrowIcon} source={require('../../assets/images/arrowRightGreen.png')} />
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}