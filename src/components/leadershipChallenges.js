import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, BackHandler, ActivityIndicator, Dimensions, ImageBackground, TouchableOpacity, TouchableHighlight, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import AsyncStorage from '@react-native-community/async-storage';
import ProfileBox from './profileBox';
import ImagePicker from 'react-native-image-picker';
import APILIB from '../api';
import SortableList from 'react-native-sortable-list';
import Row from './sortRow';

const { width, height } = Dimensions.get('screen');

const sortOptions = {
    0: {
      text: 'Chloe'
    },
    1: {
      text: 'Jasper',
    },
    2: {
      text: 'Pepper',
    },
    3: {
      text: 'Oscar',
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    leadershipWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10,
    },
    leadershipHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        paddingVertical: 20,
        position: 'relative',
        width: '90%',
        paddingTop: 40,
    },
    goBackIconWrapper: {
        marginRight: 30,
        paddingHorizontal:15,
        paddingVertical:10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    headerLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'left',
        flex: 1,
        fontFamily: Fonts.PoppinsRegular,
    },
    leadershipContent: {
        flex: 1,
        width: '90%',
        alignSelf: 'center',
        marginTop: 20
    },
    notifWrapper: {

    },
    notifIcon: {
        width: 25,
        height: 33,
    },
    borderLine: {
        width: '100%',
        height: 1,
        backgroundColor: '#27414f',
        marginBottom: 10
    },
    dialog: {
        backgroundColor: 'transparent',
        marginTop: 70,
    },
    dialogContent: {
        backgroundColor: '#ffffff',
        // justifyContent: 'space-between',
        borderRadius: 12,
        paddingVertical: 15,
        paddingHorizontal: 20,
    },
    dialogQuestion: {
        fontFamily: Fonts.PoppinsRegular,
        fontSize: 18,
        marginTop: 20,
        marginBottom:10,
    },
    optionsWrapper: {
        paddingBottom:20,
    },
    option: {
        paddingVertical: 12,
        paddingHorizontal: 20,
        // backgroundColor: '#eceef0',
        borderRadius: 100,
        position:'relative',
    },
    optionText: {
        color: '#7a848b',
        fontSize: 14,
        fontFamily: Fonts.PoppinsRegular,
        marginLeft: 5,
    },
    optionTextActive: {
        color: '#000000'
    },
    radioBtnWrapper: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 30
    },
    checkBox:{
        ...Commonstyles.radioBtn,
    },
    checkBoxSquare:{
        ...Commonstyles.radioBtnCircle,
        borderRadius:0,
    },
    checkBoxInner:{
        ...Commonstyles.radioBtnCircleInner,
        borderRadius:0
    },
    checkBoxActive: {
        borderColor: Colors.bgBlue,
    },
    dialogFooter: {
        borderTopWidth: 1,
        borderTopColor: "#e6e9eb",
        paddingTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop:'auto'
    },
    userPrizes: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    rewardLabel: {
        fontSize: 12,
        color: '#848c92',
        marginRight: 7,
    },
    prizeBoxDialog: {
        backgroundColor: '#e6e9eb',
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    prizeIconDialog: {
        width: 15,
        height: 15,
    },
    prizeTextDialog: {
        color: '#081d29',
        fontSize: 10,
        marginRight: 5,
        marginTop:2,
        fontFamily: Fonts.PoppinsRegular,
    },
    btnPrimary: {
        borderRadius: 20,
        backgroundColor: Colors.primary,
        height: 50,
        width: 130,
        justifyContent: 'center'
    },
    btnPrimaryText: {
        fontSize: 14,
        textAlign: 'center',
        color: Colors.secondary,
        textTransform: 'uppercase',
        fontFamily: Fonts.PoppinsBold,
        letterSpacing: 2,
    },
    dialogCloseBtn: {
        alignSelf: 'center',
        marginTop: 50,
    },
    dialogCloseIcon: {
        width: 50,
        height: 55,
    },
    rightDialogHead: {
        paddingTop: 30,
    },
    rightDialogContent: {
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    rightDialogTitle: {
        fontFamily: Fonts.PoppinsBold,
        fontSize: 28,
        marginBottom: 7,
        textAlign: 'center',
    },
    rightDialogSubTitle: {
        fontFamily: Fonts.PoppinsRegular,
        fontSize: 16,
        color: '#6c767e',
        textAlign: 'center',
    },
    coinWrapper: {
        width: 150,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 15,
    },
    pointsText: {
        fontSize: 50,
        textAlign: 'center',
        fontFamily: Fonts.PoppinsBold,
        color: '#744900',
        marginTop: 10,
    },
    tipText: {
        color: '#848c92',
        fontFamily: Fonts.PoppinsRegular,
        textAlign: 'center',
    },
    wrongDialogContent: {
        alignItems: 'center',
    },
    wrongAnswerIcon: {
        width: 100,
        height: 100,
        marginVertical: 35,
    },
    wrongDialogTitle: {
        fontFamily: Fonts.PoppinsBold,
        fontSize: 28,
        marginBottom: 7,
        textAlign: 'center',
    },
    wrongDialogSubTitle: {
        fontFamily: Fonts.PoppinsRegular,
        fontSize: 14,
        color: '#6c767e',
        textAlign: 'center',
    },
    questionBoxWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    questionBox: {
        backgroundColor: '#0b2432',
        padding: 30,
        borderWidth: 1,
        borderColor: Colors.primary,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        width: width / 2 - 30,
        height: 170,
        marginBottom: 20,
        position: 'relative',
        overflow: 'hidden'
    },
    questionBoxContentWrapper: {
        alignItems: 'center',
    },
    questionBoxWon: {

    },
    questionBoxWonIcon: {
        width: 100,
        height: 46,
        marginBottom: -5,
    },
    questionBoxWonText: {
        fontFamily: Fonts.PoppinsRegular,
        fontSize: 16,
        marginBottom: 7,
        color: '#b5d6e9',
        textAlign: 'center',
    },
    questionBoxBetterLuck: {
        fontFamily: Fonts.PoppinsRegular,
        fontSize: 16,
        marginBottom: 7,
        color: '#b5d6e9',
        textAlign: 'center',
    },
    prizeBox: {
        backgroundColor: Colors.secondary,
        paddingVertical: 7,
        paddingHorizontal: 15,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    prizeIcon: {
        width: 15,
        height: 15,
        marginTop: -3,
    },
    prizeText: {
        color: Colors.primary,
        fontSize: 15,
        marginLeft: 5,
        fontFamily: Fonts.PoppinsRegular,
    },
    questionBoxGiftWrapper: {
        position: 'absolute',
        backgroundColor: Colors.primary,
        width: width / 2 - 30,
        height: 170,
        top: 0,
        left: 0,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1,
    },
    questionBoxGiftIcon: {
        width: 80,
        height: 91
    },
    sortIndex:{
        color: '#000000',
        fontSize: 14,
        width:12,
        lineHeight:30,
        fontFamily: Fonts.PoppinsRegular,
    },
    sortSep:{
        width:1,
        height:30,
        backgroundColor:'#a7afb4',
        marginHorizontal:10,
    },
    sortIcon:{
        width:25,
        height:25,
    },
    answerTextAreaWrapper:{
        backgroundColor:'#eceef0',
        paddingVertical:10,
        borderRadius:20,
    },
    answerTextArea:{
        color: '#000000',
        fontSize: 14,
        paddingVertical:50,
        paddingHorizontal:15,
        fontFamily: Fonts.PoppinsRegular,
    },
    selfieBtn:{
        backgroundColor:'#eceef0',
        paddingVertical:50,
        borderRadius:20,
        alignItems:'center',
        marginVertical:10,
        width:'80%',
        alignSelf:'center',
    },
    selfieBtnIcon:{
        width:60,
        height:50,
        marginBottom:20,
    },
    selfieImage:{
        width:200,
        height:150,
        marginBottom:20,
    },
    selfieBtnText:{
        color: Colors.bgBlue,
        fontSize: 16,
        fontFamily: Fonts.PoppinsMedium,
        textAlign:'center'
    }
});

export default class leadershipChallenges extends Component {
    constructor(props) {
        super(props);
        this.state = {
            questionDialog: false,
            questionRightDialog: false,
            questionWrongDialog: false,
            questions: [
                { "opened": false },
                { "opened": false }
            ],
            openedQuestion: null,
            openedQuestionDocId:'',
            selectedChoice:null,
            selectedChoices:[],
            correctAnswer:'',
            correctAnswers:[],
            textAnswer:'',
            anyAnswers:[],
            selfieUploaded:false,
            userSelfie:null,
            userSelfieData:'',
            reward:0,
            loading:false,
            dialogLoading:false,
            openedQuestionIndex:-1,
            userData:{},
            sortOptions:[...Array(4)].map((d, index) => ({
                key: `item-${index}`,
                label: index,
            }))
        }
        this._isMounted = false;
    }
    openDialog(dialogId) {
        this.setState({
            [dialogId]: true
        })
    }
    closeDialog(dialogId) {
        this.setState({
            [dialogId]: false,
            openedQuestion: null,
            openedQuestionDocId:'',
            selectedChoice:null,
            selectedChoices:[],
            textAnswer:'',
            selfieUploaded:false,
            userSelfie:null,
            userSelfieData:'',
        });
    }

    selectSelfie() {
        this.setState({
            dialogLoading: true,
        })
        const options = {
            title: 'Select Selfie',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                this.setState({
                    dialogLoading: false,
                });
            } else if (response.error) {
                this.setState({
                    dialogLoading: false,
                });
            } else if (response.customButton) {
                this.setState({
                    dialogLoading: false,
                });
            } else {
                const source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    dialogLoading: false,
                    userSelfie: source,
                    userSelfieData: response.data,
                });
            }
        });
    }

    changeSortOrder(order){
        console.log(order);
    }

    async validateAnswer(type){
        console.log(type);
        
        switch (type) {
            case 1 :
                this.submitTypeOne();
                return null;
            case 2 :
                this.submitTypeTwo();
                return null;
            case 3 : 
                this.submitTypeThree();
                return null;
            case 4 : 
                this.submitTypeFour();
                return null;
            case 5 :
                this.submitTypeFive();
                return null;
            default :
                this.closeDialog('questionDialog');
                this.openDialog('questionRightDialog');
        }
    }

    submitTypeFive(){
        if(this.state.selectedChoice){
            this.closeDialog('questionDialog');
            let questions = [...this.state.questions];
            questions[this.state.openedQuestionIndex].attempted = true;
            questions[this.state.openedQuestionIndex].rewards = this.state.openedQuestion.rewards;
            this.openDialog('questionRightDialog');
            var api = new APILIB();
            let data = {
                "user": this.props.navigation.getParam('userid'),
                "topic": this.state.openedQuestion.topic,
                "iscompleted": true,
                "docId": this.state.openedQuestionDocId,
                "managerid": this.props.navigation.getParam('managerid'),
                "checklist": true,
                "data": {
                    "reward": this.state.openedQuestion.rewards,
                    "priority": this.state.openedQuestion.priority,
                    "sub_cat": this.state.openedQuestion.sub_cat,
                    "type": this.state.openedQuestion.type
                }
            }
            this.setState({
                loading: true,
            });
            let fetchResponse = api.updateChallenge(JSON.stringify(data));
            fetchResponse.then(response => {
                response.json().then(res => {
                    console.log('five');
                    this.setState({
                        questions,
                        loading:false
                    });
                });
            }).catch(error => {
                console.log(error);
            });
        }
    }

    submitTypeFour(){
        if(this.state.selectedChoice){
            this.closeDialog('questionDialog');
            let questions = [...this.state.questions];
            questions[this.state.openedQuestionIndex].attempted = true;
            if(this.state.selectedChoice === this.state.openedQuestion.answer){
                questions[this.state.openedQuestionIndex].rewards = this.state.openedQuestion.rewards;
                this.openDialog('questionRightDialog');
                var api = new APILIB();
                let data = {
                    "user": this.props.navigation.getParam('userid'),
                    "topic": this.state.openedQuestion.topic,
                    "iscompleted": true,
                    "docId": this.state.openedQuestionDocId,
                    "managerid": this.props.navigation.getParam('managerid'),
                    "checklist": true,
                    "data": {
                        "reward": this.state.openedQuestion.rewards,
                        "priority": this.state.openedQuestion.priority,
                        "sub_cat": this.state.openedQuestion.sub_cat,
                        "type": this.state.openedQuestion.type
                    }
                }
                this.setState({
                    loading: true,
                });
                let fetchResponse = api.updateChallenge(JSON.stringify(data));
                fetchResponse.then(response => {
                    response.json().then(res => {
                    });
                }).catch(error => {
                    console.log(error);
                });
            }else{
                this.openDialog('questionWrongDialog');
                this.setState({
                    loading: true,
                });
                var api = new APILIB();
                let data = {
                    "user": this.props.navigation.getParam('userid'),
                    "topic": this.state.openedQuestion.topic,
                    "iscompleted": false,
                    "docId": this.state.openedQuestionDocId,
                    "managerid": this.props.navigation.getParam('managerid'),
                    "checklist": true,
                    "data": {
                        "reward": this.state.openedQuestion.rewards,
                        "priority": this.state.openedQuestion.priority,
                        "sub_cat": this.state.openedQuestion.sub_cat,
                        "type": this.state.openedQuestion.type
                    }
                }
                let fetchResponse = api.updateChallenge(JSON.stringify(data));
                fetchResponse.then(response => {
                    response.json().then(res => {
                    });
                }).catch(error => {
                    console.log(error);

                });
            }
            console.log('four');
            
            this.setState({
                questions,
                loading:false
            });
        }
    }

    submitTypeThree(){
        if(this.state.selectedChoices.length > 0 ){
            this.closeDialog('questionDialog');
            let questions = [...this.state.questions];
            questions[this.state.openedQuestionIndex].attempted = true;
            if(this.arraysEqual(this.state.selectedChoices,this.state.anyAnswers)){
                questions[this.state.openedQuestionIndex].rewards = this.state.openedQuestion.rewards;
                this.openDialog('questionRightDialog');
                var api = new APILIB();
                let data = {
                    "user": this.props.navigation.getParam('userid'),
                    "topic": this.state.openedQuestion.topic,
                    "iscompleted": true,
                    "docId": this.state.openedQuestionDocId,
                    "managerid": this.props.navigation.getParam('managerid'),
                    "checklist": true,
                    "data": {
                        "reward": this.state.openedQuestion.rewards,
                        "priority": this.state.openedQuestion.priority,
                        "sub_cat": this.state.openedQuestion.sub_cat,
                        "type": this.state.openedQuestion.type
                    }
                }
                this.setState({
                    loading: true,
                });
                let fetchResponse = api.updateChallenge(JSON.stringify(data));
                fetchResponse.then(response => {
                    response.json().then(res => {
                    });
                }).catch(error => {
                    console.log(error);
                });
            }else{
                this.openDialog('questionWrongDialog');
                this.setState({
                    loading: true,
                });
                var api = new APILIB();
                let data = {
                    "user": this.props.navigation.getParam('userid'),
                    "topic": this.state.openedQuestion.topic,
                    "iscompleted": false,
                    "docId": this.state.openedQuestionDocId,
                    "managerid": this.props.navigation.getParam('managerid'),
                    "checklist": true,
                    "data": {
                        "reward": this.state.openedQuestion.rewards,
                        "priority": this.state.openedQuestion.priority,
                        "sub_cat": this.state.openedQuestion.sub_cat,
                        "type": this.state.openedQuestion.type
                    }
                }
                let fetchResponse = api.updateChallenge(JSON.stringify(data));
                fetchResponse.then(response => {
                    response.json().then(res => {
                    });
                }).catch(error => {
                    console.log(error);

                });
            }
            console.log('three');
            
            this.setState({
                questions,
                loading:false
            });
        }
    }

    submitTypeTwo(){
        if(this.state.userSelfie){
            let questions = [...this.state.questions];
            questions[this.state.openedQuestionIndex].attempted = true;
            this.setState({
                dialogLoading: true,
            });
            const uploadTask = firebase.storage().ref('challenges/'+this.state.userData.user+'/').child(this.state.openedQuestionDocId + '.jpg').putString(this.state.userSelfieData, 'base64', { contentType: 'image/jpeg' });
            uploadTask.then(
                (response) => {
                    questions[this.state.openedQuestionIndex].rewards = this.state.openedQuestion.rewards;
                    var api = new APILIB();
                    let data = {
                        "user": this.props.navigation.getParam('userid'),
                        "topic": this.state.openedQuestion.topic,
                        "iscompleted": true,
                        "docId": this.state.openedQuestionDocId,
                        "managerid": this.props.navigation.getParam('managerid'),
                        "checklist": true,
                        "data": {
                            "reward": this.state.openedQuestion.rewards,
                            "priority": this.state.openedQuestion.priority,
                            "sub_cat": this.state.openedQuestion.sub_cat,
                            "type": this.state.openedQuestion.type
                        }
                    }
                    let fetchResponse = api.updateChallenge(JSON.stringify(data));
                    fetchResponse.then(response => {
                        response.json().then(res => {
                            this.closeDialog('questionDialog');
                            this.openDialog('questionRightDialog');
                            this.setState({
                                dialogLoading: false,
                            });
                        });
                    }).catch(error => {
                        console.log(error);
                    });
                },
                (failedReason) => {
                    alert(failedReason);
                    this.closeDialog('questionDialog');
                    this.setState({
                        loading: false,
                    });
                }
            );
            this.setState({
                questions,
                loading:false
            });
        }
    }

    submitTypeOne(){
        if(this.state.textAnswer.length > 0 ){
            this.closeDialog('questionDialog');
            let questions = [...this.state.questions];
            questions[this.state.openedQuestionIndex].attempted = true;
            questions[this.state.openedQuestionIndex].rewards = this.state.openedQuestion.rewards;
            this.openDialog('questionRightDialog');
            var api = new APILIB();
            let data = {
                "user": this.props.navigation.getParam('userid'),
                "topic": this.state.openedQuestion.topic,
                "iscompleted": true,
                "textAnswer": this.state.textAnswer,
                "docId": this.state.openedQuestionDocId,
                "managerid": this.props.navigation.getParam('managerid'),
                "checklist": true,
                "data": {
                    "reward": this.state.openedQuestion.rewards,
                    "priority": this.state.openedQuestion.priority,
                    "sub_cat": this.state.openedQuestion.sub_cat,
                    "type": this.state.openedQuestion.type
                }
            }
            this.setState({
                loading: true,
            });
            let fetchResponse = api.updateChallenge(JSON.stringify(data));
            fetchResponse.then(response => {
                response.json().then(res => {
                    console.log('one');
                    this.setState({
                        questions,
                        loading:false
                    });
                });
            }).catch(error => {
                console.log(error);
            });
        }
    }

    arraysEqual(a, b) {
        if (a === b) return true;
        if (a == null || b == null) return false;
        if (a.length != b.length) return false;
        for (var i = 0; i < a.length; ++i) {
          if (a[i] !== b[i]) return false;
        }
        return true;
      }

    async openQuestion(e, index) {
        let questions = [...this.state.questions];
        let collection = questions[index].question;
        this.setState({
            questionDialog: true,
        });
        const documentSnapshot = await firestore()
            .doc(collection)
            .get();
        let db = documentSnapshot;
        if(this.state.questionDialog){
            this.setState({
                openedQuestion: db.data(),
                openedQuestionDocId:db.id,
                openedQuestionIndex:index,
                correctAnswer:db.data().answer || null,
                correctAnswers:db.data().answers || [],
                anyAnswers:db.data().answers || [],
                reward:db.data().rewards
            });
        }
    }

    wonComp = (awardPoints) => <View style={styles.questionBoxContentWrapper}>
        <Image style={styles.questionBoxWonIcon} source={require('../../assets/images/icon-fireworks.png')} />
        <Text style={styles.questionBoxWonText}>You've Won</Text>
        <View style={styles.prizeBox}>
            <Image style={styles.prizeIcon} source={require('../../assets/images/icon-coin.png')} />
            <Text style={styles.prizeText}>+ {awardPoints}</Text>
        </View>
    </View>

    betterLuckComp = () => <View style={styles.questionBoxContentWrapper}>
        <Text style={styles.questionBoxBetterLuck}>Better Luck Next Time</Text>
    </View>

    async componentDidMount() {
        this._isMounted = true;
        await this.getData();
        const { navigation } = this.props;
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            if(this.state.questionWrongDialog){
                this.setState({
                    questionWrongDialog:false
                });
                return true;
            }else if(this.state.questionRightDialog){
                this.setState({
                    questionRightDialog:false
                });
                return true;
            }else if(this.state.questionDialog){
                this.setState({
                    questionDialog:false
                });
                return true;
            }else{
                // this.props.navigation.navigate('leadershipLanding');
            }
        });
        if (navigation.getParam('challenges')) {
            let challenges = [];
            navigation.getParam('challenges').map((value, index) => {
                challenges.push({
                    "question": value,
                    "attempted": false,
                    "rewards":0
                });
            });
            await this.setState({
                questions: challenges
            });
        }
        if (navigation.getParam('attemptedChallenges')){
            let obj = navigation.getParam('attemptedChallenges');
            let questions = this.state.questions;
            if(obj){
                Object.keys(obj).map((value,index) => {
                    questions.map((val,i) => {
                        if (value === val.question.split('/')[3]){
                            questions[i].attempted = true;
                            if (obj[value].iscompleted){
                                questions[i].rewards = obj[value].reward;
                            }else{
                                questions[i].rewards = 0;
                            }
                        }
                    });
                });
                console.log('didMount');
                
                this.setState({
                    questions
                });
            }
        }
    }

    async getData() {
        try {
            const value = await AsyncStorage.getItem('@user')
            if (value !== null) {
                if(this._isMounted){
                    this.setState({
                        userData: JSON.parse(value)
                    });
                }
            }
        } catch (e) {
            console.log(e);
        }
    }

    toggleCheckbox(value){
        let choices = this.state.selectedChoices;
        if(choices.includes(value)){
            choices.splice(choices.indexOf(value),1);
        }else{
            choices.push(value);
        }
        this.setState({
            selectedChoices:choices,
        });
    }

    renderQuestion(type){
        let userImageUri = this.state.userSelfie;
        switch (type) {
            case 1 : 
                return <View style={styles.answerTextAreaWrapper}>
                    <TextInput style={styles.answerTextArea} placeholder={"Type answer"} placeholderTextColor={"#5c6f7a"} blurOnSubmit={true} returnKeyType={"done"} returnKeyLabel={"Done"} value={this.state.textAnswer} multiline={true} numberOfLines={5} onChangeText={(value) => this.setState({textAnswer:value}) } ></TextInput>
                </View>
            case 2 : 
                return <TouchableOpacity onPress={() => this.selectSelfie()} style={styles.selfieBtn}>
                    {userImageUri ? <Image style={styles.selfieImage} source={userImageUri} /> : 
                    <Image style={styles.selfieBtnIcon} source={require('../../assets/images/icon-upload.png')} /> }
                    <Text style={styles.selfieBtnText}>UPLOAD AN IMAGE</Text>
                </TouchableOpacity>
            case 3 :
                return <View>
                    {this.state.openedQuestion.choice.map((value,index) => (
                    <TouchableOpacity onPress={() => this.toggleCheckbox(value)} key={index} style={{ marginBottom: 12 }}>
                        <ImageBackground style={styles.option} resizeMode={'contain'} source={require('../../assets/images/radioOptionBg.png')}>
                            <View style={{ ...styles.checkBox, marginRight: 40 }}>
                                <View style={this.state.selectedChoices.includes(value) ? { ...styles.checkBoxSquare } : { ...styles.checkBoxSquare,...styles.checkBoxActive }}>
                                    {this.state.selectedChoices.includes(value) ? <View style={styles.checkBoxInner} ></View> : null}
                                </View>
                                <Text style={{ ...styles.optionText, ...styles.optionTextActive }}>{value}</Text>
                            </View>
                        </ImageBackground>
                    </TouchableOpacity>
                    ))}
                </View>
            case 4 : 
                return <View>
                    {this.state.openedQuestion.choice.map((value,index) => (
                    <TouchableOpacity onPress={() => {
                        this.setState({selectedChoice:value})
                    }} key={index} style={{ marginBottom: 12 }}>
                        <ImageBackground style={styles.option} source={require('../../assets/images/radioOptionBg.png')}>
                            <View style={{ ...Commonstyles.radioBtn, marginRight: 40 }}>
                                <View style={value === this.state.selectedChoice ? { ...Commonstyles.radioBtnCircle,...Commonstyles.radioBtnCircleActive } : { ...Commonstyles.radioBtnCircle }}>
                                    {value === this.state.selectedChoice ? <View style={Commonstyles.radioBtnCircleInner} ></View> : null}
                                </View>
                                <Text style={{ ...styles.optionText, ...styles.optionTextActive }}>{value}</Text>
                            </View>
                        </ImageBackground>
                    </TouchableOpacity>
                    ))}
                </View>
            case 5 :
                return <View>
                {this.state.openedQuestion.choice.map((value,index) => (
                <TouchableOpacity onPress={() => {
                    this.setState({selectedChoice:value})
                }} key={index} style={{ marginBottom: 12 }}>
                    <ImageBackground style={styles.option} source={require('../../assets/images/radioOptionBg.png')}>
                        <View style={{ ...Commonstyles.radioBtn, marginRight: 40 }}>
                            <View style={value === this.state.selectedChoice ? { ...Commonstyles.radioBtnCircle,...Commonstyles.radioBtnCircleActive } : { ...Commonstyles.radioBtnCircle }}>
                                {value === this.state.selectedChoice ? <View style={Commonstyles.radioBtnCircleInner} ></View> : null}
                            </View>
                            <Text style={{ ...styles.optionText, ...styles.optionTextActive }}>{value}</Text>
                        </View>
                    </ImageBackground>
                </TouchableOpacity>
                ))}
            </View>
            case 6 :
                return <View style={{paddingBottom:20,}}>
                    <SortableList
                    style={styles.list}
                    contentContainerStyle={styles.contentContainer}
                    data={sortOptions}
                    renderRow={this._renderRow} 
                    onReleaseRow = {(key, currentOrder) => {
                        this.changeSortOrder(currentOrder)
                    }}
                    />
                    {/* {this.state.openedQuestion.choice.map((value,index) => (
                    <TouchableOpacity key={index} style={{ marginBottom: 0, }}>
                        <ImageBackground style={{...styles.option}} resizeMode={'contain'} source={require('../../assets/images/radioOptionBg.png')}>
                            <View style={{ ...styles.checkBox, marginRight: 40 }}>
                                <Text style={styles.sortIndex}>{index}</Text>
                                <View style={styles.sortSep}></View>
                                <Text style={{ ...styles.optionText, ...styles.optionTextActive, width: (width - 210)  }}>{value}</Text>
                                <Image style={styles.sortIcon} source={require('../../assets/images/icon-sort.png')} />
                            </View>
                        </ImageBackground>
                    </TouchableOpacity>
                    ))} */}
                </View>
            default : 
                return <View><Text>Invalid Question</Text></View>
        } 
    }

    _renderRow = ({key, index, data, disabled, active}) => {
        return <Row data={this.state.anyAnswers[index]} active={active} index={index} />
    }

    render() {
        return (
            <SafeAreaView style={styles.wrapper}>
                <View style={styles.leadershipWrapper}>
                    <View style={styles.leadershipHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                        </TouchableOpacity>
                        <Text style={styles.headerLabel}>Challenges</Text>
                        {/* <TouchableOpacity style={styles.notifWrapper}>
                            <Image style={styles.notifIcon} source={require('../../assets/images/icon-notif.png')} />
                        </TouchableOpacity> */}
                    </View>

                    <Dialog
                        visible={this.state.questionDialog}
                        onTouchOutside={() => this.closeDialog('questionDialog')}
                        dialogStyle={{...styles.dialog}}
                        width={width - 30}
                        height={null}
                        overlayOpacity={0.5}
                    >
                        <DialogContent>
                            <View style={{...styles.dialogContent,position:'relative'}}>
                            {this.state.dialogLoading &&
                                <View style={{...Commonstyles.loaderWrapper,top:0,height:'110%',width:(width - 30)}}>
                                    <ActivityIndicator color={'#ffffff'} style={Commonstyles.loader}></ActivityIndicator>
                                </View>
                            }
                                {this.state.openedQuestion ?
                                    <Fragment>
                                        <Text style={styles.dialogQuestion}>{this.state.openedQuestion.question}</Text>
                                        <View style={styles.optionsWrapper}>
                                            {this.renderQuestion(this.state.openedQuestion.type)}
                                        </View>
                                        <View style={styles.dialogFooter}>
                                            <View style={styles.userPrizes}>
                                                <Text style={styles.rewardLabel}>Reward</Text>
                                                <View style={styles.prizeBoxDialog}>
                                                    <Text style={styles.prizeTextDialog}>{this.state.openedQuestion.rewards}</Text>
                                                    <Image style={styles.prizeIconDialog} source={require('../../assets/images/icon-coin.png')} />
                                                </View>
                                            </View>
                                            <TouchableOpacity onPress={() => this.validateAnswer(this.state.openedQuestion.type)} style={styles.btnPrimary}>
                                                <Text style={styles.btnPrimaryText}>SUBMIT</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Fragment>
                                    :
                                    <View style={Commonstyles.loaderWrapperInline}>
                                        <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                                    </View>
                                }
                            </View>
                            {/* <TouchableOpacity onPress={() => this.closeDialog('questionDialog')} style={styles.dialogCloseBtn}>
                                <Image style={styles.dialogCloseIcon} source={require('../../assets/images/questionPopupClose.png')} ></Image>
                            </TouchableOpacity> */}
                        </DialogContent>
                    </Dialog>

                    <Dialog
                        visible={this.state.questionRightDialog}
                        onTouchOutside={() => this.closeDialog('questionRightDialog')}
                        dialogStyle={styles.dialog}
                        width={width - 30}
                        height={560}
                        overlayOpacity={0.5}
                    >
                        <DialogContent>
                            <View style={styles.dialogContent}>
                                <View style={styles.rightDialogHead}>
                                    <Text style={styles.rightDialogTitle}>Congratulations</Text>
                                    <Text style={styles.rightDialogSubTitle}>You have earned {this.state.reward} Coins</Text>
                                </View>
                                <View style={styles.rightDialogContent}>
                                    <ImageBackground style={styles.coinWrapper} source={require('../../assets/images/coinReward.png')}>
                                        <Text style={styles.pointsText}>+{this.state.reward}</Text>
                                    </ImageBackground>
                                    <Text style={styles.tipText}>Tip : Earn more points by reporting positive observations</Text>
                                </View>
                                <View style={{ ...styles.dialogFooter, width: '100%' }}>
                                    <TouchableOpacity onPress={() => this.closeDialog('questionRightDialog')} style={{ ...styles.btnPrimary, width: '100%' }}>
                                        <Text style={styles.btnPrimaryText}>CONTINUE</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {/* <TouchableOpacity onPress={() => this.closeDialog('questionRightDialog')} style={styles.dialogCloseBtn}>
                                <Image style={styles.dialogCloseIcon} source={require('../../assets/images/questionPopupClose.png')} ></Image>
                            </TouchableOpacity> */}
                        </DialogContent>
                    </Dialog>

                    <Dialog
                        visible={this.state.questionWrongDialog}
                        onTouchOutside={() => this.closeDialog('questionWrongDialog')}
                        dialogStyle={styles.dialog}
                        width={width - 30}
                        height={560}
                        overlayOpacity={0.5}
                        
                    >
                        <DialogContent>
                            <View style={styles.dialogContent}>
                                <View style={styles.wrongDialogContent}>
                                    <Image source={require('../../assets/images/wrongAnswer.png')} style={styles.wrongAnswerIcon}></Image>
                                    <Text style={styles.wrongDialogTitle}>Wrong Answer!</Text> 
                                        { this.state.correctAnswer !== null ?  
                                            <Text style={styles.wrongDialogSubTitle}>Correct Answer : {this.state.correctAnswer}</Text>
                                            :
                                            null
                                        }
                                </View>
                                <View style={{ ...styles.dialogFooter, width: '100%' }}>
                                    <TouchableOpacity onPress={() => this.closeDialog('questionWrongDialog')} style={{ ...styles.btnPrimary, width: '100%' }}>
                                        <Text style={styles.btnPrimaryText}>CONTINUE</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {/* <TouchableOpacity onPress={() => this.closeDialog('questionWrongDialog')} style={styles.dialogCloseBtn}>
                                <Image style={styles.dialogCloseIcon} source={require('../../assets/images/questionPopupClose.png')} ></Image>
                            </TouchableOpacity> */}
                        </DialogContent>
                    </Dialog>
                    {this.state.loading &&
                        <View style={{...Commonstyles.loaderWrapper}}>
                            <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                        </View>
                    }
                    <ScrollView style={styles.leadershipContent}>
                        <ProfileBox navigation={this.props.navigation} />

                        <View style={styles.borderLine}></View>
                        <View style={styles.questionBoxWrapper}>
                            {this.state.questions.length && this.state.questions.map((value, index) => (
                                <TouchableHighlight key={index} disabled={value.attempted} onPress={event => this.openQuestion(event, index)} ref={"_question" + index} style={styles.questionBox}>
                                    <Fragment>
                                    {!value.attempted && <View style={styles.questionBoxGiftWrapper}>
                                        <Image style={styles.questionBoxGiftIcon} source={require('../../assets/images/icon-gift.png')} />
                                    </View>}
                                    {value.attempted && (value.rewards ? this.wonComp(value.rewards) : this.betterLuckComp())}
                                    </Fragment>
                                </TouchableHighlight>
                            ))
                            }
                        </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}
