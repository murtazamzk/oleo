
import React, { Component, Fragment } from 'react';
import { View, Image, ImageBackground, Text, StyleSheet } from 'react-native'; 
import firebase from '@react-native-firebase/app';

import { Colors, Commonstyles, Fonts } from '../constants';

const styles = StyleSheet.create({
    userHexBg: {
        position: 'absolute',
        bottom: 10,
        left: 40,
        width: 25,
        height: 25,
        justifyContent: 'center',
    },
    userLevel: {
        color: '#ffffff',
        fontSize: 13,
        marginTop:3,
        fontFamily: Fonts.PoppinsRegular,
        textAlign: 'center',
    },
});

export default class AsyncImage extends Component {

    constructor(props) {
        super(props)
        this.state = { 
            userImage: null
        }
        this._isMounted = false;
    }
    async getProfileImage(uid) {
        const ref = firebase.storage().ref('userImages/' + uid + '.jpg');
        ref.getDownloadURL().then((res) => {
            if(this._isMounted){
                this.setState({
                    userImage: res
                });
            }
        }, () => {
        });
    }
    componentDidMount(){
        this._isMounted = true;
        this.getProfileImage(this.props.uid);
    }
    render() {
        return (
            <View>
                {this.state.userImage ? 
                <View style={{ ...Commonstyles.userAvatar }}>
                    <Image style={Commonstyles.userImage} source={{ uri: this.state.userImage }} />
                    <ImageBackground style={styles.userHexBg} source={require('../../assets/images/hexBg.png')}>
                        <Text style={styles.userLevel}>{this.props.level || 0}</Text>
                    </ImageBackground>
                </View>
                :
                    <View>
                        <Image style={Commonstyles.userImage} source={require('../../assets/images/placeholder.png')} />
                        <ImageBackground style={styles.userHexBg} source={require('../../assets/images/hexBg.png')}>
                            <Text style={styles.userLevel}>{this.props.level || 0}</Text>
                        </ImageBackground>
                    </View>
                }
            </View>
        )
    }
}