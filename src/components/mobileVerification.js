import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, ActivityIndicator, ToastAndroid, TextInput, Dimensions, TouchableOpacity, SafeAreaView, KeyboardAvoidingView } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { Colors, Commonstyles, Fonts } from '../constants';
import auth from '@react-native-firebase/auth';
import firebase from '@react-native-firebase/app';
import md5 from 'md5';
import APILIB from '../api';

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary,
    },
    authHeader: {
        height: 170,
        backgroundColor: Colors.primary,
        borderBottomRightRadius: 50,
        borderBottomLeftRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        width: 120,
        height: 135,
        position: 'absolute',
        top: 100
    },
    formWrapper: {
        paddingTop: 180,
        alignItems: 'center'
    },
    submitBtn: {
        marginTop: 10
    },
    otpWrapper: {
        flexDirection: 'row',
        width: '80%',
        marginBottom: 30
    },
    otpInputWrapper: {
        flex: 1,
        height: 60,
        marginHorizontal: 5,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: Colors.textGrey,
        // paddingVertical: 5
        alignItems: 'center',
    },
    otpInput: {
        flex: 1,
        color: '#ffffff',
        fontSize: 22,
        height: 40,
        lineHeight: 40,
        textAlign: 'center',
        fontFamily: Fonts.PoppinsRegular,
        position: 'relative',
        top: 3,
    },
    otpLabel: {
        position: 'absolute',
        top: 100,
        color: '#ffffff',
        fontSize: 16,
        fontFamily: Fonts.PoppinsRegular,
    },
    resendOTPWrapper: {
        flexDirection: 'row',
        marginTop: 40
    },
    resendOTPLabel: {
        color: '#ffffff',
        fontSize: 16,
        fontFamily: Fonts.PoppinsRegular,
    },
    resendOTPBtnWrapper: {

    },
    resendOTPBtn: {
        color: Colors.primary,
        fontSize: 16,
        fontFamily: Fonts.PoppinsRegular,
    },
    errorText: {
        color: '#ff0000',
        fontFamily: Fonts.PoppinsMedium,
        fontSize: 12,
    }
});

export default class MobileVerification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            otpSent: false,
            otp: [],
            mobileNo: '',
            errors: '',
            loading: false,
            otpVerified: false,
            pwd: '',
            confirmPwd: '',
        }
        this.otpTextInput = [];
        this.confirmation = null;
    }
    renderInputs() {
        const inputs = Array(6).fill(0);
        const txt = inputs.map(
            (i, j) => <View key={j} style={styles.otpInputWrapper}>
                <TextInput
                    style={[styles.otpInput, { borderRadius: 10 }]}
                    keyboardType="numeric"
                    returnKeyType={"done"}
                    value={this.state.otp[j]}
                    onChangeText={v => this.focusNext(j, v)}
                    onKeyPress={e => this.focusPrevious(e.nativeEvent.key, j)}
                    ref={ref => this.otpTextInput[j] = ref}
                />
            </View>
        );
        return txt;
    };
    focusPrevious(key, index) {
        if (key === 'Backspace' && index !== 0)
            this.otpTextInput[index - 1].focus();
    }

    focusNext(index, value) {
        let otps = this.state.otp;
        otps[index] = value;
        this.setState({
            otp: otps
        }, () => console.log(this.state.otp));
        if (index < this.otpTextInput.length - 1 && value) {
            this.otpTextInput[index + 1].focus();
        }
        if (index === this.otpTextInput.length - 1) {
            this.otpTextInput[index].blur();
        }
    }
    componentDidMount() {

    }
    async sendOTP() {
        let phonenoReg = /^\d{10}$/;
        if (this.state.mobileNo.length > 1 && !this.state.mobileNo.match(phonenoReg)) {
            this.setState({
                errors: 'Please Enter Proper Mobile Number without country code'
            });
        } else {
            if (this.state.mobileNo.length == 0) {
                this.setState({
                    errors: 'Please Enter Proper Mobile Number without country code'
                });
            } else {
                if(!this.state.otpSent){
                    this.setState({
                        loading: true,
                        errors:''
                    });
                    var api = new APILIB();
                    api.createUser(this.state.mobileNo, 'murtazamzk@gmail.com').then((response) => {
                        response.json().then(res => {
                            if(res.code == 200){
                                this.setState({
                                    errors:'Mobile Number does not exist in database',
                                    loading:false
                                });
                            }else{
                                this.setState({
                                    errors: '',
                                    otpSent: true,
                                    loading:false,
                                },() => this.sendOTP());
                            }
                        });
                    });
                }else{
                    this.confirmation = firebase.auth()
                        .verifyPhoneNumber('+91' + this.state.mobileNo)
                        .on('state_changed', (phoneAuthSnapshot) => {
                            console.log(phoneAuthSnapshot.state);
                            switch (phoneAuthSnapshot.state) {
                                case firebase.auth.PhoneAuthState.CODE_SENT: // or 'sent'
                                    console.log('code sent');
                                    break;

                                case firebase.auth.PhoneAuthState.ERROR: // or 'error'
                                    ToastAndroid.show(
                                        'Check your internet connection. There is something wrong with that! :)',
                                        ToastAndroid.LONG
                                    );
                                    console.log('error');
                                    break;

                                case firebase.auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT: // or 'timeout'
                                    ToastAndroid.showWithGravity(
                                        'Check your internet connection. There is something wrong with that! :)',
                                        ToastAndroid.SHORT,
                                        ToastAndroid.BOTTOM
                                    );
                                    console.log('error 3');
                                    break;

                                case firebase.auth.PhoneAuthState.AUTO_VERIFIED: // or 'verified'
                                    const { verificationId, code } = phoneAuthSnapshot;
                                    if (code) {
                                        this.setState({
                                            otp: code.split(''),
                                            loading: false,
                                            otpVerified: true,
                                            errors: 'OTP auto verified'
                                        });
                                    } else {
                                        this.setState({
                                            loading: false,
                                            otpVerified: true,
                                            errors: 'OTP already verified you can change password'
                                        });
                                    }
                                    break;
                            }
                        }, (error) => {
                            console.log(error);
                        }, (phoneAuthSnapshot) => {
                            console.log(phoneAuthSnapshot);
                        });
                }
            }
        }
    }
    validateOTP() {
        if (this.state.otpSent) {
            let otp = this.state.otp.length && this.state.otp.join('');
            let otpReg = /^\d{6}$/;
            if (otp.length && !otp.match(otpReg)) {
                this.setState({
                    errors: 'Please enter proper 6 digit otp'
                });
            } else {
                if (this.state.otp.length == 0) {
                    this.setState({
                        errors: 'Please enter proper 6 digit otp'
                    });
                } else {
                    this.setState({
                        loading: true,
                    });
                    this.verifyOTP();
                    // this.setState({
                    //     error: '',
                    //     otpVerified:true,
                    // });
                }
            }
        }
    }

    async verifyOTP() {
        let otp = this.state.otp.length && this.state.otp.join('');
        try {
            this.confirmation.confirm(otp).then((res) => {
                this.setState({
                    loading: false,
                    otpVerified: true,
                });
            }).catch((error) => {
                let errorMsg = '';
                if (error.message.includes('auth/session-expired')) {
                    errorMsg = 'Session expired , resending OTP';
                    this.sendOtp();
                    this.setState({
                        otp: []
                    })
                } else {
                    errorMsg = 'Invalid OTP Entered';
                }
                this.setState({
                    errors: errorMsg,
                    loading: false,
                });
            });
        } catch (e) {
            this.setState({
                error: 'Technical Issue occured',
                loading: false,
            });
            return false;
        }
    }

    updatePassword() {
        var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
        if (this.state.pwd.length > 1 && this.state.confirmPwd.length > 1) {
            if (!this.state.pwd.match(strongRegex)) {
                this.setState({
                    errors: "Password should be 8 characters long and contain 1 UpperCase letter, 1 Numeric and 1 Special Character"
                });
            } else if (this.state.pwd !== this.state.confirmPwd) {
                this.setState({
                    errors: "Passwords are not matching"
                });
            } else {
                this.setState({
                    loading: true,
                });
                var api = new APILIB();
                let fetchResponse = api.changePwd(this.state.mobileNo, md5(this.state.pwd));

                fetchResponse.then(response => {
                    response.json().then((res) => {
                        this.setState({
                            loading: false,
                        });
                        this.props.navigation.navigate('Login', {
                            signup:false,
                            pwdchanged: true
                        });
                    });
                });
            }
        } else {
            if (this.state.pwd == '' || this.state.confirmPwd == '') {
                this.setState({
                    errors: "Password should be 8 characters long and contain 1 UpperCase letter, 1 Numeric and 1 Special Character"
                });
            }
        }
    }

    render() {
        let enterPassword = <View style={{ ...styles.formWrapper, paddingTop: 150 }}>
            <Text style={{ ...styles.errorText, marginBottom: 10, marginTop: -20, paddingHorizontal: 20, }}>{this.state.errors}</Text>
            <TextInput style={Commonstyles.textInput} value={this.state.pwd} onChangeText={(pwd) => { this.setState({ pwd }) }} returnKeyType={"done"} placeholder="Enter your new password" placeholderTextColor={Colors.textGrey} secureTextEntry={true}></TextInput>
            <TextInput style={Commonstyles.textInput} value={this.state.confirmPwd} onChangeText={(confirmPwd) => { this.setState({ confirmPwd }) }} returnKeyType={"done"} placeholder="Confirm new password" placeholderTextColor={Colors.textGrey} secureTextEntry={true}></TextInput>
            <TouchableOpacity onPress={() => this.updatePassword()} style={{ ...Commonstyles.btnPrimary, ...styles.submitBtn }}>
                <Text style={Commonstyles.btnPrimaryText}>
                    Submit
                                    </Text>
            </TouchableOpacity>
        </View>
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: Colors.primary }} />
                <SafeAreaView style={styles.wrapper}>
                    {this.state.loading &&
                        <View style={Commonstyles.loaderWrapper}>
                            <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                        </View>
                    }
                    <KeyboardAvoidingView style={{ flex: 1 }} behavior="position" enabled>
                        <View style={styles.authHeader}>
                            <Image style={styles.logo} source={require('../../assets/images/logo-fill.png')}></Image>
                        </View>
                        {!this.state.otpVerified ?
                            <View style={styles.formWrapper}>
                                {this.state.otpSent &&
                                    <Text style={styles.otpLabel}>Please enter your 6 digit OTP</Text>
                                }
                                <Text style={{ ...styles.errorText, marginBottom: 10, marginTop: -20 }}>{this.state.errors}</Text>
                                {!this.state.otpSent ?
                                    <TextInput style={{ ...Commonstyles.textInput, textAlign: 'center', fontSize: 18 }}
                                        placeholder="Enter your mobile number"
                                        value={this.state.mobileNo}
                                        onChangeText={(mobileNo) => this.setState({ mobileNo: mobileNo })}
                                        placeholderTextColor={Colors.textGrey}
                                        keyboardType={'numeric'}
                                    >
                                    </TextInput>
                                    :
                                    <View style={styles.otpWrapper}>
                                        {this.renderInputs()}
                                    </View>
                                }
                                {!this.state.otpSent ?
                                    <TouchableOpacity onPress={() => this.sendOTP()} style={{ ...Commonstyles.btnPrimary, ...styles.submitBtn }}>
                                        <Text style={Commonstyles.btnPrimaryText}>SUBMIT</Text>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity onPress={() => this.validateOTP()} style={{ ...Commonstyles.btnPrimary, ...styles.submitBtn }}>
                                        <Text style={Commonstyles.btnPrimaryText}>SUBMIT</Text>
                                    </TouchableOpacity>
                                }
                                {this.state.otpSent &&
                                    <View style={styles.resendOTPWrapper}>
                                        <Text style={styles.resendOTPLabel}>Didn't get OTP?</Text>
                                        <TouchableOpacity onPress={() => this.sendOTP()} style={styles.resendOTPBtnWrapper}>
                                            <Text style={styles.resendOTPBtn}> Resend</Text>
                                        </TouchableOpacity>
                                    </View>
                                }
                            </View>
                            :
                            enterPassword}
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </Fragment>
        )
    }
}
