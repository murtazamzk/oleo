import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, Dimensions, ActivityIndicator, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-picker';
import firebase from '@react-native-firebase/app';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    profileWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10,
        paddingTop: 20,
    },
    profileHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        paddingVertical: 20,
        position: 'relative',
        width: '90%'
    },
    goBackIconWrapper: {
        marginRight: 30,
        paddingHorizontal: 15,
        paddingVertical: 10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    headerLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'left',
        flex: 1,
        fontFamily: Fonts.PoppinsRegular,
    },
    profileContent: {
        flex: 1,
        width: '85%',
        alignSelf: 'center',
    },
    userItem: {
        marginBottom: 25,
    },
    label: {
        color: '#ffffff',
        fontSize: 18,
        textAlign: 'left',
        flex: 1,
        fontFamily: Fonts.PoppinsBold,
        marginBottom: 10,
    },
    value: {
        color: Colors.textGrey,
        fontSize: 18,
        textAlign: 'left',
        flex: 1,
        fontFamily: Fonts.PoppinsRegular,
    },
    uploadPhotoWrapper: {
        width: '80%',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20,
        justifyContent: 'flex-start'
    },
    uploadPhotoImg: {
        marginRight: 15,
        width: 70,
        height: 79
    },
    uploadBtnWrapper: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: '#0e2b3b',
        borderRadius: 5
    },
    uploadBtnText: {
        color: Colors.primary,
        fontSize: 10,
        textTransform: 'uppercase',
        fontFamily: Fonts.PoppinsMedium,
    },
    uploadCTA:{
        marginBottom:20,
        width:200,
    }
});

export default class editProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: {},
            userImage: '',
            userImageData: null,
            loading: false,
        }
        this._isMounted = false;
    }
    async componentDidMount() {
        this._isMounted = true;
        const { navigation } = this.props;
        await this.getUserData();
    }
    async getUserData() {
        try {
            const value = await AsyncStorage.getItem('@user')
            if (value !== null) {
                this.setState({
                    userData: JSON.parse(value)
                }, () => {
                    this.getUserImage(this.state.userData.user);
                });
            }
        } catch (e) {
            console.log(e);
        }
    }
    async getUserImage(user) {
        this.setState({
            loading:true,
        });
        const ref = firebase.storage().ref('usersImages/' + user + '.jpg');
        ref.getDownloadURL().then((res) => {
            this.setState({
                userImage: res,
                loading:false,
            });
        }, () => {
            this.setState({
                userImage: '',
                loading:false,
            });
        });
    }
    selectPhoto() {
        this.setState({
            loading: true,
        })
        const options = {
            title: 'Select Photo',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                this.setState({
                    loading: false,
                });
            } else if (response.error) {
                this.setState({
                    loading: false,
                });
            } else if (response.customButton) {
                this.setState({
                    loading: false,
                });
            } else {
                const source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    loading: false,
                    userImage: source,
                    userImageData: response.data,
                });
            }
        });
    }

    uploadPhoto(){
        if (this.state.userImage) {
            this.setState({
                loading: true,
            });
            const uploadTask = firebase.storage().ref('userImages').child(this.state.userData.user + '.jpg').putString(this.state.userImageData, 'base64', { contentType: 'image/jpeg' });
            uploadTask.then(
                (response) => {
                    this.setState({
                        loading: false
                    });
                    alert('Photo Upload Successfully');
                },
                (failedReason) => {
                    this.setState({
                        loading: false
                    });
                }
            );
        } else {
            this.setState({
                loading: false
            });
        }
    }

    render() {
        let userImage = this.state.userImage ? { uri: this.state.userImage.uri } : require('../../assets/images/placeholder.png');
        return (
            <SafeAreaView style={styles.wrapper}>
                <View style={styles.profileWrapper}>
                    {this.state.loading &&
                        <View style={Commonstyles.loaderWrapper}>
                            <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                        </View>
                    }
                    <View style={styles.profileHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                        </TouchableOpacity>
                        <Text style={styles.headerLabel}>Profile Details</Text>
                    </View>
                    <View style={styles.profileContent}>
                        <ScrollView ref="_scrollView" style={{ flex: 1 }} contentContainerStyle={styles.formWrapper}>
                            {this.state.userData &&
                                <React.Fragment>
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Upload New Image</Text>
                                        {/* <Image style={Commonstyles.userImage}  source={userImage} /> */}
                                        <View style={styles.uploadPhotoWrapper}>
                                            <Image style={styles.uploadPhotoImg} source={userImage} />
                                            <TouchableOpacity onPress={() => this.selectPhoto()} style={styles.uploadBtnWrapper}>
                                                <Text style={styles.uploadBtnText}>Select Photo</Text>
                                            </TouchableOpacity>
                                        </View>
                                        <TouchableOpacity onPress={() => this.uploadPhoto()} style={{...styles.uploadBtnWrapper,...styles.uploadCTA}}>
                                            <Text style={{...styles.uploadBtnText,fontSize:14,textAlign:'center'}}>Upload Photo</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.userItem}>
                                        <Text style={styles.label}>Full Name</Text>
                                        <Text style={styles.value}>{this.state.userData.fullName}</Text>
                                    </View>
                                    <View style={styles.userItem}>
                                        <Text style={styles.label}>Manager</Text>
                                        <Text style={styles.value}>{this.state.userData.manager}</Text>
                                    </View>
                                    <View style={styles.userItem}>
                                        <Text style={styles.label}>Shift Start Date</Text>
                                        <Text style={styles.value}>{this.state.userData.shiftStartDate}</Text>
                                    </View>
                                    <View style={styles.userItem}>
                                        <Text style={styles.label}>Shift End Date</Text>
                                        <Text style={styles.value}>{this.state.userData.shiftEndDate}</Text>
                                    </View>
                                    <View style={styles.userItem}>
                                        <Text style={styles.label}>Mobile</Text>
                                        <Text style={styles.value}>{this.state.userData.mobile}</Text>
                                    </View>
                                    <View style={styles.userItem}>
                                        <Text style={styles.label}>Email</Text>
                                        <Text style={styles.value}>{this.state.userData.emailId}</Text>
                                    </View>
                                    <View style={styles.userItem}>
                                        <Text style={styles.label}>Company</Text>
                                        <Text style={styles.value}>{this.state.userData.company}</Text>
                                    </View>
                                    <View style={styles.userItem}>
                                        <Text style={styles.label}>Blood Group</Text>
                                        <Text style={styles.value}>{this.state.userData.bloodGrp}</Text>
                                    </View>
                                    <View style={styles.userItem}>
                                        <Text style={styles.label}>Date Of Birth</Text>
                                        <Text style={styles.value}>{this.state.userData.dob}</Text>
                                    </View>
                                </React.Fragment>
                            }
                        </ScrollView>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}