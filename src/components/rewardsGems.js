import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, Dimensions, ActivityIndicator, TouchableOpacity, SafeAreaView, ScrollView, ImageBackground } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import moment from 'moment';
import APILIB from '../api';
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    rewardsWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10,
    },
    rewardsHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf:'center',
        paddingVertical: 20,
        position: 'relative',
        width: '90%',
        paddingTop:40,
    },
    headerLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsRegular,
    },
    goBackIconWrapper: {
        marginRight: 30,
        paddingHorizontal:15,
        paddingVertical:10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    tabWrapper: {
        marginTop: 10,
        flex:1,
    },
    tabsTitle: {
        color: '#fff',
        fontSize: 16,
        fontFamily: Fonts.PoppinsBold,
    },
    tabBtnWrapper: {
        flexDirection: 'row',
        width:'90%',
        alignSelf:'center',
    },
    tabBtn: {
        backgroundColor: '#1a3645',
        width: '50%',
        paddingVertical: 15,
        borderTopLeftRadius: 12,
        borderBottomLeftRadius: 12,
    },
    tabBtnText: {
        color: Colors.textGrey,
        fontSize: 14,
        textAlign: 'center',
        fontFamily: Fonts.PoppinsMedium,
    },
    tabBtnRight: {
        backgroundColor: '#1a3645',
        width: '50%',
        paddingVertical: 15,
        borderTopRightRadius: 12,
        borderBottomRightRadius: 12,
    },
    tabBtnActive: {
        backgroundColor: Colors.primary,
    },
    tabBtnTextActive: {
        color: Colors.secondary,
    },
    tabMainDesc:{
        color: '#c3c7c9',
        fontSize: 16,
        textAlign: 'center',
        fontFamily: Fonts.PoppinsMedium,
        maxWidth:'85%',
        marginBottom:20,
        alignSelf:'center'
    },
    pointsContent:{
        borderWidth:1,
        borderColor:Colors.bgBlue,
        borderRadius:20,
        flex:1,
        width:width - 40,
        marginBottom:40,
        paddingVertical:10,
    },
    tabHeader:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingVertical:20,
        paddingHorizontal:10,
        borderBottomWidth:1,
        borderBottomColor:Colors.bgBlue,
    },
    tabHeaderText:{
        color: '#ffffff',
        fontSize: 16,
        fontFamily: Fonts.PoppinsMedium,
        alignSelf:'center',
        textAlign:'left',
        width:'50%'
    },
    pointsRow:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal:15,
        paddingVertical:10,
    },
    rankWrapper:{
        flexDirection:'row',
        justifyContent:'space-between',
    },
    iconCup:{
        width:30,
        height:35,
        marginRight:10,
    },
    rankIndex:{
        color: Colors.secondary,
        fontSize: 16,
        fontFamily: Fonts.PoppinsRegular,
        alignSelf:'center',
    },
    pointsText:{
        color: 'rgba(255,255,255,0.65)',
        fontSize: 16,
        fontFamily: Fonts.PoppinsRegular,
        textAlign:'left',
    },
    pointsGiftText:{
        width:'40%',
        marginTop:-7
    },  
    pointsGiftImage:{
        width:80,
        height:60,
    },
    iconTeamKit:{
        width:60,
        height:61,
    },
    teamText:{
        color: 'rgba(255,255,255,0.65)',
        fontSize: 16,
        fontFamily: Fonts.PoppinsRegular,
        textAlign:'center',
        marginTop:10,
    },
    gemsHeader:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingVertical:10,
        paddingHorizontal:10,
        borderBottomWidth:1,
        borderBottomColor:Colors.bgBlue,
    },
    gemsHeaderText:{
        color: '#ffffff',
        fontSize: 16,
        fontFamily: Fonts.PoppinsMedium,
        alignSelf:'center',
        textAlign:'left',
        width:'50%'
    },
    iconDiamond:{
        width:20,
        height:20,
        marginLeft:20,
        marginRight:10,
    },
    gemsGiftText:{
        width:'40%',
    }
});

export default class rewardsGems extends Component {

    constructor(props){
        super(props);
        this.state = {
            activeTab : 1
        }
        this._isMounted = false;
    }

    componentDidMount(){
        this._isMounted = true;
    }

    toggleTab(){
        if(this._isMounted){
            this.setState({
                activeTab : this.state.activeTab == 1 ? 2 : 1,
            });
        }
    }

    render(){
        let tabLeftActive = { ...styles.tabBtn, ...styles.tabBtnActive };
        let tabLeft = { ...styles.tabBtn};
        let tabActiveText = { ...styles.tabBtnText, ...styles.tabBtnTextActive };
        let tabText = { ...styles.tabBtnText };
        let tabRightActive = {...styles.tabBtnRight,...styles.tabBtnActive};
        let tabRight = {...styles.tabBtnRight};
        return(
            <SafeAreaView style={styles.wrapper}>
                <View style={styles.rewardsWrapper}>
                    <View style={styles.rewardsHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                        </TouchableOpacity>
                        <Text style={styles.headerLabel}>Rewards</Text>
                    </View>
                    <View style={styles.tabWrapper}>
                        <View style={styles.tabBtnWrapper}>
                            <TouchableOpacity onPress={() => this.toggleTab()} style={this.state.activeTab == 1 ? tabLeftActive : tabLeft}>
                                <Text style={this.state.activeTab == 1 ? tabActiveText : tabText}>Points</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.toggleTab()} style={this.state.activeTab == 2 ? tabRightActive : tabRight}>
                                <Text style={this.state.activeTab == 2 ? tabActiveText : tabText}>Gems</Text>
                            </TouchableOpacity>
                        </View>
                    {this.state.activeTab == 1 ? 
                        <ScrollView style={{ flex: 1 }} contentContainerStyle={{ marginTop:20 ,alignSelf:'center'}}>
                            <Text style={styles.tabMainDesc}>Be among the top 5 performers on the Leaderboard and with special prizes</Text>
                            <View style={styles.pointsContent}>
                                <View style={styles.pointsRow}>
                                    <View style={styles.rankWrapper}>
                                        <ImageBackground source={require('../../assets/images/icon-cup.png')} style={styles.iconCup} >
                                            <Text style={styles.rankIndex}>1</Text>
                                        </ImageBackground>
                                        <Text style={styles.pointsText}>Rank</Text>
                                    </View>
                                    <Text style={{...styles.pointsText,...styles.pointsGiftText}}>Safety Boots</Text>
                                    <Image source={require('../../assets/images/boots.png')} style={styles.pointsGiftImage}></Image>
                                </View>
                                <View style={styles.pointsRow}>
                                    <View style={styles.rankWrapper}>
                                        <ImageBackground source={require('../../assets/images/icon-cup.png')} style={styles.iconCup} >
                                            <Text style={styles.rankIndex}>2</Text>
                                        </ImageBackground>
                                        <Text style={styles.pointsText}>Rank</Text>
                                    </View>
                                    <Text style={{...styles.pointsText,...styles.pointsGiftText}}>FitBit</Text>
                                    <Image source={require('../../assets/images/fitbit.png')} style={styles.pointsGiftImage}></Image>
                                </View>
                                <View style={styles.pointsRow}>
                                    <View style={styles.rankWrapper}>
                                        <ImageBackground source={require('../../assets/images/icon-cup.png')} style={styles.iconCup} >
                                            <Text style={styles.rankIndex}>3</Text>
                                        </ImageBackground>
                                        <Text style={styles.pointsText}>Rank</Text>
                                    </View>
                                    <Text style={{...styles.pointsText,...styles.pointsGiftText}}>Bluetooth Speaker</Text>
                                    <Image source={require('../../assets/images/speakers.png')} style={styles.pointsGiftImage}></Image>
                                </View>
                            </View>
                            <View style={styles.pointsContent}>
                                <View style={styles.pointsRow}>
                                    <View style={styles.rankWrapper}>
                                        <ImageBackground source={require('../../assets/images/icon-team-kit.png')} style={styles.iconTeamKit} >
                                            <Text style={styles.teamText}>Best Team</Text>
                                        </ImageBackground>
                                    </View>
                                    <Text style={{...styles.pointsText,...styles.pointsGiftText}}>Volleyball Kit</Text>
                                    <Image source={require('../../assets/images/volleyball.png')} style={styles.pointsGiftImage}></Image>
                                </View>
                            </View>
                        </ScrollView>
                    :
                        <ScrollView style={{ flex: 1 }} contentContainerStyle={{ marginTop:20 ,alignSelf:'center'}}>
                            <Text style={{...styles.tabMainDesc,maxWidth:'70%'}}>You can use your gems to redeem special prizes</Text>
                            <View style={styles.pointsContent}>
                                <View style={styles.gemsHeader}>
                                    <Text style={styles.gemsHeaderText}>No. of Gems</Text>
                                    <Text style={styles.gemsHeaderText}>Prize</Text>
                                </View>
                                <View style={styles.pointsRow}>
                                    <View style={styles.rankWrapper}>
                                        <Image source={require('../../assets/images/icon-diamond.png')} style={styles.iconDiamond} />
                                        <Text style={styles.pointsText}>5</Text>
                                    </View>
                                    <Text style={{...styles.pointsText,...styles.gemsGiftText}}>Sunglasses</Text>
                                    <Image source={require('../../assets/images/boots.png')} style={styles.pointsGiftImage}></Image>
                                </View>
                                <View style={styles.pointsRow}>
                                    <View style={styles.rankWrapper}>
                                        <Image source={require('../../assets/images/icon-diamond.png')} style={styles.iconDiamond} />
                                        <Text style={styles.pointsText}>10</Text>
                                    </View>
                                    <Text style={{...styles.pointsText,...styles.gemsGiftText}}>FitBit</Text>
                                    <Image source={require('../../assets/images/fitbit.png')} style={styles.pointsGiftImage}></Image>
                                </View>
                                <View style={styles.pointsRow}>
                                    <View style={styles.rankWrapper}>
                                        <Image source={require('../../assets/images/icon-diamond.png')} style={styles.iconDiamond} />
                                        <Text style={styles.pointsText}>15</Text>
                                    </View>
                                    <Text style={{...styles.pointsText,...styles.gemsGiftText}}>Bluetooth Speaker</Text>
                                    <Image source={require('../../assets/images/speakers.png')} style={styles.pointsGiftImage}></Image>
                                </View>
                                <View style={styles.pointsRow}>
                                    <View style={styles.rankWrapper}>
                                        <Image source={require('../../assets/images/icon-diamond.png')} style={styles.iconDiamond} />
                                        <Text style={styles.pointsText}>20</Text>
                                    </View>
                                    <Text style={{...styles.pointsText,...styles.gemsGiftText}}>Phone</Text>
                                    <Image source={require('../../assets/images/phone.png')} style={styles.pointsGiftImage}></Image>
                                </View>
                                <View style={styles.pointsRow}>
                                    <View style={styles.rankWrapper}>
                                        <Image source={require('../../assets/images/icon-diamond.png')} style={styles.iconDiamond} />
                                        <Text style={styles.pointsText}>25</Text>
                                    </View>
                                    <Text style={{...styles.pointsText,...styles.gemsGiftText}}>Bike</Text>
                                    <Image source={require('../../assets/images/boots.png')} style={styles.pointsGiftImage}></Image>
                                </View>
                            </View>
                        </ScrollView>
                    }
                    </View>
                    <TouchableOpacity onPress={() => { this.submitForm() }} style={{ ...Commonstyles.btnPrimary, borderRadius: 0, width: '100%' }}>
                        <Text style={{ ...Commonstyles.btnPrimaryText }}>Redeem</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}