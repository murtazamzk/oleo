import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, Dimensions, ActivityIndicator, ScrollView, TouchableOpacity, SafeAreaView } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { Colors, Commonstyles, Fonts } from '../constants';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import AsyncStorage from '@react-native-community/async-storage';
import ProfileBox from './profileBox';
import moment from 'moment';
import APILIB from '../api';
import UserImage from './userImageLoop'; 

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary,
        paddingTop: 30,
        position:'relative',
    },
    socialHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 20,
        position: 'relative',
        width: '100%',
    },
    goBackIconWrapper: {
        marginLeft: 25,
        paddingHorizontal:15,
        paddingVertical:10,
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    socialLabel: {
        color: '#ffffff',
        fontSize: 22,
        flex: 1,
        marginLeft: 40,
        fontFamily: Fonts.PoppinsRegular,
    },
    socialContent: {
        width: '90%',
        alignSelf: 'center'
    },
    borderLine: {
        width: '100%',
        height: 1,
        backgroundColor: '#27414f',
        marginBottom: 20
    },
});

export default class SocialWall extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: {},
            userImage: '',
            socialPosts:[],
            noSocialPosts:false,
        };
        this._isMounted = false;
    }

    async componentDidMount() {
        this._isMounted = true;
        this.getData();
        this.unsubscribeSocial = await firestore()
        .collection('socialwall')
        .orderBy('accessed_time', "desc")
        .onSnapshot((snapshot) => {
            let db = snapshot.docs;
            let social = [];
            db.forEach((value, index) => {
                social.push({...value.data(),"docId":value.id});
            });
            if(this._isMounted){
                if(db.length){
                    this.setState({
                        socialPosts: social
                    });
                }else{
                    this.setState({
                        noSocialPosts:true,
                    });
                }
            }
        });
        this.setState({
            loading: false
        });
    }

    async getData() {
        try {
            const value = await AsyncStorage.getItem('@user')
            if (value !== null) {
                this.setState({
                    userData: JSON.parse(value)
                }, () => {
                    this.getProfileImage(this.state.userData.user);
                });
            }
        } catch (e) {
            console.log(e);
        }
    }

    async getProfileImage(uid) {
        const ref = firebase.storage().ref('userImages/' + uid + '.jpg');
        ref.getDownloadURL().then((res) => {
            this.setState({
                userImage: res
            })
        }, () => {
            console.log('Image does not exist');
        });

    }

    async getSocial() {
        const documentSnapshot = await firestore()
            .collection('socialwall')
            .get();
        let db = documentSnapshot.docs;

        let social = [];
        db.forEach((value, index) => {
            console.log(value.data());

            social.push(value.data());
        });
        this.setState({
            socialPosts: social
        });
    }

    toggleSocial(docId,isLike,userId){
        let data = {
            "docId":docId,
            "cheers":isLike,
            "userId":this.state.userData.user,
            "touser":userId
        }
        if (this._isMounted) {
            this.setState({
                loading:true
            });
        }
        
        var api = new APILIB();
        let fetchResponse = api.likeSocial(JSON.stringify(data));
        fetchResponse.then(response => {
            response.json().then((res) => {
                if (this._isMounted) {
                    this.setState({
                        loading:false
                    });
                }
            });
        });
        
    }

    checkifliked(likedUsers){
        if(!likedUsers){
            return true;
        }
        
        if(likedUsers.includes(this.state.userData.user)){
            return false;
        }else{
            return true;
        }
    }

    componentWillUnmount(){
        this._isMounted = false;
    }

    render() {
        let userImage = this.state.userImage ? { uri: this.state.userImage } : require('../../assets/images/userPhoto.png');
        return (
            <SafeAreaView style={styles.wrapper}>
                {this.state.loading &&
                    <View style={Commonstyles.loaderWrapper}>
                        <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                    </View>
                }
                <View style={styles.socialHeader}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                        <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                    </TouchableOpacity>
                    <Text style={styles.socialLabel}>Social Wall</Text>
                </View>
                <ScrollView style={styles.socialContent}>
                <ProfileBox navigation={this.props.navigation} />
                    <View style={styles.borderLine}></View>
                    <View style={Commonstyles.socialWall}>
                        {this.state.socialPosts.length ?
                            <React.Fragment>
                                {
                                    this.state.socialPosts.map((value, index) => (
                                        <View key={index} style={Commonstyles.socialRow}>
                                            <UserImage uid={value.userId} />
                                            <View style={Commonstyles.socialRowContent}>
                                                <Text style={Commonstyles.socialPost}><Text style={Commonstyles.socialPostBold}>{value.name}</Text> {value.reason}</Text>
                                                <View style={Commonstyles.socialRowBottom}>
                                                    <TouchableOpacity onPress={() => this.toggleSocial(value.docId,this.checkifliked(value.likedUsers),value.userId)}  style={Commonstyles.cheersWrapper}>
                                                    <Image style={Commonstyles.cheerIcon} source={
                                                                this.checkifliked(value.likedUsers) ? require('../../assets/images/iconHeartBorder.png') : require('../../assets/images/iconHeartBorderFill.png') } />
                                                        <Text style={Commonstyles.cheerCount}>{value.cheers} Cheers</Text>
                                                    </TouchableOpacity>
                                                    <Text style={Commonstyles.socialTime}>{moment(value.accessed_time.toDate()).fromNow()}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    ))
                                }
                            </React.Fragment>
                            :
                            <React.Fragment>
                                {this.state.noSocialPosts ? null : 
                                    <View style={Commonstyles.loaderWrapperInline}>
                                        <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                                    </View>
                                }
                            </React.Fragment>
                        }
                        {/* <View style={Commonstyles.socialRow}>
                            <View style={Commonstyles.userAvatarGrey}>
                                <Image style={Commonstyles.userImageGrey} source={require('../../assets/images/userPhoto.png')} />
                            </View>
                            <View style={Commonstyles.socialRowContent}>
                            <Text style={Commonstyles.socialPost}><Text style={Commonstyles.socialPostBold}>Monu Sharma</Text> earned 4 Points by reporting a positive observation about his teammate Sonu</Text>
                                <View style={Commonstyles.socialRowBottom}>
                                    <View style={Commonstyles.cheersWrapper}>
                                        <Image style={Commonstyles.cheerIcon} source={require('../../assets/images/iconHeartBorder.png')} />
                                        <Text style={Commonstyles.cheerCount}>6 Cheers</Text>
                                    </View>
                                    <Text style={Commonstyles.socialTime}>Now</Text>
                                </View>
                            </View>
                        </View>
                        <View style={Commonstyles.socialRow}>
                            <View style={Commonstyles.userAvatarGrey}>
                                <Image style={Commonstyles.userImageGrey} source={require('../../assets/images/userPhoto.png')} />
                            </View>
                            <View style={Commonstyles.socialRowContent}>
                                <Text style={Commonstyles.socialPost}><Text style={Commonstyles.socialPostBold}>Monu Sharma</Text> earned 4 Points by reporting a positive observation about his teammate Sonu</Text>
                                <View style={Commonstyles.socialRowBottom}>
                                    <View style={Commonstyles.cheersWrapper}>
                                        <Image style={Commonstyles.cheerIcon} source={require('../../assets/images/iconHeartBorder.png')} />
                                        <Text style={Commonstyles.cheerCount}>12 Cheers</Text>
                                    </View>
                                    <Text style={Commonstyles.socialTime}>10 Mins ago</Text>
                                </View>
                            </View>
                        </View>
                        <View style={Commonstyles.socialRow}>
                            <View style={Commonstyles.userAvatarGrey}>
                                <Image style={Commonstyles.userImageGrey} source={require('../../assets/images/userPhoto.png')} />
                            </View>
                            <View style={Commonstyles.socialRowContent}>
                                <Text style={Commonstyles.socialPost}><Text style={Commonstyles.socialPostBold}>Monu Sharma</Text> earned 4 Points by reporting a positive observation about his teammate Sonu</Text>
                                <View style={Commonstyles.socialRowBottom}>
                                    <View style={Commonstyles.cheersWrapper}>
                                        <Image style={Commonstyles.cheerIcon} source={require('../../assets/images/iconHeartBorder.png')} />
                                        <Text style={Commonstyles.cheerCount}>12 Cheers</Text>
                                    </View>
                                    <Text style={Commonstyles.socialTime}>10 Mins ago</Text>
                                </View>
                            </View>
                        </View>
                        <View style={Commonstyles.socialRow}>
                            <View style={Commonstyles.userAvatarGrey}>
                                <Image style={Commonstyles.userImageGrey} source={require('../../assets/images/userPhoto.png')} />
                            </View>
                            <View style={Commonstyles.socialRowContent}>
                                <Text style={Commonstyles.socialPost}><Text style={Commonstyles.socialPostBold}>Monu Sharma</Text> earned 4 Points by reporting a positive observation about his teammate Sonu</Text>
                                <View style={Commonstyles.socialRowBottom}>
                                    <View style={Commonstyles.cheersWrapper}>
                                        <Image style={Commonstyles.cheerIcon} source={require('../../assets/images/iconHeartBorder.png')} />
                                        <Text style={Commonstyles.cheerCount}>12 Cheers</Text>
                                    </View>
                                    <Text style={Commonstyles.socialTime}>10 Mins ago</Text>
                                </View>
                            </View>
                        </View>
                        <View style={Commonstyles.socialRow}>
                            <View style={Commonstyles.userAvatarGrey}>
                                <Image style={Commonstyles.userImageGrey} source={require('../../assets/images/userPhoto.png')} />
                            </View>
                            <View style={Commonstyles.socialRowContent}>
                                <Text style={Commonstyles.socialPost}><Text style={Commonstyles.socialPostBold}>Sachin Shah</Text> earned 4 points by completing the Basic Fire Safety training</Text>
                                <View style={Commonstyles.socialRowBottom}>
                                    <View style={Commonstyles.cheersWrapper}>
                                        <Image style={Commonstyles.cheerIcon} source={require('../../assets/images/iconHeartBorder.png')} />
                                        <Text style={Commonstyles.cheerCount}>12 Cheers</Text>
                                    </View>
                                    <Text style={Commonstyles.socialTime}>10 Mins ago</Text>
                                </View>
                            </View>
                        </View>
                        <View style={Commonstyles.socialRow}>
                            <View style={Commonstyles.userAvatarGrey}>
                                <Image style={Commonstyles.userImageGrey} source={require('../../assets/images/userPhoto.png')} />
                            </View>
                            <View style={Commonstyles.socialRowContent}>
                                <Text style={Commonstyles.socialPost}><Text style={Commonstyles.socialPostBold}>Monu Sharma</Text> earned 4 Points by reporting a positive observation about his teammate Sonu</Text>
                                <View style={{ ...Commonstyles.socialRowBottom, borderBottomWidth: 0 }}>
                                    <View style={Commonstyles.cheersWrapper}>
                                        <Image style={Commonstyles.cheerIcon} source={require('../../assets/images/iconHeartBorder.png')} />
                                        <Text style={Commonstyles.cheerCount}>9 Cheers</Text>
                                    </View>
                                    <Text style={Commonstyles.socialTime}>31 Mins ago</Text>
                                </View>
                            </View>
                        </View> */}
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}
