import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from '@react-native-firebase/app';

export default class SplashScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            userData : null,
        }
    }
    async componentDidMount(){
        let nextScreen = '';
        this.getData().then((userExists) => {
            if(userExists){
                nextScreen = 'Home';
                let uid = this.state.userData.user;
                let db = firebase.firestore()
                    .collection('users')
                    .doc(uid)
                    .get();
                
                db.then((res) => {
                    if (res.data()) {
                        this.storeData(res.data()).then(() => {
                            this.setState({
                                loading: false,
                            }, () => {
                                    const resetAction = StackActions.reset({
                                        index: 0,
                                        actions: [NavigationActions.navigate({ routeName: nextScreen })],
                                    });
                                    setTimeout(() => {
                                        this.props.navigation.dispatch(resetAction);
                                    }, 1000);
                            });
                        })
                    }
                });
            }else{
                nextScreen = 'Login';
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: nextScreen })],
                });
                setTimeout(() => {
                    this.props.navigation.dispatch(resetAction);
                }, 1000);
            }
        });
    }
    async storeData(userData) {
        try {
            await AsyncStorage.setItem('@user', JSON.stringify(userData));
        } catch (e) {
            console.log(e);
        }
    }
    async getData() {
        try {
            const value = await AsyncStorage.getItem('@user');
            if (value !== null) {
                this.setState({
                    userData:JSON.parse(value)
                });
                return true;
            }else{
                return false;
            }
        } catch (e) {
            console.log(e);
        }
    }
    render() {
        return (
            <View style={{
                justifyContent: 'center',
                alignItems: 'center', flex: 1
            }}>
                <Image source={require('../../assets/images/splash.jpg')} style={{
                    width: '100%',
                    height: '100%',
                }} />
            </View>
        );
    }
}