import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, Dimensions, ImageBackground, BackHandler, ActivityIndicator, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import { WebView, } from 'react-native-webview';
import ImageView from 'react-native-image-view';
import firebase from '@react-native-firebase/app';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import Pdf from 'react-native-pdf';
import APILIB from '../api';

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    learningWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10
    },
    learningHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf:'center',
        justifyContent:'space-between',
        paddingVertical: 20,
        position: 'relative',
        width: '90%',
        paddingTop:40,
    },
    goBackIconWrapper: {
        marginRight: 30,
        paddingHorizontal:15,
        paddingVertical:10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    btnMark:{
        marginTop:20,
        flexDirection:'row',
        alignItems:'center',
        paddingHorizontal: 25,
        paddingVertical: 15,
        backgroundColor:Colors.bgBlue,
        borderRadius:15,
        alignSelf:'flex-start'
    },
    btnMarkText:{
        color:'#ffffff',
        fontSize: 14,
        marginRight:15,
        fontFamily:Fonts.PoppinsRegular,
    },
    btnMarkChkBox:{
        width:22,
        height:22,
        borderWidth:1,
        borderColor:'rgba(255,255,255,0.5)',
        justifyContent:'center',
        alignItems:'center'
    },
    btnMarkChkBoxChecked:{
        width:18,
        height:18,
        backgroundColor:'rgba(255,255,255,0.5)'
    },
    learningContent:{
        width:'90%',
        alignSelf:'center',
        marginTop:25,
    },
    contentTitle:{
        fontSize:18,
        color:'#ffffff',
        marginBottom:10,
        fontFamily:Fonts.PoppinsMedium,
    },
    contentDesc:{
        fontSize:16,
        lineHeight:24,
        color:Colors.textBlue,
        fontFamily:Fonts.PoppinsRegular,
    },
    pdf: {
        flex: 1,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height - 350,
    },
    rewardDialogHead: {
        paddingTop: 30,
    },
    rewardDialogContent: {
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    rewardDialogTitle: {
        fontFamily: Fonts.PoppinsBold,
        fontSize: 28,
        marginBottom: 7,
        textAlign: 'center',
    },
    rewardDialogSubTitle: {
        fontFamily: Fonts.PoppinsRegular,
        fontSize: 16,
        color: '#6c767e',
        textAlign: 'center',
    },
    coinWrapper: {
        width: 150,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 15,
    },
    pointsText: {
        fontSize: 50,
        textAlign: 'center',
        fontFamily: Fonts.PoppinsBold,
        color: '#744900',
        marginTop: 10,
    },
    tipText: {
        color: '#848c92',
        fontFamily: Fonts.PoppinsRegular,
        textAlign: 'center',
    },
});

export default class learningPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            title : '',
            desc: '',
            imageModalOpen : false,
            loading:false,
            markComplete : false,
            userImage:null,
            mediaUrl:null,
            downloadUrl:"",
            userId:'',
            rewardDialog:false,
        }
        this._isMounted = false;
    }

    onShouldStartLoadWithRequest = (navigator) => {
        return true;  
    }

    async componentDidMount(){
        const { navigation } = this.props;
        this._isMounted = true;
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            if(this.state.rewardDialog){
                this.setState({
                    rewardDialog:false
                });
                return true;
            }else{
                // this.props.navigation.navigate('LearningList');
            }
        });
        if(this._isMounted){
            if (navigation.getParam('title')) {
                await this.setState({
                    title: navigation.getParam('title')
                })
            }
            if (navigation.getParam('desc')) {
                await this.setState({
                    desc: navigation.getParam('desc')
                })
            }
            if (navigation.getParam('mediaUrl')) {
                this.get_Download_URL(navigation.getParam('mediaUrl'));
            }
            if (navigation.getParam('userid')) {
                await this.setState({
                    userId: navigation.getParam('userid')
                },() => {
                    this.getProfileImage();
                });
            }
            if (navigation.getParam('completed')) {
                this.setState({
                    markComplete:true,
                });
            }
            // var api = new APILIB();
            // let data = {
            //     "user":this.state.userId,
            //     "id":navigation.getParam('docId'),
            //     "topic":navigation.getParam('topic')
            // }
            // this.setState({
            //     loading:true,
            // });
            // let fetchResponse = api.isLearningCompleted(JSON.stringify(data));
            // fetchResponse.then(response => {
            //     response.json().then(res => {
            //         if(res.isCompleted){
            //             this.setState({
            //                 loading:false,
            //                 markComplete:true,
            //             });
            //         }else{
            //             this.setState({
            //                 loading:false,
            //             });
            //         }
            //     });
            // }).catch(error => {
            //     console.log(error);
            // });
        }
    }

    componentWillUnmount(){
        this._isMounted = false;
    }

    closeDialog() {
        this.setState({
            rewardDialog:false,
        });
    }

    async getProfileImage(uid) {
        const ref = firebase.storage().ref('userImages/' + this.state.userId + '.jpg');
        ref.getDownloadURL().then((res) => {
            if(this._isMounted){
                this.setState({
                    userImage: res
                });
            }
        }, () => {
            console.log('Image does not exist');
        });

    }

    get_Download_URL(mediaUrl){
        const ref = firebase.storage().ref(mediaUrl);
        ref.getDownloadURL().then((res) => {
            if(this._isMounted){
                this.setState({
                    downloadUrl:res
                });
            }
        }, () => {
            console.log('Image does not exist');
        });
    }

    renderMedia(type,mediaUrl,imageModalOpen){    
        switch (type) {
            case 1:
                return <View style={{ height: 300 }}>
                    <WebView
                        scalesPageToFit={true}
                        source={{ uri: mediaUrl }}
                        style={{ height: 400 }}
                        onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest} //for iOS
                        onNavigationStateChange={this.onShouldStartLoadWithRequest} //for Android
                    />
                </View>
            case 2:
                let imageSource = [
                    {
                        source: {
                            uri: mediaUrl,
                        },
                    },
                ];
                return <View style={{height:300}}>
                    <TouchableOpacity onPress={()=>this.toggleImageModal()} style={{flex:1,height:300}}>
                        <Image style={{width:width,height:300}} resizeMode={"contain"} source={{uri: mediaUrl}} />
                    </TouchableOpacity>
                    <ImageView
                    images={imageSource}
                    imageIndex={0}
                    isVisible={imageModalOpen}
                    onClose={()=>this.toggleImageModal()} /></View>
            case 3:
                const source = { uri: mediaUrl, cache: true };
                return <View style={{ height: height - 350 }}>
                    <Pdf
                        source={source}
                        onLoadComplete={(numberOfPages, filePath) => {
                            console.log(`number of pages: ${numberOfPages}`);
                        }}
                        onPageChanged={(page, numberOfPages) => {
                            console.log(`current page: ${page}`);
                        }}
                        onError={(error) => {
                            console.log(error);
                        }}
                        onPressLink={(uri) => {
                            console.log(`Link presse: ${uri}`)
                        }}
                        style={styles.pdf} />
                </View>
            default:
                return <View><Text>{mediaUrl}</Text></View>
        }
    }

    toggleImageModal(){
        this.setState({
            imageModalOpen:!this.state.imageModalOpen
        });
    }

    markAsComplete(){
        const { navigation } = this.props;
        let data = {
            "isCompleted":true,
            "topic":navigation.getParam('topic'),
            "reward":navigation.getParam('reward'),
            "reason":navigation.getParam('userName') + " has completed "+navigation.getParam('topic') + " and earned " + navigation.getParam('reward') + " coins",
            "user":navigation.getParam('userid'),
            "docId":navigation.getParam('docId'),
            "totaltime": parseInt(navigation.getParam('totaltime')),
            "picurl": this.state.userImage,
            "subcat":navigation.getParam('subcat'),
            "type":parseInt(navigation.getParam('type')),
            "managerid":this.props.navigation.getParam('managerid')
        }
        this.setState({
            loading:true,
        });
        var api = new APILIB();
        let fetchResponse = api.updateLearning(JSON.stringify(data));
        console.log(JSON.stringify(data));
        fetchResponse.then(response => {
            console.log(response);
            response.json().then(res => {
                this.setState({
                    loading:false,
                    markComplete:true,
                    rewardDialog:true,
                });
            });
        }).catch(error => {
            console.log(error);
            
        });
    }

    render() {
        let { type } = this.props.navigation.state.params;
        const { navigation } = this.props;
        return (
            <SafeAreaView style={styles.wrapper}>
                {this.state.loading &&
                    <View style={Commonstyles.loaderWrapper}>
                        <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                    </View>
                }
                <Dialog
                        visible={this.state.rewardDialog}
                        onTouchOutside={() => {
                            this.setState({ rewardDialog: false });
                        }}
                        dialogStyle={Commonstyles.dialog}
                        width={width - 30}
                        height={560}
                        overlayOpacity={0.5}
                    >
                        <DialogContent>
                            <View style={Commonstyles.dialogContent}>
                                <View style={styles.rewardDialogHead}>
                                    <Text style={styles.rewardDialogTitle}>Congratulations</Text>
                                    <Text style={styles.rewardDialogSubTitle}>You have earned {navigation.getParam('reward')} Coins</Text>
                                </View>
                                <View style={styles.rewardDialogContent}>
                                    <ImageBackground style={styles.coinWrapper} source={require('../../assets/images/coinReward.png')}>
                                        <Text style={styles.pointsText}>+{navigation.getParam('reward')}</Text>
                                    </ImageBackground>
                                    <Text style={styles.tipText}>Tip : Earn more points by reporting positive observations</Text>
                                </View>
                                <View style={{ ...Commonstyles.dialogFooter, width: '100%' }}>
                                    <TouchableOpacity onPress={() => this.closeDialog('rewardDialog')} style={{ ...Commonstyles.btnPrimary, width: '100%' }}>
                                        <Text style={Commonstyles.btnPrimaryText}>CONTINUE</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {/* <TouchableOpacity onPress={() => this.closeDialog('rewardDialog')} style={Commonstyles.dialogCloseBtn}>
                                <Image style={Commonstyles.dialogCloseIcon} source={require('../../assets/images/questionPopupClose.png')} ></Image>
                            </TouchableOpacity> */}
                        </DialogContent>
                    </Dialog>
                <View style={styles.learningWrapper}>
                    <View style={styles.learningHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                        </TouchableOpacity>
                    </View>
                    {/* <WebView 
                        ref={(ref) => { this.videoPlayer = ref;}}
                        scalesPageToFit={true} 
                        style={{height:300}}
                        
                        source={{ html: '<html><meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" /><h1>Hello world</h1><iframe src="https://www.youtube.com/embed/' + this.props.navigation.state.params.videoId + '?modestbranding=1&playsinline=1&showinfo=0&rel=0" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height:100%;width:100%;position:absolute;top:0px;left:0px;right:0px;bottom:0px" height="100%" width="100%"></iframe></html>'}} 
                        onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest} //for iOS
                        onNavigationStateChange ={this.onShouldStartLoadWithRequest} //for Android
                    /> */}
                    
                    {this.state.downloadUrl !== "" ? this.renderMedia(type, this.state.downloadUrl,this.state.imageModalOpen) : null}
                    <View style={styles.learningContent}>
                        <ScrollView contentContainerStyle={{paddingBottom:30}} style={{height:height - 440,marginBottom:30}}>
                            <Text style={styles.contentTitle}>
                                {this.state.title}
                            </Text>
                            <Text style={styles.contentDesc}>
                                {this.state.desc}
                            </Text>
                            <TouchableOpacity onPress={() => this.markAsComplete()} disabled={this.state.markComplete} style={styles.btnMark}>
                                <Text style={styles.btnMarkText}>
                                    {this.state.markComplete ? 'Completed' : 'Mark As Complete'}
                                </Text>
                                <View style={styles.btnMarkChkBox}>
                                    {this.state.markComplete && <View style={styles.btnMarkChkBoxChecked}></View>}
                                </View>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}