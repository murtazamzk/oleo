import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, ActivityIndicator, Dimensions, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { StackActions, NavigationActions, NavigationEvents } from 'react-navigation';
import { Colors, Commonstyles, Fonts, logout } from '../constants';
import Modal from 'react-native-modal';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import AsyncStorage from '@react-native-community/async-storage';
import ProfileBox from './profileBox';
import moment from 'moment';
import UserImage from './userImageLoop'; 

import APILIB from '../api';

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary,
        paddingTop: 30,
        position:'relative'
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 20,
        position: 'relative',
        width: '100%',
    },
    menuWrapper: {

    },
    menuIcon: {
        width: 30,
        height: 20,
    },
    logoText: {
        width: 70,
        height: 47
    },
    notifWrapper: {

    },
    notifIcon: {
        width: 25,
        height: 33,
    },
    contentWrapper: {
        flex: 1,
        width: '90%',
        alignSelf: 'center',
        marginTop: 20
    },
    boxTitle: {
        color: 'rgba(255,255,255,0.75)',
        fontSize: 16,
        fontFamily: Fonts.PoppinsBold,
    },
    boxBtnWrapper: {
        width: 60,
        alignItems: 'flex-end'
    },
    boxBtnIcon: {
        width: 8,
        height: 14
    },
    boxTitleWrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    stars: {
        marginLeft: 10,
        width: 120,
        height: 18
    },
    activitiesWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingTop: 20,
        justifyContent: 'space-between'
    },
    activityItem: {
        width: '46%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
        alignItems: 'center',
    },
    activityLabel: {
        fontSize: 12,
        color: '#638599',
        flex: 0.8,
        fontFamily: Fonts.PoppinsRegular,
    },
    activityCountWrapper: {
        backgroundColor: Colors.secondary,
        paddingVertical: 5,
        width:40,
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'flex-start',
        justifyContent:'center',
    },
    activityCount: {
        fontSize: 12,
        color: '#ffffff',
        fontFamily: Fonts.PoppinsRegular,
        textAlign:'center',
    },
    learningWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
    },
    learningTime: {
        marginLeft: 20,
        backgroundColor: Colors.secondary,
        paddingVertical: 5,
        paddingHorizontal: 15,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'flex-start',
        justifyContent: 'center'
    },
    learningTimeText: {
        fontSize: 12,
        color: '#eb5d49',
        textAlign: 'center',
        fontFamily: Fonts.PoppinsMedium,
    }
});

const modalStyles = StyleSheet.create({
    wrapper: {
        flex: 1,
        margin: 0,
        flexDirection: 'row',
    },
    modalContent: {
        backgroundColor: 'rgba(9,28,39,0.9)',
        width: '70%',
        height: height,
        paddingHorizontal: 20,
        paddingTop: 70,
        paddingBottom: 35,
        justifyContent: 'space-between',
    },
    modalCloseArea: {
        backgroundColor: 'rgba(9,28,39,0.2)',
        width: '30%',
        height: height,
    },
    header: {
        paddingBottom: 15,
        borderBottomWidth: 2,
        borderBottomColor: '#ffffff',

    },
    userPhoto: {
        width: 70,
        height: 80,
        marginBottom: 10,
        alignSelf: 'center',
        // borderRadius: 70
    },
    userName: {
        color: '#ffffff',
        fontSize: 16,
        fontFamily: Fonts.PoppinsBold,
        textAlign: 'center',
    },
    menu: {
        marginTop: 30,
    },
    menuItem: {
        marginVertical: 20,
    },
    menuText: {
        color: Colors.primary,
        fontFamily: Fonts.PoppinsMedium,
        fontSize: 16,
        textAlign: 'center'
    },
    footer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    footerMenu: {

    },
    footerMenuText: {
        color: Colors.primary,
        fontFamily: Fonts.PoppinsRegular,
        fontSize: 12,
    }
});

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navModal: false,
            loading: true,
            activities: [],
            activitiesCount:null,
            socialPosts: [],
            noSocialPosts:false,
            learningData:null,
            whatsNew: [],
            learningCats: [],
            originaltotaltime:0,
            usertotalmins:0,
            userData: {},
            userImage: ''
        }
        this._isMounted = false;
    }

    openSideNav() {
        this.setState({
            navModal: true
        });
    }

    closeSideNav() {
        this.setState({
            navModal: false
        });
    }

    navigateToPage(page) {
        this.setState({
            navModal: false
        }, () => {
            this.props.navigation.navigate(page);
        })
    }

    async componentDidMount() {
        this._isMounted = true;
        await this.getUserData();
        await this.getUserHash();
        await this.getActivities();
        await this.getLearningData();
        // await this.getSocial();
        this.unsubscribeUser = firebase.firestore().collection('users').doc(this.state.userData.user).onSnapshot((snapshot) => {
            let db = snapshot;
            if (this._isMounted) {
                this.setState({
                    userData: db.data()
                });
            }
        });
        this.unsubscribeSocial = await firestore()
            .collection('socialwall')
            .orderBy('accessed_time', "desc")
            .limit(3)
            .onSnapshot((snapshot) => {
                let db = snapshot.docs;
                let social = [];
                db.forEach((value, index) => {
                    social.push({...value.data(),"docId":value.id});
                });
                if(this._isMounted){
                    if(db.length){
                        this.setState({
                            socialPosts: social
                        });
                    }else{
                        this.setState({
                            noSocialPosts:true,
                        })
                    }
                }
            });
        this.setState({
            loading: false
        });
    }

    async getUserData() {
        try {
            const value = await AsyncStorage.getItem('@user')
            if (value !== null) {
                this.setState({
                    userData: JSON.parse(value)
                },() => {
                    this.getProfileImage(this.state.userData.user);
                });
            }
        } catch (e) {
            console.log(e);
        }
    }

    async getProfileImage(uid) {
        const ref = firebase.storage().ref('userImages/' + uid + '.jpg');
        ref.getDownloadURL().then((res) => {
            this.setState({
                userImage: res
            })
        }, () => {
        });
    }

    async getUserHash(){
        this.unsubscribeHash = await firestore()
            .doc(`users/${this.state.userData.user}/userdash/report`)
            .onSnapshot((snapshot) => {
                if(snapshot.exists){
                    let db = snapshot.data();
                    if(this._isMounted){
                        this.setState({
                            activitiesCount:db
                        });
                    }
                }
            });
    }

    async getActivities() {
        this.unsubscribeActivities = await firestore()
            .collection('reportactivity')
            .orderBy('order','asc')
            .onSnapshot((snapshot) => {
                let db = snapshot.docs;
                let activities = [];
                db.forEach((value, index) => {
                    activities.push({
                        ...value.data(),
                        "count":this.state.activitiesCount ? (this.state.activitiesCount[value.data().category] ? this.state.activitiesCount[value.data().category] : 0) : 0
                    });
                });
                this.setState({
                    activities: activities
                });
            });
    }

    componentWillUnmount() {
        this._isMounted = false;
        this.unsubscribeUser();
        this.unsubscribeSocial();
        this.unsubscribeHash();
    }

    toggleSocial(docId,isLike,userId){
        let data = {
            "docId":docId,
            "cheers":isLike,
            "userId":this.state.userData.user,
            "touser":userId
        }
        if (this._isMounted) {
            this.setState({
                loading:true,
            });
        }
        var api = new APILIB();
        let fetchResponse = api.likeSocial(JSON.stringify(data));
        fetchResponse.then(response => {
            response.json().then((res) => {
                if (this._isMounted) {
                    if (this._isMounted) {
                        this.setState({
                            loading:false,
                        });
                    }
                }
            });
        });
        
    }

    checkifliked(likedUsers){
        if(!likedUsers){
            return true;
        }
        
        if(likedUsers.includes(this.state.userData.user)){
            return false;
        }else{
            return true;
        }
    }

    async getLearningData(){
        var api = new APILIB();
        if(this.state.userData.user){
            let fetchResponse = api.learningCats(this.state.userData.user);
            fetchResponse.then(response => {
                response.json().then((res) => {
                    if (this._isMounted) {
                        this.setState({
                            originaltotaltime: res.originaltotaltime,
                            usertotalmins: res.user_total_mins,
                            whatsNew:res.whatsNew,
                            learningCats: res.collectionsName
                        });
                    }
                });
            });
        }
    }

    logout() {
        logout().then(() => {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Login' })],
            });
            this.props.navigation.dispatch(resetAction);
        });
    }

    render() {
        let userImage = this.state.userImage ? { uri: this.state.userImage } : require('../../assets/images/placeholder.png');
        return (
            <SafeAreaView style={styles.wrapper}>
                {this.state.loading &&
                    <View style={Commonstyles.loaderWrapper}>
                        <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                    </View>
                }
                <NavigationEvents onDidFocus={() => this.getLearningData()} />
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => this.openSideNav()} style={styles.menuWrapper}>
                        <Image style={styles.menuIcon} source={require('../../assets/images/icon-menu.png')} />
                    </TouchableOpacity>
                    <Image style={styles.logoText} source={require('../../assets/images/logo-text.png')} />
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Notifications')} style={styles.notifWrapper}>
                        <Image style={styles.notifIcon} source={require('../../assets/images/icon-notif.png')} />
                    </TouchableOpacity>
                </View>


                <Modal style={modalStyles.wrapper}
                    isVisible={this.state.navModal}
                    animationIn={"slideInLeft"}
                    animationOut={"slideOutLeft"}
                >
                    <View style={modalStyles.modalContent}>
                        <View>
                            <View style={modalStyles.header}>
                                <View style={modalStyles.profile}>
                                    <TouchableOpacity onPress={() => this.navigateToPage('EditProfile')}  >
                                        <Image style={modalStyles.userPhoto} source={userImage} ></Image>
                                    </TouchableOpacity>
                                    <Text style={modalStyles.userName}>{this.state.userData.fullName && this.state.userData.fullName.split(" ")[0]}</Text>
                                </View>
                            </View>
                            <View style={modalStyles.menu}>
                                <TouchableOpacity onPress={() => this.navigateToPage('Profile')} style={modalStyles.menuItem}>
                                    <Text style={modalStyles.menuText}>Profile</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.navigateToPage('Help')}  style={modalStyles.menuItem}>
                                    <Text style={modalStyles.menuText}>Help/Support</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.navigateToPage('Rulesgems')}  style={modalStyles.menuItem}>
                                    <Text style={modalStyles.menuText}>Rules And Gems</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.navigateToPage('Rewardsgems')}  style={modalStyles.menuItem}>
                                    <Text style={modalStyles.menuText}>Rewards And Gems</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.navigateToPage('Settings')} style={modalStyles.menuItem}>
                                    <Text style={modalStyles.menuText}>Settings</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.navigateToPage('Faqs')} style={modalStyles.menuItem}>
                                    <Text style={modalStyles.menuText}>FAQs</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.logout()} style={modalStyles.menuItem}>
                                    <Text style={modalStyles.menuText}>Logout</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={modalStyles.footer}>
                            <TouchableOpacity style={{ ...modalStyles.footerMenu }}>
                                <Text style={modalStyles.footerMenuText}>Privacy Policy</Text>
                            </TouchableOpacity>
                            <View style={{ height: 20, width: 1, backgroundColor: Colors.primary }}></View>
                            <TouchableOpacity style={modalStyles.footerMenu}>
                                <Text style={{ ...modalStyles.footerMenuText, textAlign: 'right' }}>Terms &amp; Condition</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TouchableOpacity onPress={() => this.closeSideNav()} style={modalStyles.modalCloseArea}></TouchableOpacity>
                </Modal>


                <ScrollView style={styles.contentWrapper}>
                    <ProfileBox navigation={this.props.navigation} />
                    {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('Rulesgems')} style={{padding:10,marginBottom:10,backgroundColor:Colors.bgBlue}}>
                        <Text style={styles.boxTitle}>Rules And Gems</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Rewardsgems')} style={{padding:10,marginBottom:10,backgroundColor:Colors.bgBlue}}>
                        <Text style={styles.boxTitle}>Rewards And Gems</Text>
                    </TouchableOpacity> */}
                    <View style={Commonstyles.contentBox}>
                        <TouchableOpacity disabled={!this.state.activities.length} onPress={() => this.props.navigation.navigate('ReportsLanding', {
                            activities: this.state.activities
                        })}>
                            {this.state.activities.length ?
                                <React.Fragment>
                                    <View style={Commonstyles.contentBoxHeader}>
                                        <Text style={styles.boxTitle}>Activities</Text>
                                        <View style={styles.boxBtnWrapper}>
                                            <Image style={styles.boxBtnIcon} source={require('../../assets/images/arrowRightGreen.png')} />
                                        </View>
                                    </View>
                                    <View style={styles.activitiesWrapper}>
                                        {
                                            this.state.activities.map((value, index) => (
                                                <View key={index} style={styles.activityItem}>
                                                    <Text style={styles.activityLabel}>{value.category}</Text>
                                                    <View style={styles.activityCountWrapper}>
                                                        <Text style={styles.activityCount}>{value.count}</Text>
                                                    </View>
                                                </View>
                                            ))
                                        }
                                    </View>
                                </React.Fragment>
                                :
                                <View style={Commonstyles.loaderWrapperInline}>
                                    <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                                </View>
                            }
                        </TouchableOpacity>
                    </View>

                    <View style={Commonstyles.contentBox}>
                        {this.state.learningCats.length ?
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('LearningLanding',{
                                whatsNew:this.state.whatsNew,
                                learningCats:this.state.learningCats,
                            })}>
                                <View style={{ ...Commonstyles.contentBoxHeader, borderBottomWidth: 0 }}>
                                    <View style={styles.boxTitleWrapper}>
                                        <Text style={styles.boxTitle}>Learning</Text>
                                        <View style={styles.learningTime}>
                                            <Text style={styles.learningTimeText}>{Math.round(this.state.usertotalmins/60)} Min / {Math.round(this.state.originaltotaltime/60)} Min</Text>
                                        </View>
                                    </View>
                                    <View style={styles.boxBtnWrapper}>
                                        <Image style={styles.boxBtnIcon} source={require('../../assets/images/arrowRightGreen.png')} />
                                    </View>
                                </View>
                                <View style={styles.learningWrapper}>
                                    <View style={{ ...Commonstyles.progressWrapper, flex: 1, marginRight: 0, marginTop: 0 }}>
                                        <View style={{ ...Commonstyles.progressBar, width: `${Math.round(this.state.usertotalmins / 60) / Math.round(this.state.originaltotaltime / 60) * 100}%` }}></View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            :
                            <View style={Commonstyles.loaderWrapperInline}>
                                <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                            </View>
                        }
                    </View>
                    <View style={Commonstyles.contentBox}>
                        {this.state.socialPosts.length ?
                            <React.Fragment>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('SocialWall')} style={Commonstyles.contentBoxHeader}>
                                    <Text style={styles.boxTitle}>Social Wall</Text>
                                    <View style={styles.boxBtnWrapper}>
                                        <Image style={styles.boxBtnIcon} source={require('../../assets/images/arrowRightGreen.png')} />
                                    </View>
                                </TouchableOpacity>
                                <View style={Commonstyles.socialWall}>
                                    {
                                        this.state.socialPosts.map((value, index) => (
                                            <View key={index} style={Commonstyles.socialRow}>
                                                <UserImage uid={value.userId} />
                                                <View style={Commonstyles.socialRowContent}>
                                                    <Text style={Commonstyles.socialPost}><Text style={Commonstyles.socialPostBold}>{value.name}</Text> {value.reason}</Text>
                                                    <View style={Commonstyles.socialRowBottom}>
                                                        <TouchableOpacity onPress={() => this.toggleSocial(value.docId,this.checkifliked(value.likedUsers),value.userId)} style={Commonstyles.cheersWrapper}>
                                                            <Image style={Commonstyles.cheerIcon} source={
                                                                this.checkifliked(value.likedUsers) ? require('../../assets/images/iconHeartBorder.png') : require('../../assets/images/iconHeartBorderFill.png') } />
                                                            <Text style={Commonstyles.cheerCount}>{value.cheers} Cheers</Text>
                                                        </TouchableOpacity>
                                                        <Text style={Commonstyles.socialTime}>{moment(value.accessed_time.toDate()).fromNow()}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        ))
                                    }
                                </View>
                            </React.Fragment>
                            :
                            <React.Fragment>
                                {this.state.noSocialPosts ? null : 
                                    <View style={Commonstyles.loaderWrapperInline}>
                                        <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                                    </View>
                                }
                            </React.Fragment>
                        }
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}
