import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, ActivityIndicator, Dimensions, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import AsyncStorage from '@react-native-community/async-storage';
import APILIB from '../api';
import ProfileBox from './profileBox';

const { width, height } = Dimensions.get('screen');

_isMounted = false;

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    learningWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10,
    },
    learningHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf:'center',
        paddingVertical: 20,
        position: 'relative',
        width: '90%',
        paddingTop:40,
    },
    goBackIconWrapper: {
        marginRight: 30,
        paddingHorizontal:15,
        paddingVertical:10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    headerLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsRegular,
    },
    learningContent: {
        flex: 1,
        width: '90%',
        alignSelf: 'center',
        marginTop: 20
    },
    borderLine: {
        width: '100%',
        height: 1,
        backgroundColor: '#27414f',
        marginBottom: 10
    },
    learningWrapper: {
        flex: 1,
    },
    contentBoxTitle:{
        color:'#ffffff',
        fontSize:16,
        fontFamily:Fonts.PoppinsBold,
    },
    listContent:{
        paddingTop:20,
    },
    listItem:{
        flexDirection:'row',
        alignItems:'center',
        marginBottom: 15,
    },
    bullet:{
        width:7,
        height:8,
        marginRight:10
    },
    listItemText:{
        fontSize:16,
        color:'rgba(255,255,255,0.65)',
        fontFamily:Fonts.PoppinsRegular,
    },
    lItemsWrapper:{
        flexDirection:'row',
        flexWrap:'wrap',
        alignItems:'center',
        justifyContent:'space-between'
    },
    lItem:{
        backgroundColor:'#0e2b3b',
        padding: 20,
        borderWidth:1,
        borderColor:Colors.primary,
        borderRadius:15,
        alignItems:'center',
        width:width/2 - 30,
        height:200,
        marginBottom:20,
    },
    lItemTitleWrapper:{
        height:80,
    },
    lItemTitle:{
        fontSize: 16,
        color:Colors.primary,
        paddingVertical:10,
        marginBottom:10,
        textAlign:'center',
        fontFamily:Fonts.PoppinsMedium,
    },
    lItemCountWrapper:{
        paddingVertical:7,
        paddingHorizontal: 20,
        borderRadius:15,
        backgroundColor:Colors.secondary,
        marginBottom: 20,
    },
    lItemCountText:{
        fontSize: 16,
        color: '#ffffff',
        textAlign:'center',
        fontFamily:Fonts.PoppinsRegular,
    },
    lItemFooter:{
        flexDirection:'row',
        alignSelf:'stretch',
        justifyContent:'space-between',
        alignItems:'center',
        marginBottom:15
    },
    lItemMinutes:{
        color:Colors.textGrey,
        fontSize: 12,
        fontFamily:Fonts.PoppinsRegular,
    }
});

export default class learningLanding extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: {},
            userImage: '',
            loading:false,
            whatsNew:[],
            learningCats:[]
        };
    }

    async componentDidMount() {
        this._isMounted = true;
        await this.getData();
        const { navigation } = this.props;
        if (navigation.getParam('whatsNew')) {
            this.setState({
                whatsNew: navigation.getParam('whatsNew'),
            });
        }
        if (navigation.getParam('learningCats')){
            let learningCats = navigation.getParam('learningCats');
            learningCats.map((value,index) => {
                learningCats[index].completed = 0;
                learningCats[index].completedTime = 0;
            });
            this.setState({
                learningCats: learningCats,
            },() => {
                this.getLearningCount();
            });
        }
    }

    async getLearningCount(){
        this.unsubscribeLearning = await firestore()
            .doc(`users/${this.state.userData.user}/gamification/learning`)
            .onSnapshot((snapshot) => {
                if (snapshot.exists) {
                    let db = snapshot.data();
                    let learningCats = this.state.learningCats;
                    if (this._isMounted) {
                        learningCats.map((value,index) => {
                            if(db[value.name]){
                                learningCats[index].completed = db[value.name];
                                if(db.completed[value.name]){
                                    learningCats[index].completedTime = db.completed[value.name].time;
                                }
                            }
                        });
                        this.setState({
                            learningCats:learningCats
                        });
                    }
                }
            });
    }

    componentWillUnMount(){
        this._isMounted = false;
        this.unsubscribeLearning && this.unsubscribeLearning();
    }

    async getData() {
        try {
            const value = await AsyncStorage.getItem('@user')
            if (value !== null) {
                this.setState({
                    userData: JSON.parse(value)
                });
            }
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        let userImage = this.state.userImage ? { uri: this.state.userImage } : require('../../assets/images/userPhoto.png');
        return (
            <SafeAreaView style={styles.wrapper}>
                <View style={styles.learningWrapper}>
                    <View style={styles.learningHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                        </TouchableOpacity>
                        <Text style={styles.headerLabel}>Learning</Text>
                    </View>
                    <View style={styles.learningContent}>
                        <ScrollView style={styles.learningWrapper}>
                            <ProfileBox navigation={this.props.navigation} />
                            <View style={Commonstyles.contentBox}>
                                <View style={Commonstyles.contentBoxHeader}>
                                    <Text style={styles.contentBoxTitle}>What's New?</Text>
                                </View>
                                <View style={styles.listContent}>
                                    {this.state.whatsNew.length ?
                                        <React.Fragment>
                                            {
                                                this.state.whatsNew.map((value, index) => (
                                                    <View key={index} style={styles.listItem}>
                                                        <Image style={styles.bullet} source={require('../../assets/images/bulletLogo.png')} />
                                                        <Text style={styles.listItemText}>{value}</Text>
                                                    </View>
                                                ))
                                            }
                                        </React.Fragment>
                                        :
                                        <View style={Commonstyles.loaderWrapperInline}>
                                            <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                                        </View>
                                    }
                                </View>
                            </View>
                            <View style={styles.borderLine}></View>
                            <View style={styles.lItemsWrapper}>
                                {this.state.learningCats.length ?
                                    <React.Fragment>
                                    {
                                        this.state.learningCats.map((value,index) => (
                                            <TouchableOpacity key={index} onPress={() => this.props.navigation.navigate('LearningList',{
                                                collections : this.state.learningCats,
                                                selectedCat : value.name,
                                                userid:this.state.userData.user,
                                                userName : this.state.userData.fullName,
                                                managerid:this.state.userData.managerid,
                                            })} style={styles.lItem}>
                                                <View style={styles.lItemTitleWrapper}>
                                                    <Text style={styles.lItemTitle}>{value.name}</Text>
                                                </View>
                                                <View style={styles.lItemCountWrapper}>
                                                    <Text style={styles.lItemCountText}>{value.completed}/{value.size}</Text>
                                                </View>
                                                <View style={styles.lItemFooter}>
                                                    <View style={{...Commonstyles.progressWrapper,flex:1,marginRight:10,marginTop:0}}>
                                                        <View style={{ ...Commonstyles.progressBar, width: Math.round((value.completedTime / value.time) * 100)+'%',height:10 }}>
                                                        </View>
                                                    </View>
                                                    <Text style={styles.lItemMinutes}>{Math.round(value.time/60)} Min</Text>
                                                </View>
                                            </TouchableOpacity>
                                        ))
                                    }
                                    </React.Fragment>
                                    :
                                    <View style={Commonstyles.loaderWrapperInline}>
                                        <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                                    </View>
                                }
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}
