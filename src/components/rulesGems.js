import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, Dimensions, ActivityIndicator, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import moment from 'moment';
import APILIB from '../api';
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    rulesWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10,
    },
    rulesHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf:'center',
        paddingVertical: 20,
        position: 'relative',
        width: '90%',
        paddingTop:40,
    },
    headerLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsRegular,
    },
    goBackIconWrapper: {
        marginRight: 30,
        paddingHorizontal:15,
        paddingVertical:10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    tabWrapper: {
        marginTop: 10,
        flex:1,
    },
    tabsTitle: {
        color: '#fff',
        fontSize: 16,
        fontFamily: Fonts.PoppinsBold,
    },
    tabBtnWrapper: {
        flexDirection: 'row',
        width:'90%',
        alignSelf:'center',
    },
    tabBtn: {
        backgroundColor: '#1a3645',
        width: '50%',
        paddingVertical: 15,
        borderTopLeftRadius: 12,
        borderBottomLeftRadius: 12,
    },
    tabBtnText: {
        color: Colors.textGrey,
        fontSize: 14,
        textAlign: 'center',
        fontFamily: Fonts.PoppinsMedium,
    },
    tabBtnRight: {
        backgroundColor: '#1a3645',
        width: '50%',
        paddingVertical: 15,
        borderTopRightRadius: 12,
        borderBottomRightRadius: 12,
    },
    tabBtnActive: {
        backgroundColor: Colors.primary,
    },
    tabBtnTextActive: {
        color: Colors.secondary,
    },
    rulesContent:{
        borderWidth:1,
        borderColor:Colors.bgBlue,
        borderRadius:20,
        paddingHorizontal:20,
        flex:1,
        width:width - 40,
        marginBottom:40,
    },
    rulesItem:{
        borderBottomWidth:1,
        borderBottomColor:Colors.bgBlue,
        flexDirection:'row',
        justifyContent:'flex-start',
        marginTop:20,
        paddingBottom:20,
        maxWidth:'95%'
    },
    bullet:{
        width:10,
        height:10,
        borderRadius:10,
        backgroundColor:'#2c5a73',
        marginRight:10,
        marginTop:5,
    },
    rulesText:{
        color: '#c3c7c9',
        fontSize: 16,
        textAlign: 'center',
        fontFamily: Fonts.PoppinsMedium,
        textAlign:'left',
    },
    gemsDesc:{
        color: '#c3c7c9',
        fontSize: 16,
        textAlign: 'center',
        fontFamily: Fonts.PoppinsMedium,
        maxWidth:'85%',
        marginBottom:20,
        alignSelf:'center'
    },
    gemsContent:{
        borderWidth:1,
        borderColor:Colors.bgBlue,
        borderRadius:20,
        flex:1,
        width:width - 40,
        marginBottom:40,
    },
    gemsHeader:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingVertical:20,
        paddingHorizontal:10,
        borderBottomWidth:1,
        borderBottomColor:Colors.bgBlue,
    },
    gemsHeaderText:{
        color: '#ffffff',
        fontSize: 16,
        fontFamily: Fonts.PoppinsMedium,
        alignSelf:'center',
        textAlign:'left',
        width:'50%'
    },
    gemsBody:{
        paddingHorizontal:10,
        paddingVertical:20,
    },
    gemsRow:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginBottom:30,
    },
    gemsBadge:{
        flexDirection:'row',
        alignItems:'flex-start',
        width:'50%',
    },
    badgeIcon:{
        width:20,
        height:30,
        marginRight:10
    },
    badgeText:{
        color: '#c3c7c9',
        fontSize: 16,
        textAlign: 'center',
        fontFamily: Fonts.PoppinsRegular,
        alignSelf:'center',
        marginBottom:'auto'
    },
    gemsText:{
        color: '#c3c7c9',
        fontSize: 16,
        textAlign: 'left',
        fontFamily: Fonts.PoppinsRegular,
        width:'50%'
    }
});

export default class rulesGems extends Component {

    constructor(props){
        super(props);
        this.state = {
            activeTab : 1
        }
        this._isMounted = false;
    }

    componentDidMount(){
        this._isMounted = true;
    }

    toggleTab(){
        if(this._isMounted){
            this.setState({
                activeTab : this.state.activeTab == 1 ? 2 : 1,
            });
        }
    }

    render(){
        let tabLeftActive = { ...styles.tabBtn, ...styles.tabBtnActive };
        let tabLeft = { ...styles.tabBtn};
        let tabActiveText = { ...styles.tabBtnText, ...styles.tabBtnTextActive };
        let tabText = { ...styles.tabBtnText };
        let tabRightActive = {...styles.tabBtnRight,...styles.tabBtnActive};
        let tabRight = {...styles.tabBtnRight};
        return(
            <SafeAreaView style={styles.wrapper}>
                <View style={styles.rulesWrapper}>
                    <View style={styles.rulesHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                        </TouchableOpacity>
                        <Text style={styles.headerLabel}>Rules &amp; Gems Info</Text>
                    </View>
                    <View style={styles.tabWrapper}>
                        <View style={styles.tabBtnWrapper}>
                            <TouchableOpacity onPress={() => this.toggleTab()} style={this.state.activeTab == 1 ? tabLeftActive : tabLeft}>
                                <Text style={this.state.activeTab == 1 ? tabActiveText : tabText}>Rules</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.toggleTab()} style={this.state.activeTab == 2 ? tabRightActive : tabRight}>
                                <Text style={this.state.activeTab == 2 ? tabActiveText : tabText}>Gems Table</Text>
                            </TouchableOpacity>
                        </View>
                        {this.state.activeTab == 1 ? 
                            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ marginTop:20 ,alignSelf:'center'}}>
                                <View style={styles.rulesContent}>
                                    <View style={styles.rulesItem}>
                                        <View style={styles.bullet}></View>
                                        <View style={{flex:1}}>
                                            <View style={{flex:1,flexDirection:'row',flexWrap:'wrap'}}>
                                                <Text style={styles.rulesText}>
                                                Every time you see the symbol
                                                </Text>
                                                <Image source={require('../../assets/images/coinReward.png')} resizeMode={'contain'} 
                                            style={{ backgroundColor:'transparent', marginHorizontal:5, marginTop:5, width:15,height:15}} />
                                                <Text style={styles.rulesText}>
                                                you earn points
                                                </Text>
                                            </View>
                                            <View style={{flex:1,flexDirection:'row',flexWrap:'wrap'}}>
                                            <Text style={styles.rulesText}>e.g:</Text>
                                            <Image source={require('../../assets/images/coinReward_3.png')} resizeMode={'contain'} 
                                            style={{ backgroundColor:'transparent', marginHorizontal:5, marginTop:5, width:15,height:15}} />
                                            <Text style={styles.rulesText}>earns you 3 points</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.rulesItem}>
                                        <View style={styles.bullet}></View>
                                        <Text style={styles.rulesText}>
                                        You can earn points by reporting activities, completing learning modules and daily challenges
                                        </Text>
                                    </View>
                                    <View style={styles.rulesItem}>
                                        <View style={styles.bullet}></View>
                                        <Text style={styles.rulesText}>
                                        Daily login to the app earns you one point
                                        </Text>
                                    </View>
                                    <View style={styles.rulesItem}>
                                        <View style={styles.bullet}></View>
                                        <Text style={styles.rulesText}>
                                        Top 5 point earners on the Leaderboard will get special prizes
                                        </Text>
                                    </View>
                                    <View style={styles.rulesItem}>
                                        <View style={styles.bullet}></View>
                                        <Text style={styles.rulesText}>
                                        Badges earn you gems and you can redeem these gems to earn you special prizes
                                        </Text>
                                    </View>
                                    <View style={styles.rulesItem}>
                                        <View style={styles.bullet}></View>
                                        <Text style={styles.rulesText}>
                                        The Leaderboard is reset at the end of every month and the winners are selected based on their position on the Leaderboard
                                        </Text>
                                    </View>
                                </View>
                            </ScrollView>
                        :
                            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ marginTop:20 ,alignSelf:'center'}}>
                                <Text style={styles.gemsDesc}>Badges earn you gems and you can redeem these gems to earn special prizes</Text>
                                <View style={styles.gemsContent}>
                                    <View style={styles.gemsHeader}>
                                        <Text style={styles.gemsHeaderText}>Badge</Text>
                                        <Text style={styles.gemsHeaderText}>Description</Text>
                                    </View>
                                    <View style={styles.gemsBody}>
                                        <View style={styles.gemsRow}>
                                            <View style={styles.gemsBadge}>
                                                <Image style={styles.badgeIcon} source={require('../../assets/images/icon-badge.png')} />
                                                <Text style={styles.badgeText}>Safety Starter</Text>
                                            </View>
                                            <Text style={styles.gemsText}>Earn your easiest badge by completing your profile</Text>
                                        </View>
                                        <View style={styles.gemsRow}>
                                            <View style={styles.gemsBadge}>
                                                <Image style={styles.badgeIcon} source={require('../../assets/images/icon-badge.png')} />
                                                <Text style={styles.badgeText}>Safety Starter</Text>
                                            </View>
                                            <Text style={styles.gemsText}>Earn your easiest badge by completing your profile</Text>
                                        </View>
                                        <View style={styles.gemsRow}>
                                            <View style={styles.gemsBadge}>
                                                <Image style={styles.badgeIcon} source={require('../../assets/images/icon-badge.png')} />
                                                <Text style={styles.badgeText}>Safety Starter</Text>
                                            </View>
                                            <Text style={styles.gemsText}>Earn your easiest badge by completing your profile</Text>
                                        </View>
                                        <View style={styles.gemsRow}>
                                            <View style={styles.gemsBadge}>
                                                <Image style={styles.badgeIcon} source={require('../../assets/images/icon-badge.png')} />
                                                <Text style={styles.badgeText}>Safety Starter</Text>
                                            </View>
                                            <Text style={styles.gemsText}>Earn your easiest badge by completing your profile</Text>
                                        </View>
                                        <View style={styles.gemsRow}>
                                            <View style={styles.gemsBadge}>
                                                <Image style={styles.badgeIcon} source={require('../../assets/images/icon-badge.png')} />
                                                <Text style={styles.badgeText}>Safety Starter</Text>
                                            </View>
                                            <Text style={styles.gemsText}>Earn your easiest badge by completing your profile</Text>
                                        </View>
                                        <View style={styles.gemsRow}>
                                            <View style={styles.gemsBadge}>
                                                <Image style={styles.badgeIcon} source={require('../../assets/images/icon-badge.png')} />
                                                <Text style={styles.badgeText}>Safety Starter</Text>
                                            </View>
                                            <Text style={styles.gemsText}>Earn your easiest badge by completing your profile</Text>
                                        </View>
                                        <View style={styles.gemsRow}>
                                            <View style={styles.gemsBadge}>
                                                <Image style={styles.badgeIcon} source={require('../../assets/images/icon-badge.png')} />
                                                <Text style={styles.badgeText}>Safety Starter</Text>
                                            </View>
                                            <Text style={styles.gemsText}>Earn your easiest badge by completing your profile</Text>
                                        </View>
                                        <View style={styles.gemsRow}>
                                            <View style={styles.gemsBadge}>
                                                <Image style={styles.badgeIcon} source={require('../../assets/images/icon-badge.png')} />
                                                <Text style={styles.badgeText}>Safety Starter</Text>
                                            </View>
                                            <Text style={styles.gemsText}>Earn your easiest badge by completing your profile</Text>
                                        </View>
                                        <View style={styles.gemsRow}>
                                            <View style={styles.gemsBadge}>
                                                <Image style={styles.badgeIcon} source={require('../../assets/images/icon-badge.png')} />
                                                <Text style={styles.badgeText}>Safety Starter</Text>
                                            </View>
                                            <Text style={styles.gemsText}>Earn your easiest badge by completing your profile</Text>
                                        </View>
                                    </View>
                                </View>
                            </ScrollView>
                        }
                    </View>
                </View>
            </SafeAreaView>
        )
    }

}