import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, ActivityIndicator, Dimensions, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import firebase from '@react-native-firebase/app';
import AsyncStorage from '@react-native-community/async-storage';
import APILIB from '../api';

export default class profileBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: {},
            userImage: '',
        };
        this._ismounted = false;
    }

    async componentDidMount() {
        this._ismounted = true;
        await this.getData();
        if (Object.entries(this.state.userData).length) {
            this.unsubscribe = firebase.firestore().collection('users').doc(this.state.userData.user).onSnapshot((snapshot) => {
                let db = snapshot;
                if(this._ismounted){
                    this.setState({
                        userData: db.data()
                    });
                }
            });
        }
    }

    componentWillUnmount(){
        this._ismounted = false;
        this.unsubscribe();
    }

    async getData() {
        try {
            const value = await AsyncStorage.getItem('@user');
            if (value !== null) {
                this.setState({
                    userData: JSON.parse(value)
                }, () => {
                    this.getProfileImage(this.state.userData.user);
                });
            }
        } catch (e) {
            console.log(e);
        }
    }

    async getProfileImage(uid) {
        const ref = firebase.storage().ref('userImages/' + uid + '.jpg');
        ref.getDownloadURL().then((res) => {
            this.setState({
                userImage: res
            })
        }, () => {
        });
    }
    render() {
        let nextPage = this.props.nextPage || 'Profile';
        let userImage = this.state.userImage ? { uri: this.state.userImage } : require('../../assets/images/placeholder.png');
        return (
            <View style={{ ...Commonstyles.contentBox, paddingRight: 5 }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('LeadershipLanding')}>
                    <View style={Commonstyles.userProfile}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Profile')} style={Commonstyles.userAvatar}>
                            <Image style={Commonstyles.userImage} source={userImage} />
                            <View style={Commonstyles.userLevelWrapper}>
                                <Text style={Commonstyles.userLevel}>#{this.state.userData.rank}</Text>
                            </View>
                        </TouchableOpacity>
                        <View style={Commonstyles.userDetails}>
                            <View style={Commonstyles.nameWrapper}>
                                <Text style={Commonstyles.nameMain}>{this.state.userData.fullName && this.state.userData.fullName.split(" ")[0]}</Text>
                                <Text style={Commonstyles.nameTitle}> - {this.state.userData.manager && this.state.userData.manager.split(" ")[0]} Team</Text>
                            </View>
                            <View style={Commonstyles.progressWrapper}>
                                <View style={{ ...Commonstyles.progressBar, width: Math.round((this.state.userData.exp / this.state.userData.maxexp) * 100)+'%' }}>
                                </View>
                                <Text style={Commonstyles.progressBarValue}>{this.state.userData.exp}/{this.state.userData.maxexp}</Text>
                                <View style={Commonstyles.progressNumberWrapper}>
                                    <Text style={Commonstyles.progressNumber}>Level {this.state.userData.level}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={Commonstyles.userPrizes}>
                            <View style={{ ...Commonstyles.prizeBox, marginBottom: 5 }}>
                                <Text style={Commonstyles.prizeText}>{this.state.userData.reward}</Text>
                                <Image style={Commonstyles.prizeIcon} source={require('../../assets/images/icon-coin.png')} />
                            </View>
                            <View style={Commonstyles.prizeBox}>
                                <Text style={Commonstyles.prizeText}>{this.state.userData.gem}</Text>
                                <Image style={Commonstyles.prizeIcon} source={require('../../assets/images/icon-diamond.png')} />
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}