import React, { Component, Fragment } from 'react';
import { View, Text, ActivityIndicator, Image, BackHandler, StyleSheet, TextInput, Picker, Dimensions, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { StackActions, NavigationActions, NavigationEvents } from 'react-navigation';
import { Colors, Commonstyles, Fonts } from '../constants';
import { Dropdown } from 'react-native-material-dropdown';
import DatePicker from 'react-native-datepicker';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import ImagePicker from 'react-native-image-picker';
import md5 from 'md5';
import storage from '@react-native-firebase/storage';

import APILIB from '../api';

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary,
        position: 'relative',
    },
    signUpWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10
    },
    signUpHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 20,
        position: 'relative',
        width: '100%'
    },
    goBackIconWrapper: {
        position: 'absolute',
        left: 15,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    stepLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'center',
        flex: 1,
        fontFamily: Fonts.PoppinsRegular,
    },
    formWrapper: {
        flex: 1,
        alignItems: 'center',
    },
    btnWrapper: {

    },
    datePickerWrapper: {
        width: '80%',
        paddingBottom: 5,
        borderBottomColor: Colors.textGrey,
        borderBottomWidth: 1,
        marginBottom: 50,
        marginTop: -15
    },
    uploadPhotoWrapper: {
        width: '80%',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 40,
        justifyContent: 'flex-start'
    },
    uploadPhotoImg: {
        marginRight: 15,
        width: 70,
        height: 79
    },
    uploadBtnWrapper: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: '#0e2b3b',
        borderRadius: 5
    },
    uploadBtnText: {
        color: Colors.primary,
        fontSize: 10,
        textTransform: 'uppercase',
        fontFamily: Fonts.PoppinsMedium,
    },
    errorText: {
        color: '#ff0000',
        fontFamily: Fonts.PoppinsMedium,
        fontSize: 12,
        marginLeft: '10%',
    }
});


const datePickerStyles = {
    dateIcon: {
        position: 'absolute',
        right: 0,
        top: 15,
        marginLeft: 0,
        width: 20,
        height: 20,
    },
    dateInput: {
        margin: 0,
        borderWidth: 0,
        justifyContent: 'flex-end'
    },
    placeholderText: {
        fontSize: 20,
        color: Colors.textGrey,
        alignSelf: 'flex-start',
        fontFamily: Fonts.PoppinsRegular,
    },
    dateText: {
        fontSize: 20,
        lineHeight: 22,
        color: Colors.textGrey,
        alignSelf: 'flex-start',
        justifyContent: 'flex-end',
        fontFamily: Fonts.PoppinsRegular,
    },
}

let compData = [{
    value: 'Reliance',
}, {
    value: 'Emaar',
}, {
    value: 'Hiranandani',
}, {
    value: 'LnT',
}];

let bloodGrp = [
    { value: 'A+' },
    { value: 'A-' },
    { value: 'B+' },
    { value: 'B-' },
    { value: 'O+' },
    { value: 'O-' },
    { value: 'AB+' },
    { value: 'AB-' }
]

let ImgArrow = <Image style={{ position: 'relative', bottom: -10, width: 16, height: 10 }} source={require('../../assets/images/arrow-down.png')} />;

export default class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            company: null,
            companyId:'',
            empId: '',
            fullName: '',
            mobileNo: '',
            emailId: '',
            bloodGrp: null,
            manager: null,
            managerid:'',
            managerlocation:'',
            step: 1,
            dob: null,
            shiftStartDate: null,
            shiftEndDate: null,
            pwd: '',
            confirmPwd: '',
            errors: [],
            loading: false,
            userImage: null,
            compData:[],
            managers:[],
        }
    }

    onSelect(value, label) {
        this.setState({ company: value });
    }

    onChangeText(value, label) {
        this.setState({
            [label]: value
        });
    }

    async updateManager(company){
        // console.log(company);
        let compObj = this.state.compData.filter(data => {
            return data.value == company;
        });
        this.setState({
            loading:true,
        })
        const documentSnapshot = await firestore()
            .collection('managers')
            .where('cmp_id','==',compObj[0].id)
            .get();
        let db = documentSnapshot.docs;
        let managers = [];   
        db.forEach((value,index) => {
            managers.push({
                "value":value.data().name,
                "location":value.data().location,
                "id":value.id
            });
        });
        this.setState({
            managers:managers,
            companyId:compObj[0].id,
            manager:null,
            loading:false,
        });
    }

    updateManagerId(manager){
        let manObj = this.state.managers.filter(data => {
            return data.value == manager;
        });
        this.setState({
            managerid:manObj[0].id,
            managerlocation:manObj[0].location

        })
    }

    async componentDidMount(){
        this.enableBackHandler();
        this.getCompanies();
    }

    async getCompanies(){
        this.setState({
            loading:true
        })
        const documentSnapshot = await firestore()
            .collection('company')
            .get();
        let db = documentSnapshot.docs;
        let companies = [];
        db.forEach((value,index) => {
            companies.push({
                "value":value.data().name,
                "id":value.id
            });
        });
        this.setState({
            compData:companies,
            loading:false,
        });
    }

    enableBackHandler(){
        let _this = this;
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            if(this.state.step == 2 && this.props.navigation.state.routeName == 'SignUp'){
                _this.setState({
                    step: 1
                });
                return true;
            }
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    goToStep2() {
        let labels = [
            { company: "Please Select Company" },
            { empId: "Please Enter Employee Id" },
            { fullName: "Please Enter Full Name" },
            { mobileNo: "Please Enter Proper Mobile Number" },
            { emailId: "Please Enter Proper Email Id" },
            { dob: "Please Select Birth Date" },
            { bloodGrp: "Please Select Blood Group" },
        ];
        let errors = [];
        let phonenoReg = /^\d{10}$/;
        let emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        labels.forEach((val, index) => {
            var label = Object.keys(val)[0];
            if (!this.state[label]) {
                errors.push(labels[index][label]);
            }
        });
        if (this.state.mobileNo.length > 1 && !this.state.mobileNo.match(phonenoReg)) {
            errors.push("Please Enter Proper Mobile Number")
        }
        if (this.state.emailId.length > 1 && !this.state.emailId.match(emailReg)) {
            errors.push("Please Enter Proper Email Id")
        }
        if (errors.length) {
            this.setState({
                errors: errors
            });
            this.refs._scrollView.scrollTo({ y: 0 }); 
        } else {
            this.setState({
                loading: true,
            });
            var api = new APILIB();
            api.createUser(this.state.mobileNo, this.state.emailId).then((response) => {
                    response.json().then(res => {
                        // console.log(res);
                        let error = [];
                        if(res.code == 403){
                            console.log(res.result.message);
                            error.push(res.result.message);
                            this.setState({
                                errors:error
                            });
                            this.refs._scrollView.scrollTo({y:0}); 
                        } else if (res.code == 200){
                            this.setState({
                                errors: errors,
                                step: 2
                            });
                        }else{
                            error.push("Some technical issue occured");
                            this.setState({
                                errors: error
                            });
                            this.refs._scrollView.scrollTo({ y: 0 }); 
                        }
                    });
                
                this.setState({
                    loading: false,
                });
            });
            // this.setState({
            //     errors: errors,
            //     step: 2
            // });
        }
    }

    goToStep1() {
        this.setState({
            step: 1,
            errors: []
        });
    }

    submitForm() {
        let labels = [
            { manager: "Please Enter Manager" },
            { shiftStartDate: "Please Select Shift Start Date" },
            { shiftEndDate: "Please Select Shift End Date" },
            { pwd: "Please Enter Password" },
            { confirmPwd: "Please Enter Confirm Password" },
        ];
        let errors = [];
        var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
        labels.forEach((val, index) => {
            var label = Object.keys(val)[0];
            if (!this.state[label]) {
                errors.push(labels[index][label]);
            }
        });
        if (this.state.pwd.length > 1 && this.state.confirmPwd.length > 1) {
            if (!this.state.pwd.match(strongRegex)) {
                errors.push("Password should be 8 characters long and contain 1 UpperCase letter, 1 Numeric and 1 Special Character");
            } else if (this.state.pwd !== this.state.confirmPwd) {
                errors.push("Passwords are not matching");
            }
        }
        if(this.state.shiftStartDate.length !== null && this.state.shiftEndDate.length !== null){
            if(this.state.shiftEndDate < this.state.shiftStartDate){
                errors.push("Shift End date should be greater than start date");
            }
        }
        if (errors.length) {
            this.setState({
                errors: errors
            });
            this.refs._scrollView.scrollTo({ y: 0 }); 
        } else {
            this.setState({
                errors: errors,
                step:1,
            });
            let data = {
                "company": this.state.company,
                "company_id":this.state.companyId,
                "empId": this.state.empId,
                "fullName": this.state.fullName,
                "mobile": this.state.mobileNo,
                "emailId": this.state.emailId,
                "dob": this.state.dob,
                "bloodGrp": this.state.bloodGrp,
                "manager": this.state.manager,
                "managerid":this.state.managerid,
                "managerlocation":this.state.managerlocation,
                "shiftStartDate": this.state.shiftStartDate,
                "shiftEndDate": this.state.shiftEndDate,
                "password": md5(this.state.pwd),
                "firsttime" : true,
            }
            this.resetForm();
            // this.backHandler.remove();
            BackHandler.removeEventListener("hardwareBackPress");
            this.props.navigation.navigate('OtpSignUpVerification',{
                userData:data,
                userImage:this.state.userImageData
            });
            // firebase
            //     .auth()
            //     .createUserWithEmailAndPassword(this.state.emailId, this.state.pwd)
            //     .then((res) => {
            //         var api = new APILIB();
            //         console.log(JSON.stringify({ ...data, "user": res.user.uid }));
            //         let fetchResponse = api.postData(JSON.stringify({ ...data, "user": res.user.uid }));

            //         fetchResponse.then(response => {
            //             response.json();
            //         })
            //             .then(responseJson => {
            //                 if (this.state.userImage) {
            //                     const uploadTask = firebase.storage().ref('userImages').child(res.user.uid + '.jpg').putString(this.state.userImageData, 'base64', { contentType: 'image/jpeg' });
            //                     uploadTask.then(
            //                         (response) => {
            //                             this.setState({
            //                                 loading: false
            //                             }, () => this.resetFormAndNavigate(data.mobile));
            //                         },
            //                         (failedReason) => {
            //                             this.setState({
            //                                 loading: false
            //                             }, () => this.resetFormAndNavigate(data.mobile));
            //                         }
            //                     );
            //                 } else {
            //                     this.setState({
            //                         loading: false
            //                     }, () => this.resetFormAndNavigate(data.mobile));
            //                 }
            //             });
            //     })
            //     .catch(error => {
            //         let errors = ["The email address is already in use please use another or contact our technical support"];
            //         this.refs._scrollView.scrollTo({ y: 0 }); 
            //         this.setState({
            //             errors: errors,
            //             loading: false,
            //         })
            //     });
            //this.props.navigation.navigate('MobileVerification');
        }
    }

    resetForm() {
        this.setState({
            company: null,
            companyId:'',
            empId: '',
            fullName: '',
            mobileNo: '',
            emailId: '',
            bloodGrp: null,
            manager: null,
            managerid:'',
            step: 1,
            dob: null,
            shiftStartDate: null,
            shiftEndDate: null,
            pwd: '',
            confirmPwd: '',
            errors: [],
            loading: false,
            userImage: null,
            userImageData:null,
            companies:[],
            managers:[]
        });
    }

    selectPhoto() {
        this.setState({
            loading: true,
        })
        const options = {
            title: 'Select Photo',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                this.setState({
                    loading: false,
                });
            } else if (response.error) {
                this.setState({
                    loading: false,
                });
            } else if (response.customButton) {
                this.setState({
                    loading: false,
                });
            } else {
                const source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    loading: false,
                    userImage: source,
                    userImageData: response.data,
                });
            }
        });
    }

    render() {

        let Step1Comp = <View style={styles.formWrapper}>
            <Dropdown
                containerStyle={Commonstyles.selectInput}
                baseColor={Colors.textGrey}
                textColor={Colors.textGrey}
                itemTextStyle={{
                    fontSize: 30,
                    fontFamily: Fonts.PoppinsRegular,
                }}
                labelTextStyle={{
                    fontFamily: Fonts.PoppinsRegular,
                }}
                fontSize={20}
                inputContainerStyle={{
                    borderBottomWidth: 0, marginBottom: -10,
                }}
                renderAccessory={() => ImgArrow}
                value={this.state.company || 'Select your Company*'}
                onChangeText={(value) => { this.setState({ company: value },() => this.updateManager(value)) }}
                data={this.state.compData}
            />
            <TextInput style={Commonstyles.textInput} value={this.state.empId} onChangeText={(value) => this.onChangeText(value, 'empId')} placeholder="Employee Id*" placeholderTextColor={Colors.textGrey}></TextInput>
            <TextInput style={Commonstyles.textInput} value={this.state.fullName} onChangeText={(value) => this.onChangeText(value, 'fullName')} placeholder="Full Name*" placeholderTextColor={Colors.textGrey}></TextInput>
            <TextInput keyboardType={"number-pad"} style={Commonstyles.textInput} value={this.state.mobileNo} onChangeText={(value) => this.onChangeText(value, 'mobileNo')} placeholder="Mobile Number*" placeholderTextColor={Colors.textGrey}></TextInput>
            <TextInput style={Commonstyles.textInput} value={this.state.emailId} onChangeText={(value) => this.onChangeText(value, 'emailId')} placeholder="Email Id*" placeholderTextColor={Colors.textGrey}></TextInput>
            <DatePicker
                style={styles.datePickerWrapper}
                date={this.state.dob}
                mode="date"
                placeholder="Birth Date"
                format="YYYY-MM-DD"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                iconSource={require('../../assets/images/icon-calendar.png')}
                customStyles={{
                    ...datePickerStyles
                }}
                onDateChange={(dob) => { this.setState({ dob: dob }) }}
            />
            <Dropdown
                containerStyle={Commonstyles.selectInput}
                baseColor={Colors.textGrey}
                textColor={Colors.textGrey}
                itemTextStyle={{
                    fontSize: 30,
                    fontFamily: Fonts.PoppinsRegular,
                }}
                labelTextStyle={{
                    fontFamily: Fonts.PoppinsRegular,
                }}
                fontSize={20}
                inputContainerStyle={{
                    borderBottomWidth: 0, marginBottom: -10,
                    marginTop: -40,
                }}
                renderAccessory={() => ImgArrow}
                dropdownPosition={2}
                value={this.state.bloodGrp || 'Blood Group'}
                onChangeText={(value) => { this.setState({ bloodGrp: value }) }}
                data={bloodGrp}
            />
        </View>;

        let userImageUri = this.state.userImage || require('../../assets/images/imgPlaceholder.png');

        let Step2Comp = <View style={styles.formWrapper}>
            <View style={styles.uploadPhotoWrapper}>
                <Image style={styles.uploadPhotoImg} source={userImageUri} />
                <TouchableOpacity onPress={() => this.selectPhoto()} style={styles.uploadBtnWrapper}>
                    <Text style={styles.uploadBtnText}>Upload Photo</Text>
                </TouchableOpacity>
            </View>
            <Dropdown
                containerStyle={Commonstyles.selectInput}
                baseColor={Colors.textGrey}
                textColor={Colors.textGrey}
                itemTextStyle={{
                    fontSize: 30,
                    fontFamily: Fonts.PoppinsRegular,
                }}
                labelTextStyle={{
                    fontFamily: Fonts.PoppinsRegular,
                }}
                fontSize={20}
                inputContainerStyle={{
                    borderBottomWidth: 0, marginBottom: -10,
                }}
                renderAccessory={() => ImgArrow}
                value={this.state.manager || 'Select your Manager*'}
                onChangeText={(value) => { this.setState({ manager: value },() => this.updateManagerId(value)); }}
                data={this.state.managers}
            />
            <DatePicker
                style={styles.datePickerWrapper}
                date={this.state.shiftStartDate}
                mode="date"
                placeholder="Shift start date*"
                format="YYYY-MM-DD"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                iconSource={require('../../assets/images/icon-calendar.png')}
                customStyles={{
                    ...datePickerStyles
                }}
                onDateChange={(shiftStartDate) => { this.setState({ shiftStartDate: shiftStartDate }) }}
            />
            <DatePicker
                style={styles.datePickerWrapper}
                date={this.state.shiftEndDate}
                mode="date"
                placeholder="Shift end date*"
                format="YYYY-MM-DD"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                iconSource={require('../../assets/images/icon-calendar.png')}
                customStyles={{
                    ...datePickerStyles
                }}
                onDateChange={(shiftEndDate) => { this.setState({ shiftEndDate: shiftEndDate }) }}
            />
            <TextInput secureTextEntry={true} style={Commonstyles.textInput} value={this.state.pwd} onChangeText={(value) => this.onChangeText(value, 'pwd')} placeholder="Password*" placeholderTextColor={Colors.textGrey}></TextInput>
            <TextInput secureTextEntry={true} style={Commonstyles.textInput} value={this.state.confirmPwd} onChangeText={(value) => this.onChangeText(value, 'confirmPwd')} placeholder="Confirm Password*" placeholderTextColor={Colors.textGrey}></TextInput>
        </View>;

        return (
            <Fragment>
                <NavigationEvents onDidFocus={() => this.enableBackHandler()} />
                <SafeAreaView style={styles.wrapper}>
                    {this.state.loading &&
                        <View style={Commonstyles.loaderWrapper}>
                            <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                        </View>
                    }
                    <View style={styles.signUpWrapper}>
                        <View style={styles.signUpHeader}>
                            {this.state.step == 1 ?
                                <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                                    <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => this.goToStep1()} style={styles.goBackIconWrapper}>
                                    <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                                </TouchableOpacity>
                            }
                            <Text style={styles.stepLabel}>Step {this.state.step} Of 2</Text>
                        </View>
                        <ScrollView ref="_scrollView" style={{ flex: 1 }}>
                            {this.state.errors.map((error, index) => (
                                <Text key={index} style={styles.errorText}>{error}</Text>
                            ))}

                            {this.state.step == 1 ? Step1Comp : Step2Comp}

                        </ScrollView>
                        <View>

                            {this.state.step == 1 ?
                                <TouchableOpacity onPress={() => this.goToStep2()} style={{ ...Commonstyles.btnPrimary, borderRadius: 0, width: '100%' }}>
                                    <Text style={Commonstyles.btnPrimaryText}>Next</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => { this.submitForm() }} style={{ ...Commonstyles.btnPrimary, borderRadius: 0, width: '100%' }}>
                                    <Text style={{ ...Commonstyles.btnPrimaryText }}>Sign Up</Text>
                                </TouchableOpacity>
                            }
                        </View>
                    </View>
                </SafeAreaView>
                {/* <SafeAreaView style={{ flex: 0, backgroundColor: Colors.primary }} /> */}
            </Fragment>
        );
    }
}
