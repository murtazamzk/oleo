import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, Dimensions, ActivityIndicator, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import firebase from '@react-native-firebase/app';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    reportWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10,
        paddingTop:20,
    },
    reportHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        paddingVertical: 20,
        position: 'relative',
        width: '90%'
    },
    goBackIconWrapper: {
        marginRight: 30,
        paddingHorizontal:15,
        paddingVertical:10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    headerLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsRegular,
    },
    reportContent:{
        flex:1,
        width:'85%',
        alignSelf:'center',
    },
    reportItem:{
        marginBottom:25,
    },
    label:{
        color: '#ffffff',
        fontSize: 18,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsBold,
        marginBottom:10,
    },
    value:{
        color: Colors.textGrey,
        fontSize: 18,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsRegular,
    }
});

export default class reportDetails extends Component {
    constructor(props){
        super(props);
        this.state = {
            reportDetails:null,
            reportUrl:'',
        }
        this._isMounted = false;
    }
    componentDidMount(){
        this._isMounted = true;
        const { navigation } = this.props;
        if (navigation.getParam('reportDetails')) {
            if(this._isMounted){
                this.setState({
                    reportDetails:navigation.getParam('reportDetails')
                });
                this.getReportImage(navigation.getParam('reportDetails').srno);
            }
        }
    }
    async getReportImage(srno) {
        const ref = firebase.storage().ref('reports/report-' + srno + '.jpg');
        ref.getDownloadURL().then((res) => {
            this.setState({
                reportUrl: res
            })
        }, () => {
        });
    }
    componentWillUnmount(){
        this._isMounted = false;
    }
    render() {
        let nmTypeArr = ["Major","Minor","Fatal"];
        return (
            <SafeAreaView style={styles.wrapper}>
                <View style={styles.reportWrapper}>
                    <View style={styles.reportHeader}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ReportsLanding',{
                            reportAdded: false,
                        })} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                        </TouchableOpacity>
                        <Text style={styles.headerLabel}>Activity Details</Text>
                    </View>
                    <View style={styles.reportContent}>
                        <ScrollView ref="_scrollView" style={{flex:1}} contentContainerStyle={styles.formWrapper}>
                            {this.state.reportDetails && 
                            <React.Fragment>
                                {this.state.reportDetails.category && 
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Category</Text>
                                        <Text style={styles.value}>{this.state.reportDetails.category}</Text>
                                    </View>
                                }
                                {this.state.reportDetails.shortdesc &&
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Short Description</Text>
                                        <Text style={styles.value}>{this.state.reportDetails.shortdesc}</Text>
                                    </View>
                                }
                                {this.state.reportDetails.type &&
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Type</Text>
                                        <Text style={styles.value}>{nmTypeArr[this.state.reportDetails.type - 1]}</Text>
                                    </View>
                                }
                                {this.state.reportDetails.date &&
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Report Date</Text>
                                        <Text style={styles.value}>{this.state.reportDetails.date}</Text>
                                    </View>
                                }
                                {this.state.reportDetails.time &&
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Report Time</Text>
                                        <Text style={styles.value}>{this.state.reportDetails.time}</Text>
                                    </View>
                                }
                                {this.state.reportDetails.zone &&
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Zone</Text>
                                        <Text style={styles.value}>{this.state.reportDetails.zone}</Text>
                                    </View>
                                }
                                {this.state.reportDetails.area ?
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Area Of Occurence</Text>
                                        <Text style={styles.value}>{this.state.reportDetails.area}</Text>
                                    </View>
                                    :
                                    null
                                }
                                {this.state.reportDetails.team.length ?
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Team Members</Text>
                                        <Text style={styles.value}>{this.state.reportDetails.team.join()}</Text>
                                    </View>
                                    :
                                    null
                                }
                                {this.state.reportDetails.casualty ?
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Casualty</Text>
                                        <Text style={styles.value}>{this.state.reportDetails.casualty}</Text>
                                    </View>
                                    :
                                    null
                                }
                                {this.state.reportDetails.lostmanhour ?
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Lost Man Hours</Text>
                                        <Text style={styles.value}>{this.state.reportDetails.lostmanhour}</Text>
                                    </View>
                                    :
                                    null
                                }
                                {this.state.reportDetails.medicalRequired &&
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Medical Assistance Required</Text>
                                        <Text style={styles.value}>{this.state.reportDetails.medicalRequired ? "Yes" : "No"}</Text>
                                    </View>
                                }
                                {this.state.reportDetails.propertyDamaged &&
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Property Damaged</Text>
                                        <Text style={styles.value}>{this.state.reportDetails.propertyDamaged ? "Yes" : "No"}</Text>
                                    </View>
                                }
                                {this.state.reportDetails.envImpact ?
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Environmental Impact</Text>
                                        <Text style={styles.value}>{this.state.reportDetails.envImpact}</Text>
                                    </View>
                                    : null
                                }
                                {this.state.reportDetails.actionTaken ?
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Action Taken</Text>
                                        <Text style={styles.value}>{this.state.reportDetails.actionTaken}</Text>
                                    </View>
                                    :
                                    null
                                }
                                {this.state.reportDetails.report_reason ?
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Report Reason</Text>
                                        <Text style={styles.value}>{this.state.reportDetails.report_reason}</Text>
                                    </View>
                                    :
                                    null
                                }
                                {this.state.reportUrl ?
                                    <View style={styles.reportItem}>
                                        <Text style={styles.label}>Uploaded Image</Text>
                                        <Image style={{height:300}}  source={{uri: this.state.reportUrl}} />
                                    </View>
                                    :
                                    null
                                }
                            </React.Fragment>
                            }
                        </ScrollView>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}
