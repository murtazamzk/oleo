import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, Dimensions, ActivityIndicator, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import moment from 'moment';
import APILIB from '../api';
import AsyncStorage from '@react-native-community/async-storage';
import { Collapse, CollapseHeader, CollapseBody, AccordionList } from "accordion-collapse-react-native";

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    faqsWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10,
    },
    faqsHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        paddingVertical: 20,
        paddingHorizontal:20,
        position: 'relative',
        paddingTop: 40,
        borderBottomColor:Colors.bgBlue,
        borderBottomWidth:1,
    },
    headerLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'left',
        flex: 1,
        fontFamily: Fonts.PoppinsRegular,
    },
    goBackIconWrapper: {
        marginRight: 30,
        paddingHorizontal: 15,
        paddingVertical: 10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    ddMenu: {

    },
    ddMenuIcon: {
        width: 5,
        height: 22,
    },
    faqsContent:{
        width:'90%',
        alignSelf:'center',
        marginTop:20,
        flex:1,
        marginBottom:30,
    },
    faqsIndexWrapper:{
        width:20,
        height:20,
        borderRadius:20,
        backgroundColor:'#2c5a73',
        marginRight:10,
        marginTop:5,
        justifyContent:'center'
    },
    faqsIndex:{
        color: Colors.secondary,
        fontSize: 12,
        marginTop:2,
        textAlign:'center',
        fontFamily: Fonts.PoppinsRegular,
    },
    faqsTitleWrapper:{
        paddingVertical:15,
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor:Colors.bgBlue,
        borderBottomWidth:1,
    },
    faqsTitle:{
        color: '#ffffff',
        fontSize: 16,
        textAlign: 'left',
        flex: 12,
        fontFamily: Fonts.PoppinsRegular,
        marginRight:20,
    },
    faqsDescWrapper:{
        paddingVertical:15,
        paddingLeft:30,
        borderBottomColor:Colors.bgBlue,
        borderBottomWidth:1,
        marginTop:-20,
        backgroundColor:Colors.secondary,
    },
    faqsDesc:{
        color: '#c3c7c9',
        fontSize: 14,
        textAlign: 'left',
        flex: 1,
        fontFamily: Fonts.PoppinsRegular,
    },
    faqsArrow:{
        width:20,
        height:20,
        marginTop:10,
    },
    faqsArrowDown:{
        transform:[{rotate:'180deg'}]
    }
});

const faqlist =[
    {
        index:1,
        title: 'My shift has changed, how do i know make the chnages?',
        body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et'
    },
    {
        index:2,
        title: 'I have changed my nuimber, how do i trabsfer my points to the new number?',
        body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et'
    }
];

export default class faqs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            faqlist : [
                {
                    index:1,
                    title: 'My shift has changed, how do i know make the chnages?',
                    body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et',
                    open:false,
                },
                {
                    index:2,
                    title: 'I have changed my nuimber, how do i trabsfer my points to the new number?',
                    body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et',
                    open:false,
                },
                {
                    index:3,
                    title: 'My location has chnaged, what do i do?',
                    body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et',
                    open:false,
                },
                {
                    index:4,
                    title: 'How do i decide whether the incident is major or minor?',
                    body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et',
                    open:false,
                },
                {
                    index:5,
                    title: 'My shift has changed, how do i know make the chnages?',
                    body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et',
                    open:false,
                },
                {
                    index:6,
                    title: 'I have chnaged my nuimber, how do i trabsfer my points to the new number?',
                    body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et',
                    open:false,
                },
                {
                    index:7,
                    title: 'My location has changed, what do i do?',
                    body: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et',
                    open:false,
                }
            ]
        }
        this._isMounted = false;
    }

    toggleFaq(index){
        let faqlist = this.state.faqlist;
        faqlist[index].open = !faqlist[index].open;
        this.setState({
            faqlist
        });
    }

    _head(item){
        console.log(item);
        
        return(
            <View style={{...styles.faqsTitleWrapper,borderBottomWidth:item.open ? 0 : 1 }}>
                <View style={styles.faqsIndexWrapper}>
                    <Text style={styles.faqsIndex}>{item.index}</Text>
                </View>
                <Text style={styles.faqsTitle}>{item.title}</Text>
                <Image style={ item.open ? {...styles.faqsArrow,...styles.faqsArrowDown} : styles.faqsArrow } source={require('../../assets/images/faqs-arrow.png')} />
            </View>
        );
    }
    
    _body(item){
        return (
            <View style={styles.faqsDescWrapper}>
              <Text style={styles.faqsDesc}>{item.body}</Text>
            </View>
        );
    }

    render() {
        return (
            <SafeAreaView style={styles.wrapper}>
                <View style={styles.faqsWrapper}>
                    <View style={styles.faqsHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                        </TouchableOpacity>
                        <Text style={styles.headerLabel}>FAQs</Text>
                        <TouchableOpacity style={styles.ddMenu}>
                            <Image style={styles.ddMenuIcon} source={require('../../assets/images/icon-dd.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.faqsContent}>

                    <AccordionList
                        extraData={this.state.faqlist}
                        list={this.state.faqlist}
                        header={this._head}
                        body={this._body}
                        onToggle={this.toggleFaq.bind(this)}
                    />
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}
