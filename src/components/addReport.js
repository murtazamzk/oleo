import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, BackHandler, KeyboardAvoidingView, ImageBackground, Dimensions, ActivityIndicator, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import { Dropdown } from 'react-native-material-dropdown';
import moment from 'moment';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-picker';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import DatePicker from 'react-native-datepicker';
import APILIB from '../api';

const { width, height } = Dimensions.get('screen');

const form1Elems = ["Near Miss", "Incident", "Unsafe Act"];

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    reportWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10,
        paddingTop:20,
    },
    reportHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        paddingVertical: 20,
        position: 'relative',
        width: '90%'
    },
    goBackIconWrapper: {
        marginRight: 30,
        paddingHorizontal:15,
        paddingVertical:10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    headerLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsRegular,
    },
    borderLine: {
        width: '100%',
        height: 1,
        backgroundColor: '#27414f',
        marginBottom: 20
    },
    reportContent:{
        flex:1,
        width:'100%',
        alignSelf:'center'
    },
    formWrapper:{
        alignItems:'center',
    },
    textInput:{
        width:'90%'
    },
    inputLabel:{
        color:Colors.textGrey,
        fontSize:20,
        textAlign:'left',
        alignSelf:'flex-start',
        marginLeft:'5%',
        marginBottom: 10,
        fontFamily:Fonts.PoppinsRegular,
    },
    inputSubLabel:{
        color: Colors.textGrey,
        fontSize: 14,
        textAlign: 'left',
        alignSelf: 'flex-start',
        marginLeft: '5%',
        marginTop:5,
        marginBottom: 30,
        fontFamily:Fonts.PoppinsRegular,
    },
    addTMWrapper:{
        flexDirection:'row',
        alignItems:'center',
        backgroundColor:'#0e2b3b',
        borderWidth:1,
        borderColor:Colors.primary,
        borderRadius:15,
        paddingHorizontal: 20,
        paddingVertical:10,
        marginBottom: 50,
        alignSelf:'flex-start',
        marginLeft: '5%',
    },
    addTMText:{
        fontSize:14,
        textTransform:'uppercase',
        color:Colors.primary,
        marginRight: 10,
        fontFamily:Fonts.PoppinsBold,
    },
    addTMIcon:{
        width:15,
        height:15
    },
    teamMembersWrapper:{
        marginBottom:20,
        width:'90%',
    },
    teamMember:{
        backgroundColor:'#0e2b3b',
        borderWidth:1,
        borderColor:Colors.primary,
        borderRadius:15,
        paddingHorizontal: 20,
        paddingVertical:10,
        flexDirection:'row',
        justifyContent:'space-between',
        alignSelf:'flex-start',
        width:200,
        marginBottom:15,
    },
    teamMemberName:{
        fontSize:14,
        textTransform:'uppercase',
        color:Colors.primary,
        marginRight: 10,
        fontFamily:Fonts.PoppinsBold,
    },
    teamMemberRemove:{
        
    },
    teamMemberRemoveText:{
        fontFamily:Fonts.PoppinsRegular,
        color:Colors.primary,
    },
    uploadWrapper:{
        flexDirection: 'row',
        alignItems: 'flex-end',
        alignSelf: 'flex-start',
        marginLeft: '5%',
        marginBottom:50,

    },
    uploadBtn:{
        backgroundColor: '#0e2b3b',
        borderWidth: 1,
        borderColor: Colors.primary,
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginRight:10
    },
    uploadBtnText:{
        fontSize: 14,
        textTransform: 'uppercase',
        color: Colors.primary,
        fontFamily:Fonts.PoppinsBold,
    },
    uploadBtnInfo:{
        paddingBottom: 10,
        // borderBottomWidth:1,
        // borderBottomColor: Colors.textGrey,
        flex:1,
        marginRight: '5%',
    },
    uploadBtnInfoText:{
        fontSize: 10,
        color: Colors.textGrey,
        fontFamily:Fonts.PoppinsRegular,
    },
    errorText: {
        color: '#ff0000',
        width:'100%',
        fontFamily: Fonts.PoppinsMedium,
        fontSize: 12,
        marginLeft: '10%',
        textAlign:'left'
    },
    uploadPhotoImg: {
        alignSelf:'flex-start',
        marginLeft:'10%',
        marginBottom:20,
        width: 80,
        height: 80
    },
    datePickerWrapper: {
        width: '90%',
        paddingBottom: 5,
        borderBottomColor: Colors.textGrey,
        borderBottomWidth: 1,
        marginTop:40,
    },
    optionsWrapper: {

    },
    option: {
        paddingVertical: 12,
        paddingHorizontal: 20,
        borderRadius: 100,
    },
    optionText: {
        color: '#7a848b',
        fontSize: 14,
        fontFamily: Fonts.PoppinsRegular,
        marginLeft: 5,
    },
    checkBoxBorder:{
        ...Commonstyles.radioBtnCircle,
        borderRadius:0
    },
    checkBoxBorderActive:{
        ...Commonstyles.radioBtnCircle,
        ...Commonstyles.radioBtnCircleActive,
        borderRadius:0,
    },
    checkboxInner:{
        ...Commonstyles.radioBtnCircleInner,
        borderRadius:0,
    }
});

const datePickerStyles = {
    dateIcon: {
        position: 'absolute',
        right: 0,
        top: 15,
        marginLeft: 0,
        width: 20,
        height: 20,
    },
    dateInput: {
        margin: 0,
        borderWidth: 0,
        justifyContent: 'flex-end'
    },
    placeholderText: {
        fontSize: 20,
        color: Colors.textGrey,
        alignSelf: 'flex-start',
        fontFamily: Fonts.PoppinsRegular,
    },
    dateText: {
        fontSize: 20,
        lineHeight: 22,
        color: Colors.textGrey,
        alignSelf: 'flex-start',
        justifyContent: 'flex-end',
        fontFamily: Fonts.PoppinsRegular,
    }
}

let zoneData = [{
    value: 'Zone A',
}, {
    value: 'Zone B',
}, {
    value: 'Zone C',
}];

let nmTypeArr = ["Major","Minor","Fatal"];

export default class addReport extends Component {
    constructor(props){
        super(props);
        this.state = {
            userData:{},
            category : null,
            reward :0,
            zone:null,
            nmType : 'Major',
            nmTypeValue : 1,
            maReq : 1,
            reportDate:null,
            reportTime:null,
            propDmg : 1,
            shortDesc:'',
            area:'',
            casualty:'',
            lostManHours:'',
            envImpact:'',
            actionTaken:'',
            reason:'',
            errors: [],
            reportImage: null,
            activities:[],
            formToShow : 1,
            userImage:null,
            teamMemberDialog:false,
            teamMembers:[],
            selectedMembers:[],
            selectedMembersIds:[],
        }
        this._isMounted = false;
    }
    toggleRadio(stType,type) {
        this.setState({
            [stType]: type,
            nmTypeValue: stType == 'nmType' ? nmTypeArr.indexOf(type) + 1 : this.state.nmTypeValue
        });
    }
    onChangeText(value, label) {
        this.setState({
            [label]: value
        })
    }
    async openDialog() {
        this.setState({
            teamMemberDialog: true
        });
    }
    closeDialog() {
        this.setState({
            teamMemberDialog: false,
        })
    }
    toggleTeamMember(value){
        let members = this.state.selectedMembers;
        let membersIds = this.state.selectedMembersIds;
        if(this.state.selectedMembers.includes(value.name)){
            members.splice(members.indexOf(value.name),1);
            membersIds.splice(membersIds.indexOf(value.id),1);
        }else{
            members.push(value.name);
            membersIds.push(value.id);
        }
        this.setState({
            selectedMembers:members,
            selectedMembersIds:membersIds,
        });
    }
    onChaneCategory(value){        
        let activities = this.state.activities;
        let selected = activities.filter((obj,index) => {
            return obj.value == value
        });
        this.setState({
            category: selected[0].value,
            reward:selected[0].reward,
            formToShow:selected[0].type,
        },() => this.resetFormState());
    }
    submitForm() {
        let labelsOne = [
            { shortDesc: "Please Enter Short Description" },
            { zone: "Please Select Zone" },
        ];
        let labelsTwo = [
            { shortDesc: "Please Enter Short Description" },
            { zone: "Please Select Zone" },
            { reason: "Please Enter Reason" },
        ];
        let errors = [];
        let labelsToVerify = [];
        labelsToVerify = form1Elems.includes(this.state.category) ? labelsOne : labelsTwo;
        labelsToVerify.forEach((val, index) => {
            var label = Object.keys(val)[0];
            if (!this.state[label]) {
                errors.push(labelsToVerify[index][label]);
            }
        });
        if (errors.length) {
            this.setState({
                errors: errors
            });
            this.refs._scrollView.scrollTo({ y: 0 }); 
        } else {
            this.setState({
                loading: true,
                errors: errors
            });
            let dataOne = {
                "category":this.state.category,
                "reward":this.state.reward,
                "status": 1,
                "shortdesc":this.state.shortDesc,
                "type":this.state.nmTypeValue,
                "zone":this.state.zone,
                "area":this.state.area,
                "team":this.state.selectedMembers,
                "teamids":this.state.selectedMembersIds,
                "casualty":this.state.casualty,
                "lostmanhour":this.state.lostManHours,
                "medicalRequired":this.state.maReq,
                "propertyDamaged":this.state.propDmg,
                "envImpact":this.state.envImpact,
                "actionTaken":this.state.actionTaken,
                "time":this.state.reportTime,
                "date":this.state.reportDate,
                "user":this.state.userData.user,
                "reported_by":this.state.userData.fullName,
                "reportImage" : this.state.reportImage ? true : false,
                "reason":this.state.userData.fullName + " has reported an "+this.state.category+" and earned "+this.state.reward+" coins",
                "photourl": this.state.userImage,
            }

            let dataTwo = {
                "category":this.state.category,
                "reward":this.state.reward,
                "status": 1,
                "shortdesc":this.state.shortDesc,
                "type":this.state.nmTypeValue,
                "zone":this.state.zone,
                "area":this.state.area,
                "team":this.state.selectedMembers,
                "teamids":this.state.selectedMembersIds,
                "report_reason":this.state.reason,
                "time":this.state.reportTime,
                "date":this.state.reportDate,
                "user":this.state.userData.user,
                "reported_by":this.state.userData.fullName,
                "reportImage" : this.state.reportImage ? true : false,
                "reason":this.state.userData.fullName + " has reported an "+this.state.category+" and earned "+this.state.reward+" coins",
                "photourl": this.state.userImage,
            }

            let dataToPost = form1Elems.includes(this.state.category) ? dataOne : dataTwo;

            console.log(JSON.stringify({...dataToPost,user:this.state.userData.user}));
            
            
            var api = new APILIB();
            let fetchResponse = api.addReport(JSON.stringify({...dataToPost,user:this.state.userData.user}));
            
            fetchResponse.then(response => {
                response.json().then(res => {
                    this.resetFormState();
                    let confirmBtn = null;
                    console.log(res);
                    if (this.state.reportImage) {
                        const uploadTask = firebase.storage().ref('reports').child('report-'+res.result.srno+'.jpg').putString(this.state.reportImageData, 'base64', { contentType: 'image/jpeg' });
                        uploadTask.then(
                            (response) => {
                                console.log(response);
                                
                                this.setState({
                                    loading: false
                                }, () => this.navigateToList());
                            },
                            (failedReason) => {
                                console.log('Failed',failedReason);
                                
                                this.setState({
                                    loading: false
                                }, () => navigateToList());
                            }
                        );
                    }else{
                        this.setState({
                            loading: false
                        }, () => this.navigateToList());
                    }
                });
            })
            .then(responseJson => {
                
            }).catch(error => {
                console.log(error.message);
                this.setState({
                    loading: false,
                });
            });

        }
    }
    async componentDidMount() {
        this._isMounted = true;
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            if(this.state.teamMemberDialog){
                this.setState({
                    teamMemberDialog:false
                });
                return true;
            }else{
                // this.props.navigation.navigate('reportsLanding');
            }
        });
        await this.getData();
        this.unsubscribeMembers = await firestore()
            .collection('users')
            .where('managerid','==',this.state.userData.managerid)
            .onSnapshot((snapshot) => {
                let db = snapshot.docs;
                let members = [];
                db.forEach((value, index) => {
                    if(value.id != this.state.userData.user){
                        members.push({"name":value.data().fullName,"id":value.id});
                    }
                });
                if(this._isMounted){
                    this.setState({
                        teamMembers: members
                    });
                }
            });
        const { navigation } = this.props;
        if (navigation.getParam('activities')) {
            let activitiesObjArr = navigation.getParam('activities');
            let activitiesArr = [];
            activitiesObjArr.map((value,index) => {
                activitiesArr.push(
                    {
                        "value" : value.category,
                        "reward" : value.reward,
                        "type" : value.type
                    }
                );
            });
            this.setState({
                activities:activitiesArr
            });
        }
    }

    componentWillUnmount(){
        this._isMounted = false;
        this.unsubscribeMembers && this.unsubscribeMembers();
    }

    async getData() {
        try {
            const value = await AsyncStorage.getItem('@user')
            if (value !== null) {
                this.setState({
                    userData: JSON.parse(value)
                },() => {
                    this.getProfileImage();
                });
            }
        } catch (e) {
            console.log(e);
        }
    }

    async getProfileImage(uid) {
        const ref = firebase.storage().ref('userImages/' + this.state.userData.user + '.jpg');
        ref.getDownloadURL().then((res) => {
            this.setState({
                userImage: res
            })
        }, () => {
            console.log('Image does not exist');
        });

    }

    async navigateToList(){
        let reward = this.state.reward;
        this.props.navigation.navigate('ReportsLanding',{
            reward:reward,
            reportAdded : true
        });
    }

    resetFormState(){
        if(this._isMounted){
            this.setState({
                zone: null,
                nmType: 'Major',
                nmTypeValue:1,
                maReq: 1,
                propDmg: 1,
                shortDesc: '',
                area: '',
                casualty: '',
                lostManHours: '',
                envImpact: '',
                actionTaken: '',
                reportTime:null,
                reportDate:null,
                reason: '',
                errors: [],
                selectedMembers:[],
                selectedMembersIds:[],
            });
        }
    }

    selectPhoto() {
        this.setState({
            loading: true,
        })
        const options = {
            title: 'Select Photo',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                this.setState({
                    loading: false,
                });
            } else if (response.error) {
                console.log(response.error);
                this.setState({
                    loading: false,
                });
            } else if (response.customButton) {
                this.setState({
                    loading: false,
                });
            } else {
                const source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.setState({
                    loading: false,
                    reportImage: source,
                    reportImageData: response.data,
                });
            }
        });
    }

    render() {
        let reportImageUri = this.state.reportImage;
        let formOne = <View style={{flex:1,width:'100%',alignItems:'center'}}>
            <TextInput style={[Commonstyles.textInput,styles.textInput]} value={this.state.shortDesc} onChangeText={(value) => this.onChangeText(value, 'shortDesc')} placeholder="Short Description*" placeholderTextColor={Colors.textGrey}></TextInput>
            <Text style={styles.inputLabel}>Type *</Text>
            <View style={{...Commonstyles.radioBtnWrapper,width:'90%',justifyContent:'flex-start',marginTop:0,marginBottom: 20,}}>
                <TouchableOpacity onPress={() => this.toggleRadio('nmType','Major')} style={{ ...Commonstyles.radioBtn,marginRight:30}}>
                    <View style={this.state.nmType == 'Major' ? { ...Commonstyles.radioBtnCircle, ...Commonstyles.radioBtnCircleActive } : { ...Commonstyles.radioBtnCircle }}>
                        {this.state.nmType == 'Major' && <View style={Commonstyles.radioBtnCircleInner}></View>}
                    </View>
                    <Text style={this.state.nmType == 'Major' ? { ...Commonstyles.radioBtnText, color: '#ffffff' } : { ...Commonstyles.radioBtnText }}>Major</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.toggleRadio('nmType','Minor')} style={{...Commonstyles.radioBtn,marginRight:30}}>
                    <View style={this.state.nmType == 'Minor' ? { ...Commonstyles.radioBtnCircle, ...Commonstyles.radioBtnCircleActive } : { ...Commonstyles.radioBtnCircle }}>
                        {this.state.nmType == 'Minor' && <View style={Commonstyles.radioBtnCircleInner}></View>}
                    </View>
                    <Text style={this.state.nmType == 'Minor' ? { ...Commonstyles.radioBtnText, color: '#ffffff' } : { ...Commonstyles.radioBtnText }}>Minor</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.toggleRadio('nmType','Fatal')} style={{...Commonstyles.radioBtn,marginRight:30}}>
                    <View style={this.state.nmType == 'Fatal' ? { ...Commonstyles.radioBtnCircle, ...Commonstyles.radioBtnCircleActive } : { ...Commonstyles.radioBtnCircle }}>
                        {this.state.nmType == 'Fatal' && <View style={Commonstyles.radioBtnCircleInner}></View>}
                    </View>
                    <Text style={this.state.nmType == 'Fatal'? { ...Commonstyles.radioBtnText, color: '#ffffff' } : { ...Commonstyles.radioBtnText }}>Fatal</Text>
                </TouchableOpacity>
            </View>
            <Dropdown
                containerStyle={{ ...Commonstyles.selectInput, width: '90%', marginBottom:0 }}
                baseColor={Colors.textGrey}
                textColor={Colors.textGrey}
                itemTextStyle={{
                    fontSize: 30,
                    fontFamily: Fonts.poppinsRegular
                }}
                labelTextStyle={{
                    fontFamily: Fonts.poppinsRegular
                }}
                fontSize={20}
                inputContainerStyle={{
                    borderBottomWidth: 0, marginBottom: -10,
                }}
                value={this.state.zone || 'Zone *'}
                onChangeText={(value) => { this.setState({ zone: value }) }}
                data={zoneData}
            />
            <DatePicker
                style={styles.datePickerWrapper}
                date={this.state.reportDate}
                mode="date"
                placeholder="Report Date"
                format="YYYY-MM-DD"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                iconSource={require('../../assets/images/icon-calendar.png')}
                customStyles={{
                    ...datePickerStyles
                }}
                onDateChange={(reportDate) => { this.setState({ reportDate: reportDate }) }}
            />
            <DatePicker
                style={styles.datePickerWrapper}
                date={this.state.reportTime}
                mode="time"
                placeholder="Report Time"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                iconSource={require('../../assets/images/icon-calendar.png')}
                customStyles={{
                    ...datePickerStyles
                }}
                onDateChange={(reportTime) => { this.setState({ reportTime: reportTime }) }}
            />
            <Text style={styles.inputSubLabel}></Text>
            <TextInput style={[Commonstyles.textInput, styles.textInput]} value={this.state.area} onChangeText={(value) => this.onChangeText(value, 'area')} placeholder="Area of occurance" placeholderTextColor={Colors.textGrey}></TextInput>
            <Text style={styles.inputLabel}>Reported by {this.state.userData.fullName}</Text>
            <View style={styles.teamMembersWrapper}>
                {this.state.teamMembers.map((value,index) => (
                    
                        this.state.selectedMembers.includes(value.name) && 
                        <TouchableOpacity key={index} onPress={() => this.toggleTeamMember(value)} style={styles.teamMember}>
                            <Text style={styles.teamMemberName}>{value.name}</Text>
                            <View style={styles.teamMemberRemove}>
                                <Text style={styles.teamMemberRemoveText}>X</Text>
                            </View>
                        </TouchableOpacity>
                        
                ))}
            </View>
            <TouchableOpacity onPress={() => this.openDialog()} style={styles.addTMWrapper}>
                <Text style={styles.addTMText}>Add Team Member</Text>
                <Image style={styles.addTMIcon} source={require('../../assets/images/iconPlus.png')} />
            </TouchableOpacity>
            <TextInput style={[Commonstyles.textInput, styles.textInput]} value={this.state.casualty} onChangeText={(value) => this.onChangeText(value, 'casualty')} placeholder="Casualty" placeholderTextColor={Colors.textGrey}></TextInput>
            <TextInput keyboardType={"numeric"} style={[Commonstyles.textInput, styles.textInput]} value={this.state.lostManHours} onChangeText={(value) => this.onChangeText(value, 'lostManHours')} placeholder="Lost man hours" placeholderTextColor={Colors.textGrey}></TextInput>
            <Text style={styles.inputLabel}>Medical assistance required</Text>
            <View style={{ ...Commonstyles.radioBtnWrapper, width: '90%', justifyContent: 'flex-start', marginTop: 0, marginBottom: 40, }}>
                <TouchableOpacity onPress={() => this.toggleRadio('maReq',1)} style={{ ...Commonstyles.radioBtn, marginRight: 30 }}>
                    <View style={this.state.maReq == 1 ? { ...Commonstyles.radioBtnCircle, ...Commonstyles.radioBtnCircleActive } : { ...Commonstyles.radioBtnCircle }}>
                        {this.state.maReq == 1 && <View style={Commonstyles.radioBtnCircleInner}></View>}
                    </View>
                    <Text style={this.state.maReq == 1 ? { ...Commonstyles.radioBtnText, color: '#ffffff' } : { ...Commonstyles.radioBtnText }}>Yes</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.toggleRadio('maReq',0)} style={{ ...Commonstyles.radioBtn, marginRight: 30 }}>
                    <View style={this.state.maReq == 0 ? { ...Commonstyles.radioBtnCircle, ...Commonstyles.radioBtnCircleActive } : { ...Commonstyles.radioBtnCircle }}>
                        {this.state.maReq == 0 && <View style={Commonstyles.radioBtnCircleInner}></View>}
                    </View>
                    <Text style={this.state.maReq == 0 ? { ...Commonstyles.radioBtnText, color: '#ffffff' } : { ...Commonstyles.radioBtnText }}>No</Text>
                </TouchableOpacity>
            </View>
            <Text style={styles.inputLabel}>Property Damaged</Text>
            <View style={{ ...Commonstyles.radioBtnWrapper, width: '90%', justifyContent: 'flex-start', marginTop: 0, marginBottom: 20, }}>
                <TouchableOpacity onPress={() => this.toggleRadio('propDmg', 1)} style={{ ...Commonstyles.radioBtn, marginRight: 30 }}>
                    <View style={this.state.propDmg == 1 ? { ...Commonstyles.radioBtnCircle, ...Commonstyles.radioBtnCircleActive } : { ...Commonstyles.radioBtnCircle }}>
                        {this.state.propDmg == 1 && <View style={Commonstyles.radioBtnCircleInner}></View>}
                    </View>
                    <Text style={this.state.propDmg == 1 ? { ...Commonstyles.radioBtnText, color: '#ffffff' } : { ...Commonstyles.radioBtnText }}>Yes</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.toggleRadio('propDmg', 0)} style={{ ...Commonstyles.radioBtn, marginRight: 30 }}>
                    <View style={this.state.propDmg == 0 ? { ...Commonstyles.radioBtnCircle, ...Commonstyles.radioBtnCircleActive } : { ...Commonstyles.radioBtnCircle }}>
                        {this.state.propDmg == 0 && <View style={Commonstyles.radioBtnCircleInner}></View>}
                    </View>
                    <Text style={this.state.propDmg == 0 ? { ...Commonstyles.radioBtnText, color: '#ffffff' } : { ...Commonstyles.radioBtnText }}>No</Text>
                </TouchableOpacity>
            </View>
            <TextInput style={[Commonstyles.textInput, styles.textInput]} value={this.state.envImpact} onChangeText={(value) => this.onChangeText(value, 'envImpact')} placeholder="Environment Impact" placeholderTextColor={Colors.textGrey}></TextInput>
            <TextInput style={[Commonstyles.textInput, styles.textInput]} value={this.state.actionTaken} onChangeText={(value) => this.onChangeText(value, 'actionTaken')} placeholder="Action Taken" placeholderTextColor={Colors.textGrey}></TextInput>
            <Text style={styles.inputLabel}>Upload Photo</Text>
            <Image style={reportImageUri && styles.uploadPhotoImg} source={reportImageUri} />
            <View style={styles.uploadWrapper}>
                <TouchableOpacity onPress={() => this.selectPhoto()} style={styles.uploadBtn}>
                    <Text style={styles.uploadBtnText}>Add Photo</Text>
                </TouchableOpacity>
                <View style={styles.uploadBtnInfo}>
                    {/* <Text style={styles.uploadBtnInfoText}>Click Add Photo to upload the jpg</Text> */}
                </View>
            </View>
        </View>

        let formTwo = <View style={{ flex: 1, width: '100%', alignItems: 'center' }}>
            <TextInput style={[Commonstyles.textInput, styles.textInput,{marginBottom: 0}]} value={this.state.shortDesc} onChangeText={(value) => this.onChangeText(value, 'shortDesc')} placeholder="Short Description*" placeholderTextColor={Colors.textGrey}></TextInput>            
            <Dropdown
                containerStyle={{ ...Commonstyles.selectInput, width: '90%', marginBottom: 0,marginTop:0 }}
                baseColor={Colors.textGrey}
                textColor={Colors.textGrey}
                
                itemTextStyle={{
                    fontSize: 30,
                    fontFamily: Fonts.poppinsRegular
                }}
                labelTextStyle={{
                    fontFamily: Fonts.poppinsRegular
                }}
                fontSize={20}
                inputContainerStyle={{
                    borderBottomWidth: 0, marginBottom: -10,
                }}
                value={this.state.zone || 'Zone *'}
                onChangeText={(value) => { this.setState({ zone: value }) }}
                data={zoneData}
            />
            <DatePicker
                style={styles.datePickerWrapper}
                date={this.state.reportDate}
                mode="date"
                placeholder="Report Date"
                format="YYYY-MM-DD"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                iconSource={require('../../assets/images/icon-calendar.png')}
                customStyles={{
                    ...datePickerStyles
                }}
                onDateChange={(reportDate) => { this.setState({ reportDate: reportDate }) }}
            />
            <DatePicker
                style={styles.datePickerWrapper}
                date={this.state.reportTime}
                mode="time"
                placeholder="Report Time"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                iconSource={require('../../assets/images/icon-calendar.png')}
                customStyles={{
                    ...datePickerStyles
                }}
                onDateChange={(reportTime) => { this.setState({ reportTime: reportTime }) }}
            />
            <Text style={styles.inputSubLabel}></Text>
            <TextInput style={[Commonstyles.textInput, styles.textInput]} value={this.state.area} onChangeText={(value) => this.onChangeText(value, 'area')} placeholder="Area of occurance" placeholderTextColor={Colors.textGrey}></TextInput>
            <Text style={styles.inputLabel}>Reported by {this.state.userData.fullName}</Text>
            <View style={styles.teamMembersWrapper}>
                {this.state.teamMembers.map((value,index) => (
                    
                        this.state.selectedMembers.includes(value.name) && 
                        <TouchableOpacity key={index} onPress={() => this.toggleTeamMember(value)} style={styles.teamMember}>
                            <Text style={styles.teamMemberName}>{value.name}</Text>
                            <View style={styles.teamMemberRemove}>
                                <Text style={styles.teamMemberRemoveText}>X</Text>
                            </View>
                        </TouchableOpacity>
                        
                ))}
            </View>
            <TouchableOpacity onPress={() => this.openDialog()} style={styles.addTMWrapper}>
                <Text style={styles.addTMText}>Add Team Member</Text>
                <Image style={styles.addTMIcon} source={require('../../assets/images/iconPlus.png')} />
            </TouchableOpacity>
            <TextInput style={[Commonstyles.textInput, styles.textInput]} value={this.state.reason} onChangeText={(value) => this.onChangeText(value, 'reason')} placeholder="Reason*" placeholderTextColor={Colors.textGrey}></TextInput>
            <Text style={styles.inputLabel}>Upload Photo</Text>
            <Image style={reportImageUri && styles.uploadPhotoImg} source={reportImageUri} />
            <View style={styles.uploadWrapper}>
                <TouchableOpacity onPress={() => this.selectPhoto()} style={styles.uploadBtn}>
                    <Text style={styles.uploadBtnText}>Add Photo</Text>
                </TouchableOpacity>
                <View style={styles.uploadBtnInfo}>
                    {/* <Text style={styles.uploadBtnInfoText}>Click Add Photo to upload the jpg</Text> */}
                </View>
            </View>
        </View>

        return (
            <SafeAreaView style={styles.wrapper}>
                {this.state.loading &&
                    <View style={Commonstyles.loaderWrapper}>
                        <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                    </View>
                }
                <Dialog
                        visible={this.state.teamMemberDialog}
                        onTouchOutside={() => {
                            this.setState({ teamMemberDialog: false });
                        }}
                        dialogStyle={Commonstyles.dialog}
                        width={width - 30}
                        height={560}
                        overlayOpacity={0.5}
                    >
                    <DialogContent>
                        <View style={{...Commonstyles.dialogContent,height:400}}>
                            {this.state.teamMembers ?
                                <Fragment>
                                    <View style={styles.optionsWrapper}>
                                        <ScrollView style={{height:310}}>
                                            {this.state.teamMembers.map((value,index) => (
                                            <TouchableOpacity onPress={() => this.toggleTeamMember(value)} key={index} style={{ marginBottom: 12 }}>
                                                <ImageBackground style={styles.option} source={require('../../assets/images/radioOptionBg.png')}>
                                                    <View style={{ ...Commonstyles.radioBtn, marginRight: 40 }}>
                                                        <View style={this.state.selectedMembers.includes(value.name) ? styles.checkBoxBorderActive : styles.checkBoxBorder}>
                                                            {this.state.selectedMembers.includes(value.name) ? <View style={styles.checkboxInner} ></View> : null}
                                                        </View>
                                                        <Text style={{ ...styles.optionText, ...styles.optionTextActive }}>{value.name}</Text>
                                                    </View>
                                                </ImageBackground>
                                            </TouchableOpacity>
                                            ))}
                                        </ScrollView>
                                        <View style={{ ...Commonstyles.dialogFooter, width: '100%' }}>
                                            <TouchableOpacity onPress={() => this.closeDialog()} style={{ ...Commonstyles.btnPrimary, width: '100%' }}>
                                                <Text style={Commonstyles.btnPrimaryText}>CONTINUE</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </Fragment>
                                :
                                <View style={Commonstyles.loaderWrapperInline}>
                                    <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                                </View>
                            }
                        </View>
                        {/* <TouchableOpacity onPress={() => this.closeDialog()} style={Commonstyles.dialogCloseBtn}>
                            <Image style={Commonstyles.dialogCloseIcon} source={require('../../assets/images/questionPopupClose.png')} ></Image>
                        </TouchableOpacity> */}
                    </DialogContent>
                </Dialog>
                <View style={styles.reportWrapper}>
                    <View style={styles.reportHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ReportsLanding',{
                            reportAdded: false,
                        })} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                        </TouchableOpacity>
                        <Text style={styles.headerLabel}>Report Activity</Text>
                    </View>
                    <View style={styles.reportContent}>
                        <KeyboardAvoidingView  style={{ flex: 1, flexDirection: 'column',justifyContent: 'center',}} behavior="height" enabled>
                            <ScrollView ref="_scrollView" style={{flex:1}} contentContainerStyle={styles.formWrapper}>
                                {this.state.activities.length ?
                                <Dropdown
                                    containerStyle={{...Commonstyles.selectInput,width:'90%',position:'relative'}}
                                    baseColor={Colors.textGrey}
                                    textColor={Colors.textGrey}
                                    itemTextStyle={{
                                        fontSize: 30
                                    }}
                                    fontSize={20}
                                    inputContainerStyle={{
                                        borderBottomWidth: 0, marginBottom: -10,
                                    }}
                                    value={this.state.category || 'Select a category'}
                                    onChangeText={(value) => this.onChaneCategory(value)}
                                    data={this.state.activities}
                                    itemCount={5}
                                    dropdownPosition={0}
                                />
                                : 
                                null
                                }
                                {this.state.errors.map((error, index) => (
                                    <Text key={index} style={styles.errorText}>{error}</Text>
                                ))}
                                {
                                    this.state.category !== null ?
                                    this.state.formToShow == 1 ? formOne : formTwo
                                    : null
                                }
                                {this.state.category !== null ? <TouchableOpacity onPress={() => this.submitForm()} style={{ ...Commonstyles.btnPrimary, borderBottomLeftRadius: 0, borderBottomRightRadius: 0, width: '100%' }}>
                                    <Text style={Commonstyles.btnPrimaryText}>Submit Report</Text>
                                </TouchableOpacity> : null }
                            </ScrollView>
                        </KeyboardAvoidingView>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}
