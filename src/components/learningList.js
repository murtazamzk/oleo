import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, Dimensions, ActivityIndicator, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import moment from 'moment';
import APILIB from '../api';
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    learningWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10
    },
    learningHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf:'center',
        paddingVertical: 20,
        position: 'relative',
        width: '90%',
        paddingTop:40,
    },
    headerLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsRegular,
    },
    goBackIconWrapper: {
        marginRight: 30,
        paddingHorizontal:15,
        paddingVertical:10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    learningContent:{
        flex:1,
        justifyContent:'flex-start'
    },
    tabScroll:{
        
    },
    tabBtn:{
        paddingVertical:10,
        paddingHorizontal:15,
        borderRadius:15,
        marginRight:15,
        backgroundColor:Colors.bgBlue,
    },
    tabBtnActive:{
        backgroundColor:Colors.primary,
    },
    tabBtnText:{
        fontSize:14,
        color:Colors.textBlue,
        fontFamily:Fonts.PoppinsMedium,
    },
    tabBtnTextActive:{
        color:Colors.secondary
    },
    tabContent:{
        flex:1,
    },
    contentBox:{
        backgroundColor: '#0e2b3b',
        paddingHorizontal: 20,
        paddingVertical: 20,
        borderRadius: 10,
        marginBottom: 20
    },
    itemWrapper:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'flex-start'
    },
    iconMedia:{
        width:40,
        height:40,
        marginRight:20,
        marginTop:5
    },
    itemTop:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
    },
    itemTitle:{
        fontSize:18,
        color:'rgba(255,255,255,0.75)',
        fontFamily:Fonts.PoppinsRegular,
    },
    iconArrow:{
        width:8,
        height:14
    },
    itemBottom:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        marginTop:10
    },
    itemTime:{
        fontSize:12,
        color:Colors.textBlue,
        fontFamily:Fonts.PoppinsRegular,
    },
    completedWrapper:{
        position:'absolute',
        bottom:-15,
        left:0,
    },
    completedText:{
        fontSize:14,
        color:'#ff0000',
        fontFamily:Fonts.PoppinsMedium,
    }
});

export default class learningList extends Component {
    constructor(props){
        super(props);
        this.state = {
            learningCats : [],
            selectedCat : '',
            selectedCatActivities : [],
            screenHeight: 0,
            userData:null,
        }
        this.locArr = [];
    }
    async componentDidMount(){
        const { navigation } = this.props;
        await this.getData();
        if (navigation.getParam('collections')) {
            this.setState({
                learningCats: navigation.getParam('collections')
            });
        }
        if (navigation.getParam('selectedCat')) {
            this.setState({
                selectedCat: navigation.getParam('selectedCat')
            },() => this.getCollection());
        }
    }

    async getData() {
        try {
            const value = await AsyncStorage.getItem('@user');
            if (value !== null) {
                this.setState({
                    userData: JSON.parse(value)
                });
            }
        } catch (e) {
            console.log(e);
        }
    }

    async getCollection(){
        const documentSnapshot = await firestore()
            .collection('gamification/learning/'+this.state.selectedCat)
            .get();
        let db = documentSnapshot.docs;
        let activities = [];
        db.forEach((value, index) => {
            // let isCompleted = this.checkIfCompleted(data);
            activities.push({...value.data(),docId:value.id,isCompleted:false});
        });

        this.updateCompleted(activities);
    }

    async updateCompleted(activities_p){
        let activities = activities_p;
        var i = 0;
        var api = new APILIB();
        for(value of activities){
            let data = {
                "user":this.state.userData.user,
                "id":value.docId,
                "topic":this.state.selectedCat
            }
            let fetchResponse = await api.isLearningCompleted(JSON.stringify(data));
            let jsonResponse = await fetchResponse.json();
            let isCompleted = jsonResponse.isCompleted;
            activities[i].isCompleted = isCompleted;
            i++;            
        }
        this.setState({
            selectedCatActivities : activities
        },() => {
            let index = -1;
            this.state.learningCats.forEach((val, i) => {
                if (this.state.selectedCat == val.name) {
                    index = i;
                }
            });
            this.refs._scrollView.scrollTo({
                x: this.locArr[index],
                y: 0,
                animated: true,
            });   
        });   
    }

    async changeCollection(collectionName){
        if(this.state.selectedCat !== collectionName){
            this.setState({
                selectedCat:collectionName,
                selectedCatActivities:[]
            },() => this.getCollection());
        }
    }

    render() {
        let actTypesImages = ["icon-image","icon-video","icon-pdf"];
        const typeImages = [
            require('../../assets/images/icon-video.png'),
            require('../../assets/images/icon-image.png'),
            require('../../assets/images/icon-pdf.png'),
        ];
        return (
            <SafeAreaView style={styles.wrapper}>
                <View style={styles.learningWrapper}>
                    <View style={styles.learningHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                        </TouchableOpacity>
                        <Text style={styles.headerLabel}>Learning</Text>
                    </View>
                    <View style={styles.learningContent}>
                        <View style={{height:90}}>
                            <ScrollView ref="_scrollView" contentContainerStyle={{marginLeft:'5%',alignItems:'center',height:70}} horizontal={true} style={styles.tabScroll}>
                                {this.state.learningCats.length ?
                                    <React.Fragment>
                                        {
                                            this.state.learningCats.map((value, index) => (
                                                <TouchableOpacity onLayout={event => {
                                                    const layout = event.nativeEvent.layout;
                                                    this.locArr[index] = layout.x;
                                                }} 
                                                    onPress={() => this.changeCollection(value.name)} key={index} style={value.name == this.state.selectedCat ? { ...styles.tabBtn, ...styles.tabBtnActive } : { ...styles.tabBtn }}>
                                                    <Text style={value.name == this.state.selectedCat ? { ...styles.tabBtnText, ...styles.tabBtnTextActive } : { ...styles.tabBtnText }}>{value.name}</Text>
                                                </TouchableOpacity>
                                            ))
                                        }
                                    </React.Fragment>
                                    :
                                    <View style={Commonstyles.loaderWrapperInline}>
                                        <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                                    </View>
                                }
                            </ScrollView>
                        </View>
                        <ScrollView style={{ flex: 1 }} 
                            contentContainerStyle={{width:'90%',alignSelf:'center'}}>
                            <View style={styles.tabContent}>
                                {this.state.selectedCatActivities.length ?
                                    <React.Fragment>
                                        {
                                            this.state.selectedCatActivities.map((value, index) => (
                                                
                                                <View key={index} style={styles.contentBox}>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('LearningPage', {
                                                        mediaUrl: value.item_url,
                                                        type:value.type,
                                                        title:value.title,
                                                        desc:value.desc,
                                                        topic:this.state.selectedCat,
                                                        reward:value.rewards,
                                                        totaltime:value.totaltime,
                                                        userid:this.props.navigation.getParam('userid'),
                                                        userName:this.props.navigation.getParam('userName'),
                                                        subcat:value.subcat,
                                                        docId:value.docId,
                                                        completed:value.isCompleted,
                                                        managerid:this.props.navigation.getParam('managerid')
                                                    })} style={styles.itemWrapper}>
                                                        <Image style={styles.iconMedia} source={typeImages[value.type - 1]} />
                                                        <View>
                                                            <View style={styles.itemTop}>
                                                                <View style={{ width: '75%' }}>
                                                                    <Text numberOfLines={2} style={styles.itemTitle}>{value.title}</Text>
                                                                </View>
                                                                <Image style={styles.iconArrow} source={require('../../assets/images/arrowRightGreen.png')} />
                                                            </View>
                                                            <View style={styles.itemBottom}>
                                                                <View style={{ ...Commonstyles.prizeBox, marginBottom: 5 }}>
                                                                    <Image style={Commonstyles.prizeIcon} source={require('../../assets/images/icon-coin.png')} />
                                                                    <Text style={{ ...Commonstyles.prizeText, fontSize: 12, marginLeft: 5 }}>{value.rewards} Coins</Text>
                                                                </View>
                                                                <Text style={styles.itemTime}>{moment.duration(parseInt(value.totaltime * 1000)).minutes() + " min " + moment.duration(parseInt(value.totaltime * 1000)).seconds() + " sec" }</Text>
                                                            </View>
                                                        </View>
                                                        <View style={styles.completedWrapper}>
                                                            {value.isCompleted ? <Text style={styles.completedText}>Completed</Text> : null}
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            ))
                                        }
                                    </React.Fragment>
                                    :
                                    <View style={Commonstyles.loaderWrapperInline}>
                                        <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                                    </View>
                                }
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}
