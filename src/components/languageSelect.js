import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, Dimensions, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { Colors, Commonstyles, Fonts } from '../constants';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    contentWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10
    },
    langHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 20,
        position: 'relative',
        width: '100%',
        flex:0.1
    },
    goBackIconWrapper: {
        position: 'absolute',
        left: 15,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    langLabelHeader: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'center',
        flex: 1,
        marginLeft: -100,
        fontFamily:Fonts.PoppinsRegular,
    },
    langLabelWrapper:{
        width: '70%',
        alignSelf:'center',
        flex:0.9,
        justifyContent:'center'
    },
    langLabelEn:{
        fontSize: 20,
        color:Colors.textGrey,
        textAlign:'center',
        fontFamily:Fonts.PoppinsRegular,
    },
    langLabelHindi:{
        fontSize: 20,
        marginTop:10,
        color: Colors.textGrey,
        textAlign: 'center',
        fontFamily:Fonts.PoppinsRegular,
    }
});

export default class LanguageSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
        language : 1
    };
  }

  toggleLanguage(lang){
      this.setState({
          language : lang
      })
  }

  render() {
    return (
        <SafeAreaView style={styles.wrapper}>
            <View style={styles.contentWrapper}>
                <View style={styles.langHeader}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                        <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                    </TouchableOpacity>
                    <Text style={styles.langLabelHeader}>App Language</Text>
                </View>
                <View style={styles.langLabelWrapper}>
                    <Text style={styles.langLabelEn}>
                        Please select your language
                    </Text>
                    <Text style={styles.langLabelHindi}>
                        अपनी भाषा चुनिए।
                    </Text>
                    <View style={Commonstyles.radioBtnWrapper}>
                        <TouchableOpacity onPress={() => this.toggleLanguage(1)} style={{ ...Commonstyles.radioBtn,marginRight: 40}}>
                            <View style={this.state.language ? { ...Commonstyles.radioBtnCircle, ...Commonstyles.radioBtnCircleActive } : { ...Commonstyles.radioBtnCircle}}>
                                {this.state.language == 1 && <View style={Commonstyles.radioBtnCircleInner}></View>}
                            </View>
                            <Text style={this.state.language ? { ...Commonstyles.radioBtnText, color: '#ffffff' } : { ...Commonstyles.radioBtnText}}>English</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.toggleLanguage(0)} style={Commonstyles.radioBtn}>
                            <View style={!this.state.language ? { ...Commonstyles.radioBtnCircle, ...Commonstyles.radioBtnCircleActive } : { ...Commonstyles.radioBtnCircle }}>
                                {this.state.language == 0 && <View style={Commonstyles.radioBtnCircleInner}></View>}
                            </View>
                            <Text style={!this.state.language ? { ...Commonstyles.radioBtnText, color: '#ffffff' } : { ...Commonstyles.radioBtnText }}>हिंदी</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')} style={{ ...Commonstyles.btnPrimary, borderRadius: 0, width: '100%' }}>
                    <Text style={{ ...Commonstyles.btnPrimaryText }}>DONE</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
  }
}
