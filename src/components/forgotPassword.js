import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Dimensions, TouchableOpacity, SafeAreaView, KeyboardAvoidingView } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { Colors, Commonstyles, Fonts } from '../constants';

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary,
    },
    authHeader: {
        height: 170,
        backgroundColor: Colors.primary,
        borderBottomRightRadius: 50,
        borderBottomLeftRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        width: 120,
        height: 135,
        position: 'absolute',
        top: 100
    },
    formWrapper: {
        paddingTop: 180,
        alignItems: 'center'
    },
    submitBtn: {
        marginTop: 10
    },
})


export default class ForgotPassword extends Component {
    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: Colors.primary }} />
                <SafeAreaView style={styles.wrapper}>
                    <KeyboardAvoidingView style={{ flex: 1 }} behavior="position" enabled>
                        <View style={styles.authHeader}>
                            <Image style={styles.logo} source={require('../../assets/images/logo-fill.png')}></Image>
                        </View>
                        <View style={styles.formWrapper}>
                            <TextInput style={Commonstyles.textInput} returnKeyType={"done"} placeholder="Enter your new password" placeholderTextColor={Colors.textGrey} secureTextEntry={true}></TextInput>
                            <TextInput style={Commonstyles.textInput} returnKeyType={"done"} placeholder="Confirm new password" placeholderTextColor={Colors.textGrey} secureTextEntry={true}></TextInput>
                            <TouchableOpacity onPress={() => {this.props.navigation.navigate("LanguageSelect")}} style={{ ...Commonstyles.btnPrimary, ...styles.submitBtn }}>
                                <Text style={Commonstyles.btnPrimaryText}>
                                    Submit
                                    </Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </Fragment>
        )
    }
}
