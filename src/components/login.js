import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, KeyboardAvoidingView, ActivityIndicator, Dimensions, TouchableOpacity, SafeAreaView } from 'react-native';
import { StackActions, NavigationActions, NavigationEvents } from 'react-navigation';
import { Colors, Commonstyles, Fonts } from '../constants';
import md5 from 'md5';
import firebase from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth';
import APILIB from '../api';
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary,
        justifyContent: 'space-between'
    },
    authHeader: {
        height: 200,
        backgroundColor: Colors.primary,
        borderBottomRightRadius: 50,
        borderBottomLeftRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    authHeaderTitle: {
        fontSize: 20,
        color: Colors.secondary,
        marginTop: -50,
        fontFamily: Fonts.PoppinsRegular,
    },
    logo: {
        width: 120,
        height: 135,
        position: 'absolute',
        top: 140
    },
    formWrapper: {
        paddingBottom: 120,
        alignItems: 'center',
        justifyContent:'center',
        height:height - 50,
    },
    pwdWrapper: {
        position: 'relative',
        width: '100%',
        alignItems: 'center'
    },
    forgotPwdWrapper: {
        position: 'absolute',
        right: '10%',
        bottom: 15
    },
    forgotPwdText: {
        color: Colors.textGrey,
        fontSize: 14,
        fontFamily: Fonts.PoppinsRegular,
    },
    loginBtn: {
        marginTop: 10
    },
    signUpWrapper: {
        marginTop: 40,
        flexDirection: 'row'
    },
    signUpWrapperLabel: {
        fontSize: 14,
        color: Colors.textGrey,
        fontFamily: Fonts.PoppinsRegular,
    },
    signUpWrapperCTA: {
        marginLeft: 5
    },
    signUpWrapperCTAText: {
        fontSize: 14,
        color: Colors.primary,
        fontWeight: '600',
        fontFamily: Fonts.PoppinsMedium
    },
    errorText: {
        color: '#ff0000',
        fontFamily: Fonts.PoppinsMedium,
        fontSize: 12,
        marginLeft: '10%',
        alignSelf: 'flex-start',
        marginBottom: 10,
    },
    togglePassword:{
        position: 'absolute',
        right: '10%',
        top: 15
    },
    togglePasswordIcon:{
        width:30,
        height:15.5
    }
})


export default class Login extends Component {

    //username: '9820333623',
    // password: 'Test@123',

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            emailId: '',
            errors: [],
            loading: false,
            signup: false,
            pwdchanged: false,
            pwdHidden:true,
        }
        
    }

    handleLogin() {
        let labels = [
            { username: "Please Enter Mobile Number" },
            { password: "Please Enter Password" },
        ];
        let errors = [];
        let emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        let phonenoReg = /^\d{10}$/;
        labels.forEach((val, index) => {
            var label = Object.keys(val)[0];
            if (!this.state[label]) {
                errors.push(labels[index][label]);
            }
        });
        if (this.state.username.length > 1 && !this.state.username.match(phonenoReg)) {
            errors.push("Please Enter 10 Digit Mobile Number")
        }
        if (errors.length) {
            this.setState({
                errors: errors
            });
        } else {
            this.setState({
                errors: errors,
                loading: true,
            });
            var api = new APILIB();
            let fetchResponse = api.verifyUser(this.state.username, md5(this.state.password));
            fetchResponse.then(response => {
                response.json().then((res) => {
                    if (res.result.message === 'Invalid Credentials') {
                        let errors = [];
                        errors.push("Invalid Mobile No or Password");
                        this.setState({
                            errors: errors,
                            loading: false,
                        });
                    } else {
                        // console.log(res.result.emp_email_primary);
                        this.setState({
                            emailId: res.result.emp_email_primary
                        },()=>{
                            this.loginUser();
                        });
                    }
                }).catch(error => {
                    let errors = [];
                    errors.push("Technical issue occured please try later");
                    this.setState({
                        errors: errors,
                        loading: false,
                    });
                });
            });
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        const { navigation } = nextProps;
        if (navigation.getParam('signup')) {
            return {
                signup: navigation.getParam('signup')
            }
        } else if (navigation.getParam('pwdchanged')) {
            return {
                pwdchanged: navigation.getParam('pwdchanged')
            };
        } else {
            return {
                signup: false,
                pwdchanged: false
            }
        }
    }

    loginUser() {
        firebase.auth().signInWithEmailAndPassword(this.state.emailId, md5(this.state.password))
            .then((res) => {
                let uid = res.user.uid;
                let db = firebase.firestore()
                    .collection('users')
                    .doc(uid)
                    .get();

                db.then((res) => {
                    if (res.data()) {
                        this.storeData(res.data()).then(() => {
                            this.setState({
                                loading: false,
                            }, () => {
                                this.props.navigation.navigate('Home');
                            });
                        })
                    }
                });
            })
            .catch((error) => {
                let errors = [];
                if (error.code === 'auth/wrong-password') {
                    errors.push("Invalid Mobile No or Password");
                } else {
                    errors.push("Technical issue occured please try again");
                    console.log(error);
                }
                this.setState({
                    errors: errors,
                    loading: false,
                });
            });
    }

    async storeData(userData) {
        try {
            await AsyncStorage.setItem('@user', JSON.stringify(userData));
        } catch (e) {
            console.log(e);
        }
    }

    async componentDidMount() {
        if (firebase.auth().currentUser) {
            await firebase.auth().signOut();
        }
        console.log(firebase.auth().currentUser);
        const { navigation } = this.props;
        if (navigation.getParam('signup')) {
            this.setState({
                signup: navigation.getParam('signup')
            });
        }
        if (navigation.getParam('pwdchanged')) {
            this.setState({
                pwdchanged: navigation.getParam('pwdchanged')
            });
        }
    }

    verifyProps(){
        const { navigation } = this.props;
        if (navigation.getParam('signup')) {
            this.setState({
                signup: navigation.getParam('signup')
            });
        }else{
            this.setState({
                signup: false
            });
        }
        if (navigation.getParam('pwdchanged')) {
            this.setState({
                pwdchanged: navigation.getParam('pwdchanged')
            });
        }else{
            this.setState({
                pwdchanged: false
            });
        }
    }

    render() {
        return (
            <Fragment>
                <NavigationEvents onDidFocus={() => this.verifyProps()} />
                <SafeAreaView style={{ flex: 0, backgroundColor: Colors.primary }} />
                {this.state.loading &&
                    <View style={Commonstyles.loaderWrapper}>
                        <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                    </View>
                }
                <SafeAreaView style={styles.wrapper}>
                    <KeyboardAvoidingView style={{ flex: 1 }} behavior="position" enabled>
                        <View style={styles.authHeader}>
                            <Text style={styles.authHeaderTitle}>Welcome To Oleo</Text>
                            <Image style={styles.logo} source={require('../../assets/images/logo-fill.png')}></Image>
                        </View>
                        <View style={styles.formWrapper}>
                            <View style={{width:'90%',alignItems:'center'}}>
                                {this.state.errors.map((error, index) => (
                                    <Text key={index} style={styles.errorText}>{error}</Text>
                                ))}
                                {this.state.signup && <Text style={{ fontFamily: Fonts.PoppinsRegular, color: Colors.primary, fontSize: 16 }}>
                                    Signup complete you can login now
                                </Text>}
                                {this.state.pwdchanged && <Text style={{ fontFamily: Fonts.PoppinsRegular, color: Colors.primary, fontSize: 16 }}>
                                    Password changed you can login now
                                </Text>}
                                <TextInput onChangeText={(value) => this.setState({ username: value })} value={this.state.username} keyboardType={"numeric"} style={Commonstyles.textInput} placeholder="Mobile Number" placeholderTextColor={Colors.textGrey}></TextInput>
                                <View style={styles.pwdWrapper}>
                                    <TextInput onChangeText={(value) => this.setState({ password: value })} value={this.state.password} style={Commonstyles.textInput} placeholder="Password" placeholderTextColor={Colors.textGrey} secureTextEntry={this.state.pwdHidden}></TextInput>
                                    <TouchableOpacity onPress={() => this.setState({pwdHidden:!this.state.pwdHidden})} style={styles.togglePassword}>
                                        <Image style={styles.togglePasswordIcon} source={require('../../assets/images/icon-eye.png')}></Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('MobileVerification')} style={styles.forgotPwdWrapper}>
                                        <Text style={styles.forgotPwdText}>Forgot Password?</Text>
                                    </TouchableOpacity>
                                </View>
                                <TouchableOpacity onPress={() => this.handleLogin()} style={{ ...Commonstyles.btnPrimary, ...styles.loginBtn }}>
                                    <Text style={Commonstyles.btnPrimaryText}>
                                        Login
                                        </Text>
                                </TouchableOpacity>
                                <View style={styles.signUpWrapper}>
                                    <Text style={styles.signUpWrapperLabel}>
                                        Don't have an account?
                                    </Text>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('SignUp')} style={styles.signUpWrapperCTA}>
                                        <Text style={styles.signUpWrapperCTAText}>Sign up</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </Fragment>
        )
    }
}
