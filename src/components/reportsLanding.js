import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, ImageBackground, BackHandler, Dimensions, ActivityIndicator, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import { StackActions, NavigationActions, NavigationEvents } from 'react-navigation';
import firebase from '@react-native-firebase/app';
import AsyncStorage from '@react-native-community/async-storage';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import ProfileBox from './profileBox';
import APILIB from '../api';
import moment from 'moment';

const { width, height } = Dimensions.get('screen');

const statusArr = ["Pending for acceptance","Pending for action","Rejected","Duplicate","Noted","RCA Done","Corrective action taken"];

const statusColors = ["#297bc7","#297bc7","#df403b","#29c756","#29c756","#29c756","#29c756"];

_isMounted = false;

_alertDone = false;

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    reportWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10,
        paddingTop:20,
    },
    reportHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        paddingVertical: 20,
        position: 'relative',
        width: '90%'
    },
    goBackIconWrapper: {
        marginRight: 30,
        paddingHorizontal:15,
        paddingVertical:10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    headerLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'left',
        flex: 1,
        fontFamily:Fonts.PoppinsRegular,
    },
    addReportBtnWrapper:{

    },
    addReportBtn:{
        width:25,
        height:25,
    },
    reportContent: {
        flex: 1,
        width: '90%',
        alignSelf: 'center',
        marginTop: 20
    },
    borderLine: {
        width: '100%',
        height: 1,
        backgroundColor: '#27414f',
        marginBottom: 20
    },
    activityWrapper: {
        flex: 1,
    },
    activityTitle: {
        color: 'rgba(255,255,255,0.75)',
        fontSize: 16,
        fontFamily:Fonts.PoppinsBold,
    },
    activityDate: {
        color: 'rgba(255,255,255,0.5)',
        fontSize: 12,
        fontFamily:Fonts.PoppinsRegular,
    },
    activityItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        marginTop: 15
    },
    activityStatusWrapper: {
        flex: 0.8
    },
    statusDetails: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    statusLabel: {
        fontSize: 12,
        color: 'rgba(255,255,255,0.5)',
        fontFamily:Fonts.PoppinsRegular,
    },
    statusColor: {
        paddingVertical: 5,
        paddingHorizontal: 7,
        borderRadius: 3,
        backgroundColor: '#cccccc'
    },
    statusColorText: {
        color: '#ffffff',
        fontSize: 10,
        textTransform: 'uppercase',
        fontFamily:Fonts.PoppinsMedium,
    },
    statusMetaWrapper: {
        marginTop: 5
    },
    statusMetaText: {
        fontSize: 12,
        color: 'rgba(255,255,255,0.5)',
        marginTop: 5,
        fontFamily:Fonts.PoppinsRegular,
    },
    userPrizes: {
        flex: 0.2,
        alignItems: 'flex-end',
        justifyContent: 'space-between',
    },
    prizeBox: {
        backgroundColor: Colors.secondary,
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    prizeIcon: {
        width: 16,
        height: 16,
        marginRight: 5
    },
    prizeText: {
        color: Colors.primary,
        fontSize: 16,
        fontFamily:Fonts.PoppinsRegular,
    },
    rewardDialogHead: {
        paddingTop: 30,
    },
    rewardDialogContent: {
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    rewardDialogTitle: {
        fontFamily: Fonts.PoppinsBold,
        fontSize: 28,
        marginBottom: 7,
        textAlign: 'center',
    },
    rewardDialogSubTitle: {
        fontFamily: Fonts.PoppinsRegular,
        fontSize: 16,
        color: '#6c767e',
        textAlign: 'center',
    },
    coinWrapper: {
        width: 150,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 15,
    },
    pointsText: {
        fontSize: 50,
        textAlign: 'center',
        fontFamily: Fonts.PoppinsBold,
        color: '#744900',
        marginTop: 10,
    },
    tipText: {
        color: '#848c92',
        fontFamily: Fonts.PoppinsRegular,
        textAlign: 'center',
    },
});

export default class reportsLanding extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: {},
            userImage: '',
            reportCat:[],
            reportList:[],
            noReports:false,
            alertDone:false,
            rewardDialog:false,
            reward:0,
        };
    }

    async componentDidMount() {
        this._isMounted = true;
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            if(this.state.rewardDialog){
                this.setState({
                    rewardDialog:false
                });
                return true;
            }else{
                // this.props.navigation.navigate('Home');
            }
        });
        await this.getData();
        this.getReports();
        const { navigation } = this.props;
        if (navigation.getParam('activities')) {
            this.setState({
                reportCat:navigation.getParam('activities')
            });

        }
    }

    async getData() {
        try {
            const value = await AsyncStorage.getItem('@user')
            if (value !== null) {
                this.setState({
                    userData: JSON.parse(value)
                });
            }
        } catch (e) {
            console.log(e);
        }
    }

    componentDidUpdate(){
        
    }

    verifyProps(){
        const { navigation } = this.props;
        if (navigation.getParam('reportAdded')) {
            this.getReports();
            this.setState({
                rewardDialog:true,
                reward:navigation.getParam('reward')
            });
            this.props.navigation.setParams({reportAdded: false });
        }
    }

    componentWillUnmount(){
        this.setState({
            alertDone:false
        });
        this._isMounted = false;
    }

    getReports(){
        var api = new APILIB();
        let fetchResponse = api.getReports(this.state.userData.user);
        fetchResponse.then(response => {
            response.json().then((res) => {
                console.log(res);
                if (this._isMounted) {
                    if(res.reports.length){
                        this.setState({
                            reportList:res.reports
                        });
                    }else{
                        this.setState({
                            noReports:true,
                        });
                    }
                }
            });
        });
    }

    openDetails(index)  {
        console.log(this.state.reportList[index]);
        this.props.navigation.navigate('ReportDetails',{
            reportDetails:this.state.reportList[index]
        });
    }

    closeDialog() {
        this.setState({
            rewardDialog:false,
        })
    }

    
    render() {
        let userImage = this.state.userImage ? { uri: this.state.userImage } : require('../../assets/images/userPhoto.png');
        return (
            <Fragment>
                <NavigationEvents onDidFocus={() => this.verifyProps()} />
                <Dialog
                        visible={this.state.rewardDialog}
                        onTouchOutside={() => {
                            this.setState({ rewardDialog: false });
                        }}
                        dialogStyle={Commonstyles.dialog}
                        width={width - 30}
                        height={560}
                        overlayOpacity={0.5}
                    >
                        <DialogContent>
                            <View style={Commonstyles.dialogContent}>
                                <View style={styles.rewardDialogHead}>
                                    <Text style={styles.rewardDialogTitle}>Congratulations</Text>
                                    <Text style={styles.rewardDialogSubTitle}>You have earned {this.state.reward} Coins</Text>
                                </View>
                                <View style={styles.rewardDialogContent}>
                                    <ImageBackground style={styles.coinWrapper} source={require('../../assets/images/coinReward.png')}>
                                        <Text style={styles.pointsText}>+{this.state.reward}</Text>
                                    </ImageBackground>
                                    <Text style={styles.tipText}>Tip : Earn more points by reporting positive observations</Text>
                                </View>
                                <View style={{ ...Commonstyles.dialogFooter, width: '100%' }}>
                                    <TouchableOpacity onPress={() => this.closeDialog('rewardDialog')} style={{ ...Commonstyles.btnPrimary, width: '100%' }}>
                                        <Text style={Commonstyles.btnPrimaryText}>CONTINUE</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {/* <TouchableOpacity onPress={() => this.closeDialog('rewardDialog')} style={Commonstyles.dialogCloseBtn}>
                                <Image style={Commonstyles.dialogCloseIcon} source={require('../../assets/images/questionPopupClose.png')} ></Image>
                            </TouchableOpacity> */}
                        </DialogContent>
                    </Dialog>
                <SafeAreaView style={styles.wrapper}>
                    <View style={styles.reportWrapper}>
                        <View style={styles.reportHeader}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                                <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                            </TouchableOpacity>
                            <Text style={styles.headerLabel}>Activities</Text>
                            {
                                this.state.reportCat.length ? <TouchableOpacity onPress={() => {
                                    this._alertDone = false;
                                    this.props.navigation.navigate('AddReport',{
                                        activities:this.state.reportCat
                                    })}
                                } style={styles.addReportBtnWrapper}>
                                    <Image style={styles.addReportBtn} source={require('../../assets/images/iconPlus.png')}></Image>
                                </TouchableOpacity> : null
                            }
                        </View>
                        <View style={styles.reportContent}>
                            <ScrollView style={styles.activityWrapper}>
                                <ProfileBox navigation={this.props.navigation} />
                                <View style={styles.borderLine}></View>
                                {this.state.reportList.length ?
                                        <React.Fragment>
                                            {
                                                this.state.reportList.map((value, index) => (
                                                    <TouchableOpacity onPress={() => this.openDetails(index)} key={index} style={Commonstyles.contentBox}>
                                                        <View style={Commonstyles.contentBoxHeader}>
                                                            <Text style={styles.activityTitle}>Activity No. {value.srno}</Text>
                                                            <Text style={styles.activityDate}>
                                                                {value.time && value.date + " | " + value.time}
                                                            </Text>
                                                        </View>
                                                        <View style={styles.activityItem}>
                                                            <View style={styles.activityStatusWrapper}>
                                                                <View style={styles.statusDetails}>
                                                                    <Text style={styles.statusLabel}>Status - </Text>
                                                                    <View style={{ ...styles.statusColor, backgroundColor: statusColors[value.status - 1] }}>
                                                                        <Text style={styles.statusColorText}>{statusArr[value.status - 1]}</Text>
                                                                    </View>
                                                                </View>
                                                                <View style={styles.statusMetaWrapper}>
                                                                    <Text style={styles.statusMetaText}>Category - {value.category}</Text>
                                                                </View>
                                                            </View>
                                                            <View style={styles.userPrizes}>
                                                                <View style={styles.prizeBox}>
                                                                    <Image style={styles.prizeIcon} source={require('../../assets/images/icon-coin.png')} />
                                                                    <Text style={styles.prizeText}>{value.reward}</Text>
                                                                </View>
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                                ))
                                            }
                                        </React.Fragment>
                                        :
                                        <React.Fragment>
                                            {this.state.noReports ? null :
                                                <View style={Commonstyles.loaderWrapperInline}>
                                                    <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                                                </View>
                                            }
                                        </React.Fragment>
                                }
                            </ScrollView>
                        </View>
                        {/* <TouchableOpacity onPress={() => this.props.navigation.navigate('AddReport')} style={{ ...Commonstyles.btnPrimary, borderBottomLeftRadius: 0, borderBottomRightRadius: 0, width: '100%' }}>
                            <Text style={Commonstyles.btnPrimaryText}>Report Activity</Text>
                        </TouchableOpacity> */}
                    </View>
                </SafeAreaView>
                <SafeAreaView style={{ flex: 0, backgroundColor: Colors.primary }} />
            </Fragment>
        )
    }
}
