import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Dimensions, BackHandler, ActivityIndicator, TouchableOpacity, ScrollView, SafeAreaView } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { Colors, Commonstyles, Fonts } from '../constants';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import moment from 'moment';
import APILIB from '../api';
import UserImage from './userImageLoop'; 

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary,
        justifyContent: 'space-between',
        position:'relative',
    },
    profileHeader: {
        backgroundColor: Colors.primary,
        borderBottomRightRadius: 50,
        borderBottomLeftRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 20,
        paddingTop:50,
    },
    goBackIconWrapper: {
        alignSelf: 'flex-start',
        marginLeft: 25,
        paddingHorizontal:15,
        paddingVertical:10,
        zIndex: 1
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    profileName: {
        fontSize: 20,
        color: Colors.secondary,
        marginTop: 10,
        fontFamily:Fonts.PoppinsBold,
    },
    profileSubName: {
        fontSize: 12,
        color: '#0a5f41',
        marginTop: 5,
        fontFamily:Fonts.PoppinsMedium,
    },
    userImage: {
        marginTop: 10,
        width: 80,
        height: 90,
    },
    userProgressData: {
        flexDirection: 'row',
        width: '70%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    progressWrapper: {
        backgroundColor: '#42bc64',
        borderRadius: 15,
        flex: 1,
        marginRight: 45,
        position: 'relative'
    },
    progressBorder: {
        position: 'absolute',
        right: -50,
        top: -5,
        width: 1,
        height: 30,
        backgroundColor: '#0caa5c'
    },
    progressBar: {
        height: 18,
        backgroundColor: '#dc624a',
        borderRadius: 18,
        justifyContent: 'center',
        alignItems:'center',
    },
    progressBarValue: {
        textAlign: 'center',
        fontSize: 10,
        fontFamily:Fonts.PoppinsMedium,
        lineHeight:18,
        position: 'absolute',
        color:'#fff',
        left: '50%',
        top: 1
    },
    progressNumberWrapper: {
        position: 'absolute',
        top: -3,
        right: -45,
        width: 55,
        height: 25,
        backgroundColor: Colors.secondary,
        borderRadius: 25,
        borderColor: '#638599',
        borderWidth: 1,
        justifyContent: 'center'
    },
    progressNumber: {
        color: '#dc624a',
        textAlign: 'center',
        fontSize: 11,
        fontFamily:Fonts.PoppinsMedium,
    },
    userPrizes: {
        flex: 0.3,
        alignItems: 'flex-end',
        justifyContent: 'space-between',
        paddingLeft: 10,
    },
    prizeBox: {
        backgroundColor: '#42bc64',
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    prizeIcon: {
        width: 15,
        height: 15,
    },
    prizeText: {
        color: Colors.secondary,
        fontSize: 10,
        marginRight: 5,
        fontFamily:Fonts.PoppinsMedium,
    },
    cheersBtnWrapper: {
        marginTop: 15,
    },
    cheersBtn: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 7,
        backgroundColor: Colors.secondary,
        borderRadius: 25,
    },
    cheersText: {
        fontSize: 10,
        color: '#7ca7bf',
        marginHorizontal: 5,
        fontFamily:Fonts.PoppinsRegular,
    },
    iconHeart: {
        width: 12,
        height: 11
    },
    iconArrow: {
        width: 12,
        height: 11
    },
    profileContent: {
        flex: 1,
        width: '90%',
        alignSelf: 'center',
        marginTop: 20
    },
    contentBox: {
        backgroundColor: '#0e2b3b',
        paddingHorizontal: 15,
        paddingVertical: 15,
        borderRadius: 10,
        marginBottom: 20
    },
    countBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 30,
        paddingVertical: 25,
    },
    countItem: {
        alignItems: 'center'
    },
    countWrapper: {
        paddingVertical: 5,
        paddingHorizontal: 15,
        borderRadius: 20,
        backgroundColor: Colors.secondary
    },
    count: {
        fontSize: 20,
        color: '#ffffff',
        fontFamily:Fonts.PoppinsLight,
    },
    countLabel: {
        fontSize: 14,
        color: Colors.textBlue,
        marginTop: 7,
        fontFamily:Fonts.PoppinsRegular,
    },
    contentBoxTitle: {
        fontSize: 18,
        color: '#ffffff',
        fontFamily:Fonts.PoppinsBold,
    },
    awardItem: {
        marginVertical: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    awardBullet: {
        width: 10,
        height: 10,
        backgroundColor: Colors.textBlue,
        borderRadius: 10,
        marginRight: 10
    },
    awardTitle: {
        fontSize: 16,
        color: Colors.textBlue,
        fontFamily:Fonts.PoppinsRegular,
    },
    awardBg: {
        position: 'absolute',
        width: 90,
        height: 87,
        right: 20,
        bottom: 0
    },
    dialog: {
        backgroundColor: '#0e2b3b',
        paddingHorizontal: 20,
        paddingVertical: 15,
        alignItems: 'center'
    },
    dialogContent: {
        width: '90%',
        alignSelf: 'center',
        alignItems:'center'
    },
    dialogHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: width - 100,
        borderBottomWidth: 1,
        borderBottomColor: '#27414f',
        marginBottom:10,
        paddingBottom:5
    },
    dialogCloseBtn: {
        width: 25,
        height: 18,
        alignItems: 'center',
    },
    dialogCloseIcon: {
        width: 12,
        height: 12,
    },
    dialogCountContent: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: width - 140,
        paddingTop: 15,
        paddingHorizontal:10,
    },
    dialogcountItem: {
        alignItems: 'center'
    },
    dialogcountWrapper: {
        paddingVertical: 3,
        paddingHorizontal: 12,
        borderRadius: 15,
        backgroundColor: Colors.secondary
    },
    dialogcount: {
        fontSize: 14,
        color: '#ffffff',
        fontFamily:Fonts.PoppinsRegular,
    },
    dialogcountLabel: {
        fontSize: 10,
        color: Colors.textBlue,
        marginTop: 5,
        fontFamily:Fonts.PoppinsRegular,
    },
})


export default class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cheersDialogVisible: false,
            userData:{},
            socialPosts:[],
            badges:[],
            NoBadges:false,
            loading:false,
        }
        this._isMounted = false;
    }
    openDialog() {
        this.setState({
            cheersDialogVisible: true
        })
    }
    closeDialog(){
        this.setState({
            cheersDialogVisible: false
        })
    }
    async componentDidMount(){
        this._isMounted = true;
        await this.getData();
        if (Object.entries(this.state.userData).length) {
            this.unsubscribe = firebase.firestore().collection('users')
            .doc(this.state.userData.user)
            .onSnapshot((snapshot) => {
                let db = snapshot;
                if(this._isMounted){
                    this.setState({
                        userData: db.data()
                    });
                }
            });
            this.unsubscribeSocial = await firestore()
            .collection('socialwall')
            .where('userId','==',this.state.userData.user)
            .onSnapshot((snapshot) => {
                let db = snapshot.docs;
                let social = [];
                db.forEach((value, index) => {
                    social.push({...value.data(),"docId":value.id});
                });
                if(this._isMounted){
                    this.setState({
                        socialPosts: social
                    });
                }
            });
            this.unsubscribeBadges = await firestore()
            .doc('users/'+this.state.userData.user+'/userdash/badges')
            .onSnapshot((snapshot) => {
                if(snapshot.exists){
                    let db  = snapshot.data();
                    console.log(db.badges);
                    
                    if(db.badges.length){
                        if(this._isMounted){
                            this.setState({
                                badges:db.badges
                            });
                        }
                    }else{
                        if(this._isMounted){
                            this.setState({
                                NoBadges:true,
                            });
                        }
                    }
                }else{
                    if(this._isMounted){
                        this.setState({
                            NoBadges:true,
                        });
                    }
                }
            });
        }
    }
    componentWillUnmount(){
        this._isMounted = false;
        this.unsubscribe();
        this.unsubscribeSocial();
        this.unsubscribeBadges();
    }
    enableBackHandler(){
        let _this = this;
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            if(this.state.cheersDialogVisible){
                _this.setState({
                    cheersDialogVisible: false
                });
                return true;
            }
        });
    }
    async getData() {
        try {
            const value = await AsyncStorage.getItem('@user');
            if (value !== null) {
                this.setState({
                    userData: JSON.parse(value)
                }, () => {
                    this.getProfileImage(this.state.userData.user);
                });
            }
        } catch (e) {
            console.log(e);
        }
    }

    async getProfileImage(uid) {
        const ref = firebase.storage().ref('userImages/' + uid + '.jpg');
        ref.getDownloadURL().then((res) => {
            this.setState({
                userImage: res
            })
        }, () => {
            console.log('Image does not exist');
        });

    }
    toggleSocial(docId,isLike,userId){
        let data = {
            "docId":docId,
            "cheers":isLike,
            "userId":this.state.userData.user,
            "touser":userId
        }
        if (this._isMounted) {
            this.setState({
                loading:true,
            });
        }
        var api = new APILIB();
        let fetchResponse = api.likeSocial(JSON.stringify(data));
        fetchResponse.then(response => {
            response.json().then((res) => {
                if (this._isMounted) {
                    this.setState({
                        loading:false,
                    });
                }
            });
        });
        
    }

    checkifliked(likedUsers){
        if(!likedUsers){
            return true;
        }
        
        if(likedUsers.includes(this.state.userData.user)){
            return false;
        }else{
            return true;
        }
    }
    render() {
        let userImage = this.state.userImage ? { uri: this.state.userImage } : require('../../assets/images/placeholder.png');
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: Colors.primary }} />
                <SafeAreaView style={styles.wrapper}>
                    {this.state.loading &&
                        <View style={Commonstyles.loaderWrapper}>
                            <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                        </View>
                    }
                    <Dialog
                        visible={this.state.cheersDialogVisible}
                        onTouchOutside={() => {
                            this.setState({ cheersDialogVisible: false });
                        }}
                        dialogStyle={styles.dialog}
                        width={width - 60}
                        height={140}
                        overlayOpacity={0.8}
                    >
                        <DialogContent>
                            <View style={styles.dialogContent}>
                                <View style={styles.dialogHeader}>
                                    <View style={{ ...styles.cheersBtn, backgroundColor: null, paddingHorizontal: 0, paddingVertical: 0, paddingBottom: 10 }}>
                                        <Image style={styles.iconHeart} source={require('../../assets/images/iconHeartFill.png')} />
                                        <Text style={styles.cheersText}> {this.state.userData.cheers} Cheers</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => this.closeDialog()} style={styles.dialogCloseBtn}>
                                        <Image style={styles.dialogCloseIcon} source={require('../../assets/images/iconClose.png')} />
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.dialogCountContent}>
                                    <View style={styles.dialogcountItem}>
                                        <View style={styles.dialogcountWrapper}>
                                            <Text style={styles.dialogcount}>{this.state.userData.teamCheerCount || 0}</Text>
                                        </View>
                                        <Text style={styles.dialogcountLabel}>My Team</Text>
                                    </View>
                                    <View style={styles.dialogcountItem}>
                                        <View style={styles.dialogcountWrapper}>
                                            <Text style={styles.dialogcount}>{this.state.userData.managerCheerCount || 0}</Text>
                                        </View>
                                        <Text style={styles.dialogcountLabel}>Managers</Text>
                                    </View>
                                    <View style={styles.dialogcountItem}>
                                        <View style={styles.dialogcountWrapper}>
                                            <Text style={styles.dialogcount}>{this.state.userData.othersCheerCount || 0}</Text>
                                        </View>
                                        <Text style={styles.dialogcountLabel}>Others</Text>
                                    </View>
                                </View>
                            </View>
                        </DialogContent>
                    </Dialog>
                    <View style={styles.profileHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrowDark.png')} />
                        </TouchableOpacity>
                        <Image style={styles.userImage} source={userImage}></Image>
                        <Text style={styles.profileName}>{this.state.userData.fullName}</Text>
                        <Text style={styles.profileSubName}>{this.state.userData.manager && this.state.userData.manager.split(" ")[0]} Team</Text>
                        <View style={styles.userProgressData}>
                            <View style={styles.progressWrapper}>
                                <View style={{ ...styles.progressBar, width: Math.round((this.state.userData.exp / this.state.userData.maxexp) * 100)+'%' }}>
                                </View>
                                <Text style={styles.progressBarValue}>{this.state.userData.exp} / {this.state.userData.maxexp}</Text>
                                <View style={styles.progressNumberWrapper}>
                                    <Text style={styles.progressNumber}>Level {this.state.userData.level}</Text>
                                </View>
                                <View style={styles.progressBorder}></View>
                            </View>
                            <View style={styles.userPrizes}>
                                <View style={{ ...styles.prizeBox, marginBottom: 5 }}>
                                    <Text style={styles.prizeText}>{this.state.userData.reward}</Text>
                                    <Image style={styles.prizeIcon} source={require('../../assets/images/icon-coin.png')} />
                                </View>
                                <View style={styles.prizeBox}>
                                    <Text style={styles.prizeText}>{this.state.userData.gem}</Text>
                                    <Image style={styles.prizeIcon} source={require('../../assets/images/icon-diamond.png')} />
                                </View>
                            </View>
                        </View>
                        <View style={styles.cheersBtnWrapper}>
                            <TouchableOpacity onPress={() => this.openDialog()} style={styles.cheersBtn}>
                                <Image style={styles.iconHeart} source={require('../../assets/images/iconHeartFill.png')}></Image>
                                <Text style={styles.cheersText}>{this.state.userData.cheers} Cheers</Text>
                                <Image style={styles.iconArrow} source={require('../../assets/images/iconArrowRightGreen.png')}></Image>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <ScrollView style={styles.profileContent}>
                        <View style={{ ...styles.contentBox, ...styles.countBox }}>
                            <View style={styles.countItem}>
                                <View style={styles.countWrapper}>
                                    <Text style={styles.count}>{this.state.userData.activity_count ? this.state.userData.activity_count : 0 }</Text>
                                </View>
                                <Text style={styles.countLabel}>Activities</Text>
                            </View>
                            <View style={styles.countItem}>
                                <View style={styles.countWrapper}>
                                    <Text style={styles.count}>{this.state.userData.total_points ? this.state.userData.activity_count : 0 }</Text>
                                </View>
                                <Text style={styles.countLabel}>Learning</Text>
                            </View>
                            <View style={styles.countItem}>
                                <View style={styles.countWrapper}>
                                    <Text style={styles.count}>{this.state.userData.challenge_attempts ? this.state.userData.challenge_attempts.length : 0 }</Text>
                                </View>
                                <Text style={styles.countLabel}>Challenges</Text>
                            </View>
                        </View>
                        <View style={{ ...styles.contentBox, position: 'relative' }}>
                            <Text style={styles.contentBoxTitle}>Achievements</Text>
                            {this.state.badges.length ?
                                <React.Fragment>
                                    {this.state.badges.map((value,index) => (
                                        <View key={index} style={styles.awardItem}>
                                            <View style={styles.awardBullet}></View>
                                            <Text style={styles.awardTitle}>{value}</Text>
                                        </View>
                                    ))}
                                    
                                </React.Fragment>
                                :
                                <React.Fragment>
                                    {this.state.NoBadges ? <View style={{height:50}}></View> :
                                        <View style={Commonstyles.loaderWrapperInline}>
                                            <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                                        </View>
                                    }
                                </React.Fragment>
                            }
                            <Image style={styles.awardBg} source={require('../../assets/images/awardBg.png')}></Image>
                        </View>
                        <View style={styles.contentBox}>
                            {
                                this.state.socialPosts.map((value, index) => (
                                    <View key={index} style={Commonstyles.socialRow}>
                                        <UserImage uid={value.userId} />
                                        <View style={Commonstyles.socialRowContent}>
                                            <Text style={Commonstyles.socialPost}><Text style={Commonstyles.socialPostBold}>{value.name}</Text> {value.reason}</Text>
                                            <View style={Commonstyles.socialRowBottom}>
                                                    <TouchableOpacity onPress={() => this.toggleSocial(value.docId,this.checkifliked(value.likedUsers),value.userId)} style={Commonstyles.cheersWrapper}>
                                                        <Image style={Commonstyles.cheerIcon} source={
                                                            this.checkifliked(value.likedUsers) ? require('../../assets/images/iconHeartBorder.png') : require('../../assets/images/iconHeartBorderFill.png') } />
                                                        <Text style={Commonstyles.cheerCount}>{value.cheers} Cheers</Text>
                                                    </TouchableOpacity>
                                                    <Text style={Commonstyles.socialTime}>{moment(value.accessed_time.toDate()).fromNow()}</Text>
                                            </View>
                                        </View>
                                    </View>
                                ))
                            }
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Fragment>
        )
    }
}
