import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, ToastAndroid, ActivityIndicator, TextInput, Dimensions, TouchableOpacity, SafeAreaView, KeyboardAvoidingView } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { Colors, Commonstyles, Fonts } from '../constants';

import auth from '@react-native-firebase/auth';
import firebase from '@react-native-firebase/app';

import APILIB from '../api';

const { width, height } = Dimensions.get('screen');

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary,
    },
    authHeader: {
        height: 170,
        backgroundColor: Colors.primary,
        borderBottomRightRadius: 50,
        borderBottomLeftRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        width: 120,
        height: 135,
        position: 'absolute',
        top: 100
    },
    formWrapper: {
        paddingTop: 200,
        alignItems: 'center'
    },
    submitBtn: {
        marginTop: 10
    },
    otpWrapper: {
        flexDirection: 'row',
        width: '80%',
        marginBottom: 30
    },
    otpInputWrapper: {
        flex: 1,
        height: 60,
        marginHorizontal: 5,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: Colors.textGrey,
        // paddingVertical: 5
        alignItems: 'center',
    },
    otpInput: {
        flex: 1,
        color: '#ffffff',
        fontSize: 22,
        height: 40,
        lineHeight: 40,
        textAlign: 'center',
        fontFamily: Fonts.PoppinsRegular,
        position: 'relative',
        top: 3,
    },
    otpLabel: {
        position: 'absolute',
        top: 100,
        color: '#ffffff',
        fontSize: 16,
        fontFamily: Fonts.PoppinsRegular,
    },
    resendOTPWrapper: {
        flexDirection: 'row',
        marginTop: 40
    },
    resendOTPLabel: {
        color: '#ffffff',
        fontSize: 16,
        fontFamily: Fonts.PoppinsRegular,
    },
    resendOTPBtnWrapper: {

    },
    resendOTPBtn: {
        color: Colors.primary,
        fontSize: 16,
        fontFamily: Fonts.PoppinsRegular,
    },
    errorText: {
    color: '#ff0000',
    fontFamily: Fonts.PoppinsMedium,
    fontSize: 12,
    marginLeft: '10%',
}
});

export default class OtpSignUpVerification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            otpSent: true,
            otp: [],
            userData: '',
            mobileNo: '',
            error: '',
            userImage:null,
            loading: false,
        }
        this.otpTextInput = [];
        this.confirmation;
    }
    renderInputs() {
        const inputs = Array(6).fill(0);
        const txt = inputs.map(
            (i, j) => <View key={j} style={styles.otpInputWrapper}>
                <TextInput
                    style={[styles.otpInput, { borderRadius: 10 }]}
                    keyboardType="numeric"
                    returnKeyType={"done"}
                    value={this.state.otp[j]}
                    onChangeText={v => this.focusNext(j, v)}
                    onKeyPress={e => this.focusPrevious(e.nativeEvent.key, j)}
                    ref={ref => this.otpTextInput[j] = ref}
                />
            </View>
        );
        return txt;
    };
    focusPrevious(key, index) {
        if (key === 'Backspace' && index !== 0)
            this.otpTextInput[index - 1].focus();
    }

    focusNext(index, value) {
        let otps = this.state.otp;
        otps[index] = value;
        this.setState({
            otp: otps
        });
        if (index < this.otpTextInput.length - 1 && value) {
            this.otpTextInput[index + 1].focus();
        }
        if (index === this.otpTextInput.length - 1) {
            this.otpTextInput[index].blur();
        }
    }
    async componentDidMount() {
        if (firebase.auth().currentUser) {
            await firebase.auth().signOut();
        }
        const { navigation } = this.props;
        if (navigation.getParam('userImage')) {
            this.setState({
                userImage: navigation.getParam('userImage'),
            });
        }
        if (navigation.getParam('userData')) {
            this.setState({
                userData: navigation.getParam('userData'),
                mobileNo: navigation.getParam('userData').mobile
            }, () => {
                this.sendOtp();
            });
        }
    }
    async sendOtp() {
        // this.confirmation = await firebase.auth().verifyPhoneNumber('+91' + this.state.mobileNo);
        this.confirmation = firebase.auth()
            .verifyPhoneNumber('+91'+this.state.mobileNo)
            .on('state_changed', (phoneAuthSnapshot) => {
                console.log(phoneAuthSnapshot.state);
                
                switch (phoneAuthSnapshot.state) {
                    case firebase.auth.PhoneAuthState.CODE_SENT: // or 'sent'
                        console.log('code sent');
                        break;

                    case firebase.auth.PhoneAuthState.ERROR: // or 'error'
                        ToastAndroid.show(
                            'Check your internet connection. There is something wrong with that! :)',
                            ToastAndroid.LONG
                        );
                        break;

                    case firebase.auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT: // or 'timeout'
                        ToastAndroid.showWithGravity(
                            'Check your internet connection. There is something wrong with that! :)',
                            ToastAndroid.SHORT,
                            ToastAndroid.BOTTOM
                        );
                        break;

                    case firebase.auth.PhoneAuthState.AUTO_VERIFIED: // or 'verified'
                        const { verificationId, code } = phoneAuthSnapshot;
                        if(code){
                            this.setState({
                                otp:code.split(''),
                                loading:true,
                            });
                        }else{
                            this.setState({
                                loading: true,
                                error:'OTP already verified signing in'
                            });
                        }
                        this.saveUserData();
                        break;
                }
            }, (error) => {
                console.log(error);
            }, (phoneAuthSnapshot) => {
                console.log(phoneAuthSnapshot);
            });
        // firebase.auth().onAuthStateChanged(user => {
        //     console.log(user);
            
        //     if (user) {
            // }
        // });
    }

    saveUserData(){
        firebase
            .auth()
            .createUserWithEmailAndPassword(this.state.userData.emailId, this.state.userData.password)
            .then((res) => {
                var api = new APILIB();
                console.log(JSON.stringify({ ...this.state.userData, "user": res.user.uid }));
                let fetchResponse = api.postData(JSON.stringify({ ...this.state.userData, "user": res.user.uid }));

                fetchResponse.then(response => {
                    response.json();
                })
                    .then(responseJson => {
                        if (this.state.userImage) {
                            const uploadTask = firebase.storage().ref('userImages').child(res.user.uid + '.jpg').putString(this.state.userImage, 'base64', { contentType: 'image/jpeg' });
                            uploadTask.then(
                                (response) => {
                                    this.setState({
                                        loading: false
                                    }, () => {
                                        this.props.navigation.navigate('Login', {
                                            signup: true
                                        });
                                    });
                                },
                                (failedReason) => {
                                    this.setState({
                                        loading: false
                                    }, () => {
                                        this.props.navigation.navigate('Login', {
                                            signup: true
                                        });
                                    });
                                }
                            );
                        } else {
                            this.setState({
                                loading: false
                            }, () => {
                                this.props.navigation.navigate('Login', {
                                    signup: true
                                });
                            });
                        }
                    });
            })
            .catch(error => {
                console.log(error);

                this.setState({
                    loading: false,
                })
            });
    }

    async verifyOTP() {
        let otp = this.state.otp.length == 6 ? this.state.otp.join('') : '';
        
        let otpReg = /^\d{6}$/;

        if (!otp.match(otpReg)) {
            this.setState({
                error: 'Please enter proper 6 digit otp'
            });
        } else {
            this.setState({
                error: '',
                loading: true
            });
            try {
                console.log(this.confirmation);
                
                this.confirmation.confirm(otp).then((res) => {
                    this.saveUserData();
                }).catch((error) => {
                    let errorMsg = '';
                    if (error.message.includes('auth/session-expired')) {
                        errorMsg = 'Session expired , resending OTP';
                        this.sendOtp();
                        this.setState({
                            otp: []
                        });
                    } else {
                        errorMsg = 'Invalid OTP Entered';
                    }
                    this.setState({
                        error: errorMsg,
                        loading: false,
                    });
                });
                // Successful login - onAuthStateChanged is triggered
            } catch (e) {
                console.log(e);
                
                this.setState({
                    error: 'Technical Issue occured',
                    loading: false,
                });
                return false;
            }
        }
    }
    resendOTP() {

    }
    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: Colors.primary }} />
                {this.state.loading &&
                    <View style={Commonstyles.loaderWrapper}>
                        <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                    </View>
                }
                <SafeAreaView style={styles.wrapper}>
                    <KeyboardAvoidingView style={{ flex: 1 }} behavior="position" enabled>
                        <View style={styles.authHeader}>
                            <Image style={styles.logo} source={require('../../assets/images/logo-fill.png')}></Image>
                        </View>
                        <View style={styles.formWrapper}>
                            <Text style={styles.otpLabel}>Please enter your 6 digit OTP sent to {this.state.mobileNo}</Text>

                            <View style={styles.otpWrapper}>
                                {this.renderInputs()}
                            </View>
                            <Text style={{ fontFamily: Fonts.PoppinsRegular, color: '#ff0000', fontSize: 14 }}>{this.state.error}</Text>
                            <TouchableOpacity onPress={() => this.verifyOTP()} style={{ ...Commonstyles.btnPrimary, ...styles.submitBtn }}>
                                <Text style={Commonstyles.btnPrimaryText}>
                                    SUBMIT
                                        </Text>
                            </TouchableOpacity>
                            <View style={styles.resendOTPWrapper}>
                                <Text style={styles.resendOTPLabel}>Didn't get OTP?</Text>
                                <TouchableOpacity onPress={() => this.sendOtp()} style={styles.resendOTPBtnWrapper}>
                                    <Text style={styles.resendOTPBtn}> Resend</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </Fragment>
        )
    }
}
