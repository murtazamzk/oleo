
import React, { Component, Fragment } from 'react';
import { View, Image } from 'react-native'; 
import firebase from '@react-native-firebase/app';

import {Commonstyles } from '../constants';

export default class AsyncImage extends Component {

    constructor(props) {
        super(props)
        this.state = { 
            userImage: null
        }
        this._isMounted = false;
    }
    async getProfileImage(uid) {
        const ref = firebase.storage().ref('userImages/' + uid + '.jpg');
        ref.getDownloadURL().then((res) => {
            if(this._isMounted){
                this.setState({
                    userImage: res
                });
            }
        }, () => {
        });
    }
    componentDidMount(){
        this._isMounted = true;
        this.getProfileImage(this.props.uid);
    }
    render() {
        return (
            <View>
                {this.state.userImage ? 
                <View>
                    <Image 
                        style={Commonstyles.userImageGrey} 
                        source={{ uri: this.state.userImage }}
                        resizeMode={'cover'}
                        onLoad={this._onLoad} />
                </View>
                :
                    <View>
                        <Image style={Commonstyles.userImage} source={require('../../assets/images/placeholder.png')} />
                    </View>
                }
            </View>
        )
    }
}