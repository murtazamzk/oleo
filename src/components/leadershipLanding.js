import React, { Component, Fragment } from 'react';
import { View, Text, Image, StyleSheet, TextInput, Picker, ActivityIndicator, Dimensions, ImageBackground, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { Colors, Commonstyles, Fonts } from '../constants';
import { Directions } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import ProfileBox from './profileBox';
import APILIB from '../api';
import UserImage from './userImageLoopLeaderBoard'; 

const { width, height } = Dimensions.get('screen');

this._isMounted = false;

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.secondary
    },
    leadershipWrapper: {
        flex: 1,
        width: '100%',
        marginTop: 10,
    },
    leadershipHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        paddingVertical: 20,
        position: 'relative',
        width: '90%',
        paddingTop: 40,
    },
    goBackIconWrapper: {
        marginRight: 30,
        zIndex: 1,
        paddingHorizontal:15,
        paddingVertical:10,
    },
    goBackIcon: {
        width: 18,
        height: 18,
    },
    headerLabel: {
        color: '#ffffff',
        fontSize: 22,
        textAlign: 'left',
        flex: 1,
        fontFamily: Fonts.PoppinsRegular,
    },
    leadershipContent: {
        flex: 1,
        width: '90%',
        alignSelf: 'center',
        marginTop: 20
    },
    challengesCTABox: {
        backgroundColor: '#0e2b3b',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 25,
        paddingVertical: 35,
        borderRadius: 10,
        marginBottom: 20,
        overflow: 'hidden'
    },
    challengesIcon: {
        width: 75,
        height: 45
    },
    challengesContent: {
        marginLeft: 20,
        flex: 1
    },
    challengesTitle: {
        color: '#ffffff',
        fontFamily: Fonts.PoppinsBold,
        fontSize: 16,
        marginBottom: 5,
    },
    challengesCountWrapper: {
        paddingHorizontal: 15,
        paddingVertical: 5,
        borderRadius: 15,
        backgroundColor: Colors.secondary,
        alignSelf: 'flex-start'
    },
    challengesCountText: {
        color: '#ffffff',
        fontSize: 12,
        fontFamily: Fonts.PoppinsRegular
    },
    challengesBg: {
        position: 'absolute',
        width: 100,
        height: 115,
        right: 0,
        bottom: -20,
        zIndex: -1
    },
    tabWrapper: {
        marginTop: 10,
    },
    tabsTitle: {
        color: '#fff',
        fontSize: 16,
        fontFamily: Fonts.PoppinsBold,
    },
    tabBtnWrapper: {
        flexDirection: 'row',
        marginTop: 12,
    },
    tabBtn: {
        backgroundColor: '#1a3645',
        width: '50%',
        paddingVertical: 15,
        borderTopLeftRadius: 12,
        borderBottomLeftRadius: 12,
    },
    tabBtnText: {
        color: Colors.textGrey,
        fontSize: 14,
        textAlign: 'center',
        fontFamily: Fonts.PoppinsMedium,
    },
    tabBtnRight: {
        backgroundColor: '#1a3645',
        width: '50%',
        paddingVertical: 15,
        borderTopRightRadius: 12,
        borderBottomRightRadius: 12,
    },
    tabBtnActive: {
        backgroundColor: Colors.primary,
    },
    tabBtnTextActive: {
        color: Colors.secondary,
    },
    tabContent: {
        marginTop: 20,
    },
    tabItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 10,
    },
    tabDetailsLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
    userHexBg: {
        position: 'absolute',
        bottom: 5,
        left: 40,
        width: 25,
        height: 25,
        justifyContent: 'center',
    },
    userLevel: {
        color: '#ffffff',
        fontSize: 13,
        fontFamily: Fonts.PoppinsRegular,
        textAlign: 'center',
    },
    userDetails: {
        paddingLeft: 25,
        alignItems: 'flex-start',
        flex: 0.7,
    },
    userRank:{
        color: '#ffffff',
        fontSize: 18,
        fontFamily: Fonts.PoppinsRegular,
        textAlign: 'center',
    },
    userName: {
        color: 'rgba(255,255,255,0.75)',
        fontSize: 16,
        fontFamily: Fonts.PoppinsBold,
    },
    userRig: {
        color: 'rgba(255,255,255,0.5)',
        fontSize: 12,
        fontFamily: Fonts.PoppinsRegular,
    },
    scoreWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    scoreArrow: {
        width: 15,
        height: 8,
    },
    scoreHex: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 70,
        height: 70,
        marginLeft: 10,
    },
    scoreText: {
        fontFamily: Fonts.PoppinsMedium,
        color: Colors.primary,
        fontSize: 14,
    }
});

export default class leadershipLanding extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: {},
            userImage: '',
            challenges: [],
            activeTab:1,
            leaderboard:[],
            userImages:[],
            attemptedChallenges:null,
        };
    }

    async componentDidMount() {
        this._isMounted = true;
        await this.getData();
        this.unsubscribeUser = firebase.firestore().collection('users').doc(this.state.userData.user).onSnapshot((snapshot) => {
            let db = snapshot;
            if (this._isMounted) {
                this.setState({
                    userData: db.data()
                });
            }
        });
        this.unsubscribeGamification = await firestore()
            .collection('users')
            .doc(this.state.userData.user)
            .collection('gamification')
            .onSnapshot((snapshot) => {
                let db = snapshot.docs[0].data().questions;
                let challenges = [];
                if(db){
                    db.forEach((value, index) => {
                        challenges.push(value);
                    });
                    if (this._isMounted) {
                        this.setState({
                            challenges
                        });
                        if (snapshot.docs[0].data().attempted) {
                            this.setState({
                                attemptedChallenges: snapshot.docs[0].data().attempted
                            });
                        }
                    }
                }
            });
        this.getMyLeaderBoard(this.state.userData.managerid);
    }

    getLeaderBoard(){
        var api = new APILIB();
        let fetchResponse = api.leaderBoard();
        fetchResponse.then(response => {
            response.json().then((res) => {
                if(this.state.activeTab == 2){
                    if(this._isMounted){
                        this.setState({
                            leaderboard:res.result
                        });
                    }
                }
            }).catch(error => {
                console.log(error);
            });
        });
    }

    getMyLeaderBoard(managerid){
        var api = new APILIB();
        let fetchResponse = api.myLeaderBoard(managerid);
        fetchResponse.then(response => {
            response.json().then((res) => {
                if(this.state.activeTab == 1){
                    if(this._isMounted){
                        this.setState({
                            leaderboard:res.result
                        });
                    }
                }
            }).catch(error => {
                console.log(error);
            });
        });
    }

    toggleTab(){
        if(this._isMounted){
            this.setState({
                activeTab : this.state.activeTab == 1 ? 2 : 1,
                leaderboard:[]
            },() => {
                this.state.activeTab == 1 ? this.getMyLeaderBoard(this.state.userData.managerid) : this.getLeaderBoard()
            });
        }
    }

    async getData() {
        try {
            const value = await AsyncStorage.getItem('@user')
            if (value !== null) {
                if(this._isMounted){
                    this.setState({
                        userData: JSON.parse(value)
                    });
                }
            }
        } catch (e) {
            console.log(e);
        }
    }

    componentWillUnmount(){
        this._isMounted = false;
        this.unsubscribeUser();
        this.unsubscribeGamification();
    }

    render() {
        let tabLeftActive = { ...styles.tabBtn, ...styles.tabBtnActive };
        let tabLeft = { ...styles.tabBtn};
        let tabActiveText = { ...styles.tabBtnText, ...styles.tabBtnTextActive };
        let tabText = { ...styles.tabBtnText };
        let tabRightActive = {...styles.tabBtnRight,...styles.tabBtnActive};
        let tabRight = {...styles.tabBtnRight};
        return (
            <SafeAreaView style={styles.wrapper}>
                <View style={styles.leadershipWrapper}>
                    <View style={styles.leadershipHeader}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackIconWrapper}>
                            <Image style={styles.goBackIcon} source={require('../../assets/images/backArrow.png')} />
                        </TouchableOpacity>
                        <Text style={styles.headerLabel}>Leaderboard</Text>
                    </View>
                    <ScrollView style={styles.leadershipContent}>
                        <ProfileBox navigation={this.props.navigation} />


                        {this.state.challenges.length ?
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('LeadershipChallenges',{
                                "challenges":this.state.challenges,
                                "attemptedChallenges":this.state.attemptedChallenges,
                                "userid": this.state.userData.user,
                                "managerid":this.state.userData.managerid
                            })} style={{ ...styles.challengesCTABox, position: 'relative' }}>
                                <Image style={styles.challengesIcon} source={require('../../assets/images/icon-puzzle.png')} />
                                <View style={styles.challengesContent}>
                                    <Text style={styles.challengesTitle}>Challenges</Text>
                                    <View style={styles.challengesCountWrapper}>
                                        <Text style={styles.challengesCountText}>{this.state.userData.challenge_attempts ? this.state.userData.challenge_attempts.length : 0}/{this.state.challenges.length}</Text>
                                    </View>
                                </View>
                                <Image style={styles.challengesBg} source={require('../../assets/images/challengesBg.png')}></Image>
                            </TouchableOpacity>
                            :
                            <View style={Commonstyles.loaderWrapperInline}>
                                <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                            </View>
                        }

                        <View style={styles.tabWrapper}>
                            <Text style={styles.tabsTitle}>Leaderboard</Text>
                            <View style={styles.tabBtnWrapper}>
                                <TouchableOpacity onPress={() => this.toggleTab()} style={this.state.activeTab == 1 ? tabLeftActive : tabLeft}>
                                    <Text style={this.state.activeTab == 1 ? tabActiveText : tabText}>MY POINTS</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.toggleTab()} style={this.state.activeTab == 2 ? tabRightActive : tabRight}>
                                    <Text style={this.state.activeTab == 2 ? tabActiveText : tabText}>MY TEAM</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.tabContent}>
                                {this.state.leaderboard.length ?
                                    <React.Fragment>
                                        {this.state.leaderboard.map((value,index) => (
                                            <View key={index} style={styles.tabItem}>
                                                <Text style={styles.userRank}>{value.rank}</Text>
                                                <UserImage uid={value.id} level={value.level} ></UserImage>
                                                <View style={styles.userDetails}>
                                                    <Text style={styles.userName}>{value.name}</Text>
                                                    <Text style={styles.userRig}>Ahmedabad</Text>
                                                </View>
                                                <View style={styles.scoreWrapper}>
                                                    {/* <Image style={styles.scoreArrow} source={require('../../assets/images/icon-arrowUp.png')}></Image> */}
                                                    <ImageBackground style={styles.scoreHex} source={require('../../assets/images/hexBg.png')} >
                                                        <Text style={styles.scoreText}>{value.points}</Text>
                                                    </ImageBackground>
                                                </View>
                                            </View>
                                        ))}
                                    </React.Fragment>
                                    :
                                    <View style={Commonstyles.loaderWrapperInline}>
                                        <ActivityIndicator style={Commonstyles.loader}></ActivityIndicator>
                                    </View>
                                }
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}
